-- Add migration script here
CREATE TABLE notifications (
    id INTEGER PRIMARY KEY NOT NULL,
    lorry_path TEXT NOT NULL,
    last_job ID INTEGER,
    sent BOOLEAN CHECK (sent IN (0, 1)) NOT NULL DEFAULT 0,
    FOREIGN KEY (last_job) REFERENCES jobs(id)
);

CREATE TRIGGER notification
AFTER UPDATE OF last_status ON lorries
FOR EACH ROW
WHEN (NEW.last_status != OLD.last_status AND old.last_status != 'None')
BEGIN
    INSERT INTO notifications 
    (lorry_path, last_job, sent) 
    VALUES (NEW.path, OLD.running_job, false);
END;
