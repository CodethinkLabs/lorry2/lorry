-- This is a concatenation of the migrations from the old lorry controller system
-- Yes, that does mean there are some redundant statements (see: "troves" being created, moved to "hosts", then dropped)
-- But in the interests of maintaining backwards compatibility, it's kept this way
CREATE TABLE running_queue (running INT);
INSERT INTO running_queue VALUES (1) ;


CREATE TABLE troves (
    trovehost TEXT PRIMARY KEY, 
    protocol TEXT, 
    username TEXT, 
    password TEXT, 
    lorry_interval INT, 
    lorry_timeout INT, 
    ls_interval INT, 
    ls_last_run INT, 
    prefixmap TEXT, 
    ignore TEXT );


CREATE TABLE lorries (
        path TEXT PRIMARY_KEY,
        text TEXT,
        from_trovehost TEXT,
        from_path TEXT,
        running_job INT,
        last_run INT,
        interval INT,
        lorry_timeout INT,
        disk_usage INT,
        last_run_exit INT,
        last_run_error TEXT
    ) ;
    
    CREATE TABLE next_job_id (job_id INT);
    INSERT INTO next_job_id VALUES (1);


    CREATE TABLE jobs (job_id INT PRIMARY KEY, host TEXT, pid INT, started INT, ended INT, updated INT, kill INT, path TEXT, exit TEXT, disk_usage INT, output TEXT);


CREATE TABLE max_jobs (max_jobs INT);

CREATE TABLE time (now INT);

CREATE TABLE stupid (value INT);

ALTER TABLE troves ADD COLUMN gitlab_token;

CREATE TABLE hosts (
          host TEXT PRIMARY KEY, 
          protocol TEXT, 
          username TEXT,
          password TEXT,
          type TEXT NOT NULL, 
          type_params TEXT NOT NULL,
          lorry_interval INT, 
          lorry_timeout INT, 
          ls_interval INT, 
          ls_last_run INT,
          prefixmap TEXT, 
          ignore TEXT 
          );




INSERT INTO hosts 
          SELECT trovehost, protocol, username, password, 
          CASE WHEN gitlab_token IS NULL THEN 'trove' ELSE 'gitlab' END, 
          CASE WHEN gitlab_token IS NULL THEN '{}' 
          ELSE '{\"private-token\":\"' || gitlab_token || '\"}' END, 
          lorry_interval, lorry_timeout, ls_interval, ls_last_run, prefixmap, 
          ignore 
          FROM troves;

DROP TABLE troves;