-- Add migration script here
ALTER TABLE lorries
ADD priority BOOLEAN CHECK (priority IN (0, 1)) NOT NULL DEFAULT 0;
