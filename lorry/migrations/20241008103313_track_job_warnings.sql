-- Add migration script here
ALTER TABLE lorries ADD COLUMN
    last_warnings INTEGER NOT NULL DEFAULT 0;

CREATE TABLE warnings (
    id INTEGER PRIMARY KEY NOT NULL,
    job_id INTEGER NOT NULL,
    kind TEXT CHECK (kind IN ('NonMatchingRefSpecs')) NOT NULL,
    message TEXT NOT NULL
);
