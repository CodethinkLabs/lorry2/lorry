-- Add migration script here
CREATE TABLE refs (
    id INTEGER PRIMARY KEY NOT NULL,
    job_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    message TEXT NOT NULL,
    successful BOOLEAN CHECK (successful IN (0, 1)) NOT NULL DEFAULT 0,
    FOREIGN KEY (job_id) REFERENCES jobs(id)
);

ALTER TABLE lorries ADD COLUMN 
    last_status TEXT 
    CHECK (last_status IN ('None', 'Successful', 'Failed', 'RefsFailed')) 
    NOT NULL DEFAULT 'None';

ALTER TABLE jobs ADD COLUMN
    status TEXT
    CHECK (status IN ('None', 'Successful', 'Failed', 'RefsFailed'))
    NOT NULL DEFAULT 'None';
