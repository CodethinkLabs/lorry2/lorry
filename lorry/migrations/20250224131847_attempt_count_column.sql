-- Add migration script here
ALTER TABLE notifications ADD COLUMN attempt INTEGER NOT NULL DEFAULT 1;

CREATE TRIGGER failed_job
AFTER UPDATE OF status on jobs
FOR EACH ROW
WHEN NEW.status = 'Failed'
BEGIN
    UPDATE notifications
    SET attempt = attempt + 1
    WHERE lorry_path = NEW.path AND sent = 0;
END;

CREATE TRIGGER successful_job
AFTER UPDATE of status on jobs
FOR EACH ROW
WHEN (NEW.status = 'Success' OR NEW.status = 'RefsFailed')
BEGIN
    UPDATE notifications
    SET attempt = 0
    WHERE lorry_path = NEW.path AND sent = 0;
END;
