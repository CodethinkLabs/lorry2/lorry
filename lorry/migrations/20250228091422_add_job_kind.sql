-- Add migration script here

ALTER TABLE jobs ADD COLUMN
    kind TEXT CHECK (kind IN ('Mirror', 'Gc', 'Fsck')) NOT NULL DEFAULT 'Mirror';

ALTER TABLE lorries ADD COLUMN
    last_kind TEXT CHECK (last_kind IN ('Mirror', 'Gc', 'Fsck')) NOT NULL DEFAULT 'Mirror';

ALTER TABLE lorries ADD COLUMN
    maintenance_interval INT NOT NULL DEFAULT 86400;
