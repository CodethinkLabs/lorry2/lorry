-- Clean up the database to use a traditional primary key for jobs and
-- eliminate the next_job_id table in favor of incrementing primary key.
CREATE TEMPORARY TABLE jobs_temp AS SELECT * FROM jobs;
DROP TABLE jobs;
CREATE TABLE jobs (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    host TEXT NOT NULL,
    pid INTEGER NOT NULL,
    started INTEGER NOT NULL,
    ended INTEGER,
    updated INTEGER,
    path TEXT NOT NULL,
    exit INTEGER,
    output TEXT
);

INSERT INTO jobs (
    id,
    host,
    pid,
    started,
    ended,
    updated,
    path,
    exit,
    output
) SELECT 
    job_id, 
    host, 
    pid, 
    started, 
    ended, 
    updated, 
    path, 
    exit, 
    output 
FROM jobs_temp;

DROP table next_job_id;
