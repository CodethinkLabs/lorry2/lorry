//!
//! Build URLs of targets
//!
use url::Url;
use workerlib::redact;

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Could not read token file: {0}")]
    CannotReadToken(#[from] std::io::Error),
    #[error("Url cannot be constructed for some reason: {0}")]
    CannotConstructUrl(String),
}

/// URLBuilder constructs downstream URLs with authentication data
#[derive(Clone, Debug)]
pub struct UrlBuilder {
    pub hostname: String,
    pub username: Option<String>,
    pub private_token: Option<String>,
    pub token_file: Option<std::path::PathBuf>,
    pub port: Option<u16>,
    pub insecure: Option<bool>,
}

impl Default for UrlBuilder {
    fn default() -> Self {
        UrlBuilder {
            hostname: String::from("localhost"),
            username: None,
            private_token: None,
            token_file: None,
            port: None,
            insecure: None,
        }
    }
}

impl UrlBuilder {
    /// Build a downstream URL with needed authentication
    pub fn build(&self, repository: &str) -> Result<Url, Error> {
        let token = if let Some(private_token) = &self.private_token {
            Some(private_token.clone())
        } else if let Some(private_token_path) = &self.token_file {
            let contents = std::fs::read_to_string(private_token_path)?;
            Some(contents.trim().to_string())
        } else {
            None
        };

        // having a token specified implies HTTP/HTTPS authentication
        let url = if let Some(ref token) = token {
            tracing::debug!("Downstream using HTTP");
            let mut url = Url::parse(&format!(
                "{}://{}",
                if self.insecure.is_some_and(|enabled| enabled) {
                    "http"
                } else {
                    "https"
                },
                self.hostname
            ))
            .map_err(|e| Error::CannotConstructUrl(redact::redact(&e.to_string())))?;
            let username = self.username.clone().unwrap_or(String::from("oauth2"));
            url.set_username(&username)
                .map_err(|_| Error::CannotConstructUrl(redact::redact(&url)))?;
            url.set_password(Some(token))
                .map_err(|_| Error::CannotConstructUrl(redact::redact(&url)))?;
            if let Some(port) = self.port {
                url.set_port(Some(port))
                    .map_err(|_| Error::CannotConstructUrl(redact::redact(&url)))?;
            };
            url
        } else {
            tracing::debug!("Downstream using SSH");
            let mut url = Url::parse(&format!("ssh://git@{}", self.hostname))
                .map_err(|e| Error::CannotConstructUrl(redact::redact(&e)))?;
            if let Some(ref username) = self.username {
                url.set_username(username)
                    .map_err(|_| Error::CannotConstructUrl(redact::redact(&url)))?;
            }
            if let Some(port) = self.port {
                url.set_port(Some(port))
                    .map_err(|_| Error::CannotConstructUrl(redact::redact(&url)))?;
            };
            url
        };
        let repository = if repository.ends_with(".git") {
            repository.to_string()
        } else {
            let mut repository = repository.to_string();
            repository.push_str(".git");
            repository
        };
        let url = url
            .join(&repository)
            .map_err(|_| Error::CannotConstructUrl(redact::redact(&url)))?;
        Ok(url)
    }
}

#[cfg(test)]
mod tests {
    use std::{fs, io::Write};
    use tempfile::tempdir;

    use super::*;

    #[test]
    fn url_builder_token() {
        let mut builder = UrlBuilder {
            hostname: String::from("localhost"),
            private_token: Some(String::from("super-secret-token")),
            port: Some(8080),
            ..Default::default()
        };
        let target = builder.build("rsc/hello").unwrap();
        assert!(
            target.to_string() == "https://oauth2:super-secret-token@localhost:8080/rsc/hello.git"
        );
        // disable https
        builder.insecure = Some(true);
        let target = builder.build("rsc/hello").unwrap();
        assert!(
            target.to_string() == "http://oauth2:super-secret-token@localhost:8080/rsc/hello.git"
        );
    }

    #[test]
    fn url_builder_ssh() {
        let builder = UrlBuilder {
            hostname: String::from("localhost"),
            port: Some(2222),
            ..Default::default()
        };
        let target = builder.build("rsc/hello").unwrap();
        assert!(target.to_string() == "ssh://git@localhost:2222/rsc/hello.git");
    }

    #[test]
    fn url_builder_token_file() {
        let tempdir = tempdir().expect("Could not get temporary directory");
        let tempfile_path = tempdir.path().join(".token");
        fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .create_new(true)
            .open(&tempfile_path)
            .unwrap();

        let mut tempfile_writer =
            fs::File::create(tempfile_path.clone()).expect("Could not create file");
        tempfile_writer
            .write_all(b"token")
            .expect("Could not write to tempfile");

        let builder = UrlBuilder {
            hostname: String::from("localhost"),
            insecure: Some(true),
            token_file: Some(tempfile_path),
            port: Some(8080),
            ..Default::default()
        };
        let target = builder.build("rsc/hello").unwrap();
        assert!(target.to_string() == "http://oauth2:token@localhost:8080/rsc/hello.git");
    }
}
