//!
//! Validates Lorry2 config
//!
//! Examples of the config files are in `/lorry2/example/`
//!
//! Validate downstream GitLab project names, group names, subgroup names and
//! paths/slugs.  Lorry not only adheres to those restrictions, but is actually
//! stricter because it accepts only some ASCII chars (so, for example, it
//! doesn't accept ASCII control chars, emojis, or any accented letters).
//!
//! Note that GitLab groups can contain subgroups, which can themselves then
//! contain subgroups
//!

use crate::comms::LorryPath;
use std::collections::HashSet;

/// If a Lorry2 configuration will result in a disallowed GitLab group name,
/// subgroup name, project name, or path, one of these GitLabNameError variants
/// will be used to indicate the problem
///
/// [Relevant GitLab documentation](https://docs.gitlab.com/ee/user/reserved_names.html)
#[derive(thiserror::Error, Debug)]
pub enum GitLabNameError {
    /// The Lorry configuration will result in a disallowed GitLab group name
    #[error("GitLab Group Name Error: {0}")]
    Group(String),

    /// The Lorry configuration will result in a disallowed GitLab subgroup name
    #[error("GitLab Sub Group Name Error: {0}")]
    SubGroup(String),

    /// The Lorry configuration will result in a disallowed GitLab project name
    #[error("GitLab Project Name Error: {0}")]
    Project(String),

    /// The Lorry configuration will result in a disallowed GitLab path
    #[error("GitLab Path Error: {0}")]
    Path(String),
}

/// Validator with possible future options
pub struct Validator {}

impl Validator {
    /// Validate that a [LorryPath] can be used by downstream GitLab
    pub fn validate_gitlab_path(lorry_path: &LorryPath) -> Result<(), Vec<GitLabNameError>> {
        let mut gitlab_name_errors: Vec<GitLabNameError> = Vec::new();

        // Break up the path to check the parts
        let mut path_components = lorry_path.components();
        if path_components.len() < 2 {
            gitlab_name_errors.push(GitLabNameError::Path(String::from(
                "Path doesn't have enough components",
            )));
            return Err(gitlab_name_errors);
        }

        // Validate the Lorry2 path as a whole
        check_gitlab_path(&lorry_path.to_string(), &mut gitlab_name_errors);

        // Check the project name
        let project = path_components.pop().unwrap();
        check_gitlab_project_name(project, &mut gitlab_name_errors);

        // Check the group name
        let group = path_components.remove(0);
        check_gitlab_group_name(group, &mut gitlab_name_errors);

        // Check any subgroups
        for subgroup in path_components {
            check_gitlab_group_name(subgroup, &mut gitlab_name_errors);
        }

        // If there are any GitLab name problems, log them and return
        if !gitlab_name_errors.is_empty() {
            return Err(gitlab_name_errors);
        }
        Ok(())
    }
}

/// Check a GitLab group name
///
/// [Relevant GitLab documentation](https://docs.gitlab.com/ee/user/reserved_names.html)
///
/// Can only contain:
/// - ~~Emoji~~ (not allowed by Lorry2)
/// - Letters
/// - Digits
/// - `_`
/// - `.`
/// - Dash (presume `-`)
/// - Space
/// - Parenthesis (presume `()`) (but not `[{«»}]`)
///
/// It must start with
/// - Letter
/// - Digit
/// - ~~Emoji~~ (not allowed by Lorry2)
/// - `_`
///
#[allow(unused)]
fn check_gitlab_group_name(group_name: &str, gitlab_name_errors: &mut Vec<GitLabNameError>) {
    // Check if the group name is reserved by GitLab
    check_reserved_gitlab_group_name(group_name, gitlab_name_errors);

    // Check first char
    let first_char = group_name.chars().next().unwrap();
    if !(first_char.is_ascii_alphanumeric() || first_char == '_') {
        gitlab_name_errors.push(GitLabNameError::Group(format!(
            "Group name '{}' has a disallowed first char",
            group_name
        )));
    }

    // Check the rest
    for char in group_name.chars() {
        if !(char.is_ascii_alphanumeric() || "_.- ()".contains(char)) {
            gitlab_name_errors.push(GitLabNameError::Group(format!(
                "Group name '{}' contains a disallowed char",
                group_name
            )));
        }
    }
}

/// Check a GitLab subgroup name
///
/// [Relevant GitLab documentation](https://docs.gitlab.com/ee/user/reserved_names.html)
///
/// GitLab restrictions are the same as for groups, but without the same
/// reserved names.
///
/// Can only contain:
/// - ~~Emoji~~ (not allowed by Lorry2)
/// - Letters
/// - Digits
/// - `_`
/// - `.`
/// - Dash (presume `-`)
/// - Space
/// - Parenthesis (presume `()`) (but not `[{«»}]`)
///
/// It must start with
/// - Letter
/// - Digit
/// - ~~Emoji~~ (not allowed by Lorry2)
/// - `_`
///
#[allow(unused)]
fn check_gitlab_subgroup_name(subgroup_name: &str, gitlab_name_errors: &mut Vec<GitLabNameError>) {
    // Check if the subgroup name is reserved by GitLab
    check_reserved_gitlab_subgroup_name(subgroup_name, gitlab_name_errors);

    // Check first char
    let first_char = subgroup_name.chars().next().unwrap();
    if !(first_char.is_ascii_alphanumeric() || first_char == '_') {
        gitlab_name_errors.push(GitLabNameError::SubGroup(format!(
            "Subgroup name '{}' has a disallowed first char",
            subgroup_name
        )));
    }

    // Check the rest
    for char in subgroup_name.chars() {
        if !(char.is_ascii_alphanumeric() || "_.- ()".contains(char)) {
            gitlab_name_errors.push(GitLabNameError::SubGroup(format!(
                "Subroup name '{}' contains a disallowed char",
                subgroup_name
            )));
        }
    }
}

/// Check a GitLab project name
///
/// [Relevant GitLab documentation](https://docs.gitlab.com/ee/user/reserved_names.html)
///
/// Can only contain:
///
/// - ~~Emoji~~ (not allowed by Lorry2)
/// - `a-zA-Z`
/// - `0-9`
/// - Underscores `_`
/// - Dots `.`
/// - `+`
/// - `-`
/// - Spaces (` `)
///
#[allow(unused)]
fn check_gitlab_project_name(project_name: &str, gitlab_name_errors: &mut Vec<GitLabNameError>) {
    // Check if the project name is reserved by GitLab
    check_reserved_gitlab_project_name(project_name, gitlab_name_errors);

    // Check whole
    for char in project_name.chars() {
        if !(char.is_ascii_alphanumeric() || "_.+- ".contains(char)) {
            gitlab_name_errors.push(GitLabNameError::Project(format!(
                "Project name '{}' contains a disallowed char",
                project_name
            )));
        }
    }
}

/// Check a GitLab path
///
/// Note that this does not check the constituent parts (group/subgroup/project)
///
/// [Relevant GitLab documentation](https://docs.gitlab.com/ee/user/reserved_names.html)
///
/// Can only contain:
/// - (Non-accented) letters
/// - Digits
/// - `_`
/// - `-`
/// - `.`
///
/// Must not:
/// - Start with `-`
/// - End in `.`, `.git`, or `.atom`
///
#[allow(unused)]
fn check_gitlab_path(path: &str, gitlab_name_errors: &mut Vec<GitLabNameError>) {
    // Check whole
    for char in path.chars() {
        // Allow '/'s because we've already verified it's not in the
        // constituent parts
        if !(char.is_ascii_alphanumeric() || "_-.".contains(char) || "/".contains(char)) {
            gitlab_name_errors.push(GitLabNameError::Path(format!(
                "Path '{}' contains a disallowed char",
                path
            )));
        }
    }

    // Check first char
    let first_char = path.chars().next().unwrap();
    if first_char == '-' {
        gitlab_name_errors.push(GitLabNameError::Path(format!(
            "Path '{}' has a disallowed first char",
            path
        )));
    }

    // Check ending
    if (path.ends_with('.') || path.ends_with(".git") || path.ends_with(".atom")) {
        gitlab_name_errors.push(GitLabNameError::Path(format!(
            "Path '{}' has a disallowed final char",
            path
        )));
    }
}

/// Check if the string is a GitLab reserved project name
///
/// [Relevant GitLab documentation](https://docs.gitlab.com/ee/user/reserved_names.html)
#[allow(unused)]
fn check_reserved_gitlab_project_name(
    project_name: &str,
    gitlab_name_errors: &mut Vec<GitLabNameError>,
) {
    let reserved_gitlab_project_names = HashSet::from([
        r"\-",
        "badges",
        "blame",
        "blob",
        "builds",
        "commits",
        "create",
        "create_dir",
        "edit",
        "environments/folders",
        "files",
        "find_file",
        "gitlab-lfs/objects",
        "info/lfs/objects",
        "new",
        "preview",
        "raw",
        "refs",
        "tree",
        "update",
        "wikis",
    ]);
    if reserved_gitlab_project_names.contains(project_name) {
        gitlab_name_errors.push(GitLabNameError::Project(format!(
            "Project name '{}' is a reserved project name",
            project_name
        )));
    }
}

/// Check if the string is a GitLab reserved group name
///
/// [Relevant GitLab documentation](https://docs.gitlab.com/ee/user/reserved_names.html)
#[allow(unused)]
fn check_reserved_gitlab_group_name(
    group_name: &str,
    gitlab_name_errors: &mut Vec<GitLabNameError>,
) {
    let reserved_gitlab_group_names = HashSet::from([
        r"\-",
        ".well-known",
        "404.html",
        "422.html",
        "500.html",
        "502.html",
        "503.html",
        "admin",
        "api",
        "apple-touch-icon.png",
        "assets",
        "dashboard",
        "deploy.html",
        "explore",
        "favicon.ico",
        "favicon.png",
        "files",
        "groups",
        "health_check",
        "help",
        "import",
        "jwt",
        "login",
        "oauth",
        "profile",
        "projects",
        "public",
        "robots.txt",
        "s",
        "search",
        "sitemap",
        "sitemap.xml",
        "sitemap.xml.gz",
        "slash-command-logo.png",
        "snippets",
        "unsubscribes",
        "uploads",
        "users",
        "v2 ",
    ]);
    if reserved_gitlab_group_names.contains(group_name) {
        gitlab_name_errors.push(GitLabNameError::Group(format!(
            "Group name '{}' is a reserved group name",
            group_name
        )));
    }
}

/// Check if the string is a GitLab reserved subgroup name
///
/// [Relevant GitLab documentation](https://docs.gitlab.com/ee/user/reserved_names.html)
///
/// The reserved GitLab group names do not apply to subgroups
#[allow(unused)]
fn check_reserved_gitlab_subgroup_name(
    subgroup_name: &str,
    gitlab_name_errors: &mut Vec<GitLabNameError>,
) {
    let reserved_gitlab_subgroup_names = HashSet::from([r"\-"]);
    if reserved_gitlab_subgroup_names.contains(subgroup_name) {
        gitlab_name_errors.push(GitLabNameError::SubGroup(format!(
            "Subroup name '{}' is a reserved subgroup term",
            subgroup_name
        )));
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    //-------------------------------------------------------------------------

    type CheckerFunc = fn(&str, &mut Vec<GitLabNameError>);
    fn assert_allowed(checker_fn: CheckerFunc, str: &str) {
        let mut gitlab_name_errors: Vec<GitLabNameError> = Vec::new();
        checker_fn(str, &mut gitlab_name_errors);
        assert!(gitlab_name_errors.is_empty());
    }
    fn assert_disallowed(checker_fn: CheckerFunc, str: &str) {
        let mut gitlab_name_errors: Vec<GitLabNameError> = Vec::new();
        checker_fn(str, &mut gitlab_name_errors);
        assert!(!gitlab_name_errors.is_empty());
    }

    //-------------------------------------------------------------------------

    #[test]
    fn test_validate_against_gitlab() {
        // This is the bad path mentioned in Lorry2 issue #134
        let bad_path = LorryPath::new(String::from(
            "sources/mirrors/github/Old-Man-Programmer/tree",
        ));
        assert!(Validator::validate_gitlab_path(&bad_path).is_err());
    }

    #[test]
    fn test_check_gitlab_group_name() {
        // Allowed group names
        let allowed = ["_fine", "9andfine", "vanilla", "a()"];
        for name in allowed {
            assert_allowed(check_gitlab_group_name, name);
        }

        // Disallowed group names
        let disallowed = ["bad!group", "9&fine", "🙂bad", "a[{}]"];
        for name in disallowed {
            assert_disallowed(check_gitlab_group_name, name);
        }
    }

    #[test]
    fn test_check_gitlab_project_name() {
        // Allowed project names
        let allowed = ["hE_.+- ll123o", "vanilla"];
        for name in allowed {
            assert_allowed(check_gitlab_project_name, name);
        }

        // Disllowed project names
        let disallowed = ["tree", "h京ello", "bad!project", "hel✅🙂lo"];
        for name in disallowed {
            assert_disallowed(check_gitlab_project_name, name);
        }
    }

    #[test]
    fn test_check_gitlab_path() {
        // Allowed paths
        let allowed = [
            "goodatom",
            "good/atom",
            "bad.atoms",
            "bad.atom.other",
            "bad._",
            "9_3-s.ok",
        ];
        for name in allowed {
            assert_allowed(check_gitlab_path, name);
        }

        // Disallowed paths
        let disallowed = [
            "bad!path",
            "bad.atom",
            "bad.atom.git",
            "bad.",
            "-bad",
            "aëèéêa",
        ];
        for name in disallowed {
            assert_disallowed(check_gitlab_path, name);
        }
    }

    #[test]
    fn test_check_reserved_gitlab_project_name() {
        // Allowed project names
        let allowed = ["wiki"];
        for name in allowed {
            assert_allowed(check_reserved_gitlab_project_name, name);
        }

        // Disallowed project names
        let disallowed = ["wikis", "info/lfs/objects", r"\-"];
        for name in disallowed {
            assert_disallowed(check_reserved_gitlab_project_name, name);
        }
    }

    #[test]
    fn test_check_reserved_gitlab_group_name() {
        // Disallowed group names
        let disallowed = [".well-known", "404.html"];
        for name in disallowed {
            assert_disallowed(check_reserved_gitlab_group_name, name);
        }
    }

    #[test]
    fn test_check_reserved_gitlab_subgroup_name() {
        // Allowed subgroup names
        let allowed = ["bob"];
        for name in allowed {
            assert_allowed(check_reserved_gitlab_subgroup_name, name);
        }

        // Disallowed subgroup names
        let disallowed = ["\\-"];
        for name in disallowed {
            assert_disallowed(check_reserved_gitlab_subgroup_name, name);
        }
    }
}
