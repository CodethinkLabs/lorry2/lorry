//!
//! Manages the Lorry database
//!

use crate::comms;
use crate::web::routes::list_jobs;
use models::JobKind;
use serde::Deserialize;
use sqlx::{
    migrate::MigrateDatabase,
    sqlite::{SqliteConnectOptions, SqliteQueryResult},
    Sqlite, SqlitePool,
};
use std::cmp::min;
use std::ops::Deref;
use std::str::FromStr;
use workerlib::{PushRefs, Warning}; // FIXME: these abstractions leak

#[derive(Debug, Clone)]
pub struct StateDatabase(SqlitePool);

impl AsRef<SqlitePool> for StateDatabase {
    fn as_ref(&self) -> &SqlitePool {
        &self.0
    }
}

impl Deref for StateDatabase {
    type Target = SqlitePool;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Models suitable for serialization in the UI
pub mod models {
    use serde::{Deserialize, Serialize};
    use std::fmt::Display;
    use std::time::{SystemTime, UNIX_EPOCH};
    use workerlib::PushRefs;

    pub struct UnixTime(pub i64);

    /// Status of the lorry as seen by the scheduler
    #[derive(Debug, Clone)]
    pub enum Status {
        /// The Lorry is actively working
        Running,

        /// The Lorry is ready to start
        Ready,

        /// The Lorry is idle and waiting
        Idle,
    }

    impl Status {
        /// Create a new status object.
        ///
        /// NOTE: this is a copy of `ready_to_run` in `give_job.rs` and may
        /// reflect a slightly different reality since that function call will
        /// be run at a later time.
        pub fn new(running: bool, last_run: UnixTime, interval: UnixTime) -> Self {
            let now = SystemTime::now().duration_since(UNIX_EPOCH).unwrap();
            let ready = (now.as_secs() as i64 - last_run.0) >= interval.0;
            if running {
                Status::Running
            } else if ready {
                Status::Ready
            } else {
                Status::Idle
            }
        }
    }

    impl Display for Status {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Status::Running => f.write_str("Running"),
                Status::Ready => f.write_str("Ready"),
                Status::Idle => f.write_str("Idle"),
            }
        }
    }

    /// The status of a given Job after it has executed
    #[derive(Default, Debug, Clone)]
    pub enum JobStatus {
        /// No job has been run
        #[default]
        None,

        /// Last job finished successfully mirroring all refs
        Successful,

        /// Last job fatally failed
        Failed,

        /// Last job mirrored at least some refs successfully
        RefsFailed,
    }

    impl JobStatus {
        pub fn from_push_refs(refs: &PushRefs) -> Self {
            if !refs.0.is_empty() && refs.1.is_empty() {
                JobStatus::Successful
            } else if refs.0.is_empty() && !refs.1.is_empty() {
                JobStatus::Failed
            } else if !refs.0.is_empty() && !refs.1.is_empty() {
                JobStatus::RefsFailed
            } else {
                JobStatus::None
            }
        }
    }

    impl Display for JobStatus {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                JobStatus::None => f.write_str("None"),
                JobStatus::Successful => f.write_str("Successful"),
                JobStatus::Failed => f.write_str("Failed"),
                JobStatus::RefsFailed => f.write_str("RefsFailed"),
            }
        }
    }

    impl From<String> for JobStatus {
        fn from(value: String) -> Self {
            match value.as_str() {
                "None" => JobStatus::None,
                "Successful" => JobStatus::Successful,
                "Failed" => JobStatus::Failed,
                "RefsFailed" => JobStatus::RefsFailed,
                _ => unreachable!(), // DB CHECK prohibits other text values
            }
        }
    }

    /// Different operations that are performed against mirrors
    #[derive(Serialize, Deserialize, Default, Debug, Clone)]
    pub enum JobKind {
        /// Mirroring of a an upstream Git repository
        #[default]
        Mirror,
        /// Git garbage collection and pruning
        Gc,
        /// Git fsck checking
        Fsck,
    }

    impl Display for JobKind {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                JobKind::Mirror => f.write_str("Mirror"),
                JobKind::Gc => f.write_str("Gc"),
                JobKind::Fsck => f.write_str("Fsck"),
            }
        }
    }

    impl From<String> for JobKind {
        fn from(value: String) -> Self {
            match value.as_str() {
                "Mirror" => JobKind::Mirror,
                "Gc" => JobKind::Gc,
                "Fsck" => JobKind::Fsck,
                _ => unreachable!(), // DB CHECK prohibits other text values
            }
        }
    }

    #[derive(Debug, Clone)]
    #[allow(dead_code)]
    pub struct Ref {
        pub id: i64,
        pub name: String,
        pub message: String,
        pub successful: bool,
    }

    #[derive(Debug, Clone)]
    pub struct Warning {
        pub id: i64,
        pub warning: workerlib::Warning,
    }

    impl Warning {
        pub fn from_row(id: i64, kind: &str, message: &str) -> Self {
            match kind {
                "NonMatchingRefSpecs" => Warning {
                    id,
                    warning: workerlib::Warning::NonMatchingRefSpecs {
                        pattern: message.to_string(),
                    },
                },
                _ => unreachable!(),
            }
        }
    }

    #[derive(Debug, Clone)]
    pub struct Job {
        pub id: i64,
        pub path: String,
        pub started: i64,
        pub ended: Option<i64>,
        pub exit_code: Option<i64>,
        pub output: Option<String>,
        pub status: JobStatus,
        pub warnings: i64,
        pub kind: JobKind,
    }

    #[derive(Debug, Clone)]
    #[allow(dead_code)]
    pub struct Lorry {
        pub path: String, // LorryPath?
        pub interval: i64,
        pub timeout: i64,
        pub status: Status,
        pub last_run: i64,
        pub last_successful_run: Option<i64>,
        pub last_exit_code: Option<i64>,
        pub last_status: JobStatus,
        pub last_warnings: i64,
    }

    #[derive(Default, Debug, Clone)]
    #[allow(dead_code)]
    pub struct LorryStatus {
        pub last_job_id: i64,
        pub path: String,
        pub status: JobStatus,
        pub warnings: i64,
    }

    #[derive(Debug)]
    pub struct Notification {
        pub id: i64,
        pub job_id: i64,
        pub path: String,
        pub output: Option<String>,
        pub status: JobStatus,
    }
}

/// Initialize the database and run migrations
pub async fn init_and_migrate(db_url: &str) -> Result<(), sqlx::Error> {
    let connect_opts = SqliteConnectOptions::from_str(db_url)?;
    if !Sqlite::database_exists(db_url).await? {
        Sqlite::create_database(db_url).await?;
    }
    let me = SqlitePool::connect_with(connect_opts)
        .await
        .map_err(|e| {
            tracing::error!("DB setup failed:{}", e);
            e
        })
        .map(StateDatabase)?;
    sqlx::migrate!().run(&me.0).await.map_err(|e| {
        tracing::error!("DB migrations failed:{}", e);
        e
    })?;
    me.close().await;
    Ok(())
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub(crate) struct LorryEntry {
    pub path: comms::LorryPath, // Possibly same as name
    pub name: String,
    pub spec: workerlib::LorrySpec,

    /// Identifier of the currently running job
    pub running_job: Option<i64>,

    /// The time the job last ran
    pub last_run: comms::TimeStamp,

    /// The time a job was last attempted to run, meaning when it began
    pub last_attempted: comms::TimeStamp,

    pub interval: std::time::Duration,
    pub maintenance: std::time::Duration,
    pub lorry_timeout: std::time::Duration,
    pub last_run_results: comms::JobExitStatus,
    pub last_run_error: Option<String>,
    pub purge_from_before: comms::TimeStamp,
    pub priority: bool,
}

/// Functions for fetching lorries and job info depending on pagination query
/// parameters to display on the UI.  
#[derive(Deserialize, Clone)]
pub struct Pagination {
    #[serde(default)]
    pub page: u32,
    #[serde(default = "items_per_page_default")]
    pub items_per_page: u32,
    #[serde(default)]
    pub future_page_count: u32,
}

// LOWER LIMIT IS 5
// TODO: Add a filter/find feature to lorries so that pagination can be used
// more effectively. Keep this simple to avoid bloating API.
fn items_per_page_default() -> u32 {
    5000
}

impl Pagination {
    /// Subtracts offset from total to find remaining jobs/lorries and modifies
    /// struct field `future_page_count`
    fn set_future_page_count(&mut self, total: u32, offset: u32) {
        const N_MAXIMUM_NEXT_PAGES: u32 = 3;
        let mut remaining: u32 = total.saturating_sub(offset);
        if remaining == self.items_per_page {
            remaining = 0
        }
        // If number of remaining jobs is less than 3 more paginations
        // prevent user clicking into empty paginations
        self.future_page_count = min(remaining / self.items_per_page, N_MAXIMUM_NEXT_PAGES);
    }

    pub async fn paginate_failed_lorries(
        &mut self,
        db: &StateDatabase,
    ) -> Result<Vec<models::Lorry>, sqlx::Error> {
        let offset = self.page * self.items_per_page;
        // Begin transaction
        let mut tx = db.0.begin().await?;
        // Get total failed lorries
        let total_lorries =
            sqlx::query!("SELECT COUNT(*) AS count FROM lorries WHERE last_run_exit != 0")
                .fetch_one(&mut *tx)
                .await
                .map(|r| r.count)?;
        // set number of future pages
        self.set_future_page_count(total_lorries as u32, offset);
        // Get information on failed lorries
        let failed_lorries = sqlx::query!(
            r#"
SELECT * FROM lorries 
WHERE last_run_exit = -1
ORDER BY 
    CASE
        -- show running jobs first
        WHEN running_job IS NOT NULL THEN 10
        ELSE 0
    END DESC,
    -- then order by last run
    max(last_run, last_attempted)
DESC LIMIT ?
OFFSET ?
"#,
            self.items_per_page,
            offset
        )
        .map(|l| models::Lorry {
            path: l.path,
            interval: l.interval,
            timeout: l.lorry_timeout,
            last_run: l.last_run,
            status: models::Status::new(
                l.running_job.is_some_and(|running| running > 0),
                models::UnixTime(std::cmp::max(l.last_run, l.last_attempted)),
                models::UnixTime(l.interval),
            ),
            last_successful_run: l.last_successful_run,
            last_exit_code: l.last_run_exit,
            last_status: l.last_status.into(),
            last_warnings: l.last_warnings,
        })
        .fetch_all(&mut *tx)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })?;
        // Pass back struct with future page count field set
        Ok(failed_lorries)
    }

    pub async fn paginate_jobs(
        &mut self,
        db: &StateDatabase,
        lorry_path: &str,
    ) -> Result<Vec<models::Job>, sqlx::Error> {
        let offset = self.page * self.items_per_page;
        // Begin transaction
        let mut tx = db.0.begin().await?;
        // Get total number of jobs
        let total_jobs = sqlx::query!(
            "SELECT COUNT(*) AS count FROM jobs WHERE path = ?",
            lorry_path
        )
        .fetch_one(&mut *tx)
        .await
        .map(|r| r.count)?;
        // Set future number of pages
        self.set_future_page_count(total_jobs as u32, offset);
        // get job info
        let jobs = sqlx::query!(
            r#"
SELECT 
    jobs.id, 
    jobs.path, 
    jobs.started, 
    jobs.ended, 
    jobs.exit, 
    jobs.status,
    jobs.kind,
    COUNT(warnings.id) as warnings
FROM jobs LEFT JOIN warnings on jobs.id = warnings.job_id
WHERE path = ?
GROUP BY jobs.id
ORDER BY jobs.id 
DESC LIMIT ?
OFFSET ?
"#,
            lorry_path,
            self.items_per_page,
            offset
        )
        .map(|r| models::Job {
            id: r.id,
            path: r.path,
            started: r.started,
            ended: r.ended,
            exit_code: r.exit,
            output: None,
            status: r.status.into(),
            warnings: r.warnings,
            kind: r.kind.into(),
        })
        .fetch_all(&mut *tx)
        .await?;
        // Pass back struct with future page count field set
        Ok(jobs)
    }

    pub async fn paginate_lorries(
        &mut self,
        db: &StateDatabase,
    ) -> Result<Vec<models::Lorry>, sqlx::Error> {
        let offset = self.page * self.items_per_page;
        // Begin transaction
        let mut tx = db.0.begin().await?;
        // Get total number of lorries
        let total_lorries = sqlx::query!("SELECT COUNT(*) AS count FROM lorries")
            .fetch_one(&mut *tx)
            .await
            .map(|r| r.count)?;

        self.set_future_page_count(total_lorries as u32, offset);
        let lorries = sqlx::query!(
            r#"
SELECT * FROM lorries 
ORDER BY 
    CASE
        -- show running jobs first
        WHEN running_job IS NOT NULL THEN 10
        ELSE 0
    END DESC,
    -- then order by last run
    max(last_run, last_attempted)
DESC LIMIT ?
OFFSET ?
"#,
            self.items_per_page,
            offset
        )
        .map(|l| models::Lorry {
            path: l.path,
            interval: l.interval,
            timeout: l.lorry_timeout,
            last_run: l.last_run,
            status: models::Status::new(
                l.running_job.is_some_and(|running| running > 0),
                models::UnixTime(std::cmp::max(l.last_run, l.last_attempted)),
                models::UnixTime(l.interval),
            ),
            last_successful_run: l.last_successful_run,
            last_exit_code: l.last_run_exit,
            last_status: l.last_status.into(),
            last_warnings: l.last_warnings,
        })
        .fetch_all(&mut *tx)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })?;
        // Pass back struct with future page count field set
        Ok(lorries)
    }
}

impl StateDatabase {
    /// Connect to the DB
    #[tracing::instrument(skip_all)]
    pub async fn connect(db_url: &str, read_only: bool) -> Result<StateDatabase, sqlx::Error> {
        let mut connect_opts = SqliteConnectOptions::from_str(db_url)?;
        if read_only {
            connect_opts = connect_opts.read_only(true);
        }
        let pool = SqlitePool::connect_with(connect_opts).await?;
        Ok(StateDatabase(pool))
    }

    /// Get all [`LorryEntry`]s from the DB
    #[tracing::instrument(skip_all)]
    pub async fn get_all_lorries_info(&self) -> Result<Vec<LorryEntry>, sqlx::Error> {
        sqlx::query!("SELECT * FROM lorries ORDER BY (max(last_run, last_attempted) + interval)")
            // TODO: This code is repeated with find_lorry_running_job; maybe they can be squashed together?
            .map(|l| {
                let (name, spec): (String, workerlib::LorrySpec) =
                    workerlib::extract_lorry_specs(l.text)
                        .unwrap()
                        .into_iter()
                        .next()
                        .unwrap(); // Lorry entry should only ever hold ONE lorry spec
                LorryEntry {
                    path: comms::LorryPath::new(l.path),
                    name,
                    spec,
                    running_job: l.running_job,
                    last_run: comms::TimeStamp(l.last_run),
                    last_attempted: comms::TimeStamp(l.last_attempted),
                    interval: std::time::Duration::from_secs(l.interval as u64),
                    maintenance: std::time::Duration::from_secs(l.maintenance_interval as u64),
                    lorry_timeout: std::time::Duration::from_secs(l.lorry_timeout as u64),
                    last_run_results: l.last_run_exit.into(),
                    last_run_error: l.last_run_error,
                    purge_from_before: comms::TimeStamp(l.purge_from_before), // TODO: we could use DATETIME in the DB and read this as https://docs.rs/sqlx/latest/sqlx/types/chrono/struct.DateTime.html maybe?
                    priority: l.priority,
                }
            })
            .fetch_all(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })
    }

    /// Returns the information of all failed lorries transformed to display on the user interface
    ///
    /// Uses the [models::Lorry] struct
    #[tracing::instrument(skip_all)] // TODO: Integrate into the function above with a parameter for failed?
    pub async fn get_all_failed_lorries_info(&self) -> Result<Vec<models::Lorry>, sqlx::Error> {
        sqlx::query!(
            r#"
SELECT * FROM lorries 
WHERE last_run_exit = -1
ORDER BY 
    CASE
        -- show running jobs first
        WHEN running_job IS NOT NULL THEN 10
        ELSE 0
    END DESC,
    -- then order by last run
    max(last_run, last_attempted)
"#,
        )
        .map(|l| models::Lorry {
            path: l.path,
            interval: l.interval,
            timeout: l.lorry_timeout,
            last_run: l.last_run,
            status: models::Status::new(
                l.running_job.is_some_and(|running| running > 0),
                models::UnixTime(std::cmp::max(l.last_run, l.last_attempted)),
                models::UnixTime(l.interval),
            ),
            last_successful_run: l.last_successful_run,
            last_exit_code: l.last_run_exit,
            last_status: l.last_status.into(),
            last_warnings: l.last_warnings,
        })
        .fetch_all(&self.0)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })
    }
    // NOTE the use of nullability
    // [overrides](https://docs.rs/sqlx/latest/sqlx/macro.query.html#overrides-cheatsheet)
    /// Get the latest [models::Job]
    #[tracing::instrument(skip_all)]
    pub async fn get_latest_job(&self, path: &str) -> Result<models::Job, sqlx::Error> {
        sqlx::query!(
            r#"
SELECT 
    jobs.id AS "id!: i64", 
    jobs.path, 
    jobs.started, 
    jobs.ended, 
    jobs.exit, 
    jobs.status,
    jobs.output,
    jobs.kind,
    COUNT(warnings.id) as warnings
FROM jobs LEFT JOIN warnings on jobs.id = warnings.job_id
WHERE path = ? 
GROUP BY jobs.id
ORDER BY jobs.id 
DESC LIMIT 1
"#,
            path
        )
        .fetch_one(&self.0)
        .await
        .map(|r| models::Job {
            id: r.id,
            path: r.path.clone(),
            started: r.started,
            exit_code: r.exit,
            ended: r.ended,
            output: r.output.clone(),
            warnings: r.warnings,
            status: models::JobStatus::from(r.status),
            kind: models::JobKind::from(r.kind),
        })
    }

    /// Get a jobs refs by its ID from the DB
    #[tracing::instrument(skip_all)]
    pub async fn get_job_refs(&self, job_id: i64) -> Result<Vec<models::Ref>, sqlx::Error> {
        Ok(sqlx::query!(
            r#"
SELECT id, name, message, successful
FROM refs WHERE job_id = ?
            "#,
            job_id
        )
        .fetch_all(&self.0)
        .await?
        .iter()
        .map(|r| models::Ref {
            id: r.id,
            name: r.name.clone(),
            message: r.message.clone(),
            successful: r.successful,
        })
        .collect())
    }

    /// Get the warnings for a job by its ID from the DB
    #[tracing::instrument(skip_all)]
    pub async fn get_job_warnings(&self, job_id: i64) -> Result<Vec<models::Warning>, sqlx::Error> {
        Ok(sqlx::query!(
            r#"
SELECT id, kind, message FROM warnings WHERE job_id = ?
            "#,
            job_id
        )
        .fetch_all(&self.0)
        .await?
        .iter()
        .map(|r| models::Warning::from_row(r.id, &r.kind, &r.message))
        .collect())
    }

    /// Get info about lorry jobs from the last 24 hours
    #[tracing::instrument(skip_all)]
    pub async fn get_latest_lorry_statuses(&self) -> Result<Vec<models::LorryStatus>, sqlx::Error> {
        let statuses: Vec<models::LorryStatus> = sqlx::query!(
            r#"
SELECT 
	ordered.id AS "job_id!: i64",
	ordered.path AS path,
	ordered.status AS status,
	(
		SELECT COUNT(*) FROM warnings 
		WHERE warnings.job_id = ordered.id
	) AS "warnings: i64"
FROM (
	SELECT 
    id, 
    path, 
    status 
  FROM jobs
  WHERE jobs.started > (SELECT unixepoch() - 86400)
  ORDER BY jobs.id DESC
) AS ordered
GROUP BY ordered.path
"#
        )
        .fetch_all(&self.0)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })?
        .iter()
        .map(|r| models::LorryStatus {
            last_job_id: r.job_id,
            path: r.path.clone(),
            status: models::JobStatus::from(r.status.clone()),
            warnings: r.warnings.unwrap_or(0),
        })
        .collect();
        Ok(statuses)
    }

    /// Return any available job for running or None if there is nothing to do
    #[tracing::instrument(skip_all)]
    pub(crate) async fn next(&self) -> Result<Option<LorryEntry>, sqlx::Error> {
        let row = sqlx::query!(
            r#"
SELECT * FROM lorries 
WHERE 
    (priority = 1 AND running_job IS NULL)
    OR (
        strftime('%s', 'now') - (MAX(last_run, last_attempted)) > interval 
        AND running_job IS NULL
    )
    -- Always allocate another job after a maintenance task has ran
    OR last_kind != 'Mirror' AND running_job IS NULL
ORDER BY
    CASE
        -- priority runs happen asap
        WHEN priority = 1 THEN 10
        -- followed by jobs which have never been ran
        WHEN last_run IS NULL THEN 5
        ELSE 0
    END DESC,
    -- finally jobs that are ready are ran based on order of previous runs
    last_run ASC
LIMIT 1
"#
        )
        .fetch_optional(&self.0)
        .await?;
        if let Some(l) = row {
            let (name, spec): (String, workerlib::LorrySpec) =
                workerlib::extract_lorry_specs(l.text)
                    .unwrap()
                    .into_iter()
                    .next()
                    .unwrap(); // Lorry entry should only ever hold ONE lorry spec
            let ret = LorryEntry {
                path: comms::LorryPath::new(l.path),
                name,
                spec,
                running_job: l.running_job,
                last_run: comms::TimeStamp(l.last_run),
                last_attempted: comms::TimeStamp(l.last_attempted),
                interval: std::time::Duration::from_secs(l.interval as u64),
                lorry_timeout: std::time::Duration::from_secs(l.lorry_timeout as u64),
                maintenance: std::time::Duration::from_secs(l.maintenance_interval as u64),
                last_run_results: l.last_run_exit.into(),
                last_run_error: l.last_run_error,
                purge_from_before: comms::TimeStamp(l.purge_from_before),
                priority: l.priority,
            };
            return Ok(Some(ret));
        };
        Ok(None)
    }

    /// Set all running jobs as having failed due to an internal error, and set
    /// all lorries as having not running jobs
    #[tracing::instrument(skip_all)]
    pub(crate) async fn reset(&self) -> Result<u64, sqlx::Error> {
        let mut tx = self.0.begin().await?;
        let result = sqlx::query!(
            r#"
WITH running_jobs AS (
    SELECT running_job FROM lorries WHERE running_job IS NOT NULL
)
UPDATE jobs
SET
    exit = -1,
    output = 'job was was not completed due to an internal error'
WHERE id IN running_jobs;

UPDATE lorries
SET
    running_job = NULL
WHERE running_job IS NOT NULL;
"#
        )
        .execute(&mut *tx)
        .await?;
        tx.commit().await?;
        Ok(result.rows_affected())
    }

    /// Get a single [`LorryEntry`] using its path
    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_lorry_info(
        &self,
        path: &comms::LorryPath,
    ) -> Result<LorryEntry, sqlx::Error> {
        let l = sqlx::query!("SELECT * FROM lorries WHERE path is ?", path)
            .fetch_one(&self.0)
            .await?;
        // These unwraps should be OK. if the data was uninitilaized in the DB, then it is a program error, not a user error
        let (name, spec): (String, workerlib::LorrySpec) = workerlib::extract_lorry_specs(l.text)
            .unwrap()
            .into_iter()
            .next()
            .unwrap(); // Lorry entry should only ever hold ONE lorry spec
        let ret = LorryEntry {
            path: comms::LorryPath::new(l.path),
            name,
            spec,
            running_job: l.running_job,
            last_run: comms::TimeStamp(l.last_run),
            last_attempted: comms::TimeStamp(l.last_attempted),
            interval: std::time::Duration::from_secs(l.interval as u64),
            maintenance: std::time::Duration::from_secs(l.maintenance_interval as u64),
            lorry_timeout: std::time::Duration::from_secs(l.lorry_timeout as u64),
            last_run_results: l.last_run_exit.into(),
            last_run_error: l.last_run_error,
            purge_from_before: comms::TimeStamp(l.purge_from_before),
            priority: l.priority,
        };
        Ok(ret)
    }

    /// Add a Lorry to the DB.
    ///
    /// This is called by `read_config::add_matching_lorries_to_statedb()`
    // TODO maybe these args could be combined into one LorryRow or something?
    #[tracing::instrument(skip_all)]
    pub(crate) async fn add_to_lorries(
        &self,
        path: &comms::LorryPath,
        text: &std::collections::BTreeMap<&comms::LorryPath, workerlib::LorrySpec>,
        interval: std::time::Duration,
        maintenance: std::time::Duration,
        timeout: std::time::Duration,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        let text = serde_yaml::to_string(text).unwrap(); // TODO pass up an error instead
        let now = comms::TimeStamp(chrono::Utc::now().timestamp());
        let interval_secs = interval.as_secs() as i32;
        let timeout_secs = timeout.as_secs() as i32;
        let maintenance_secs = maintenance.as_secs() as i32;
        // TODO reduce into one query
        match self.get_lorry_info(path).await {
            Err(_) => sqlx::query!(
                r#"
INSERT INTO lorries
(
    path,
    text,
    last_run,
    last_attempted,
    interval,
    lorry_timeout,
    running_job,
    purge_from_before,
    maintenance_interval
)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
"#,
                path,
                text,
                0,
                0,
                interval_secs,
                timeout_secs,
                Option::<i64>::None,
                now,
                maintenance_secs
            ),
            Ok(_) => sqlx::query!(
                "UPDATE lorries SET text=?, interval=?, lorry_timeout=?, maintenance_interval=? WHERE path IS ?",
                text,
                interval_secs,
                timeout_secs,
                maintenance_secs,
                path
            ),
        }
        .execute(&self.0)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })
    }

    /// Get all lorry paths from the DB
    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_lorries_paths(
        &self,
    ) -> Result<Vec<super::comms::LorryPath>, sqlx::Error> {
        sqlx::query!("SELECT path FROM lorries ORDER BY (max(last_attempted, last_run) + interval)")
            .map(|r| super::comms::LorryPath::new(r.path))
            .fetch_all(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })
    }

    /// Remove a lorry from the DB using its path
    #[tracing::instrument(skip_all)]
    pub(crate) async fn remove_lorry(
        &self,
        path: &super::comms::LorryPath,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        sqlx::query!("DELETE FROM lorries WHERE path IS ?", path)
            .execute(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })
    }

    /// Set the DB to purge all
    #[tracing::instrument(skip_all)]
    pub(crate) async fn set_repo_to_purge(&self) -> Result<SqliteQueryResult, sqlx::Error> {
        let now = comms::TimeStamp(chrono::Utc::now().timestamp());
        sqlx::query!("UPDATE lorries SET purge_from_before = ?", now)
            .execute(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })
    }

    /// Get `JobResults` from DB
    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_individual_job_records(
        &self,
    ) -> Result<Vec<list_jobs::JobResult>, sqlx::Error> {
        sqlx::query!("SELECT id, path, exit, host FROM jobs ORDER BY id DESC")
            .map(|r| list_jobs::JobResult {
                id: comms::JobId(r.id),
                path: r.path,
                exit_status: r.exit.map_or(comms::JobExitStatus::Running, |c| {
                    comms::JobExitStatus::FinishedExitCode(c)
                }),
                host: r.host,
            })
            .fetch_all(&self.0)
            .await
    }

    /// Get a [Job] by its ID
    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_job_by_id(
        &self,
        job_id: i64,
    ) -> Result<Option<models::Job>, sqlx::Error> {
        // FIXME: Add support for pagination
        sqlx::query!(
            r#"
SELECT 
    jobs.id, 
    jobs.path, 
    jobs.started, 
    jobs.ended, 
    jobs.exit, 
    jobs.status,
    jobs.output,
    jobs.kind,
    COUNT(warnings.id) as warnings
FROM jobs LEFT JOIN warnings on jobs.id = warnings.job_id
WHERE jobs.id = ?
GROUP BY jobs.id
"#,
            job_id
        )
        .fetch_one(&self.0)
        .await
        .map(|r| {
            Some(models::Job {
                id: r.id,
                path: r.path,
                started: r.started,
                ended: r.ended,
                exit_code: r.exit,
                output: r.output,
                warnings: r.warnings,
                status: r.status.into(),
                kind: r.kind.into(),
            })
        })
    }

    /// Toggle the priority column on the given lorry so that it will
    pub(crate) async fn toggle_priority_run(
        &self,
        path: &comms::LorryPath,
        state: bool,
    ) -> Result<(), sqlx::Error> {
        sqlx::query!(
            "UPDATE lorries SET priority = ? WHERE path = ?",
            state,
            path
        )
        .execute(&self.0)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })
        .map(|_| ())
    }

    /// Allocate a job for the given lorry using the next available job id,
    /// the hostname should coorespond to the current hostname of the OS and
    /// the process_id should reflect the currently running lorry scheduler
    /// process.
    pub(crate) async fn issue(
        &self,
        path: &str,
        hostname: &str,
        process_id: u32,
        maintenance_interval_secs: u32,
    ) -> Result<(i64, JobKind), sqlx::Error> {
        let mut tx = self.0.begin().await?;
        // determine if maintenance needs to be performed or if we should
        // do a regular mirror operation. Maintenance jobs do not consider
        // the schedule interval and so subsequent operations will be ran
        // mostly sequentially.
        let rec = sqlx::query!(
            r#"
WITH window AS (
	SELECT (strftime('%s', 'now') - ?) AS seconds
)
SELECT
	CASE 
		-- No job has ever been ran
		WHEN (SELECT COUNT(*) FROM jobs WHERE path = ?) = 0 THEN 'Mirror'
		-- Gc has not been run in the time period
		WHEN 
			(SELECT COUNT(*) FROM jobs, window WHERE jobs.path = ? 
			AND jobs.kind = 'Gc' AND jobs.started >= window.seconds) = 0 THEN 'Gc'
		-- Fsck has not been run in the time period
		WHEN 
			(SELECT COUNT(*) FROM jobs, window WHERE jobs.path = ? 
			AND jobs.kind = 'Fsck' AND jobs.started >= window.seconds) = 0 THEN 'Fsck'
		-- Both Fsck and Gc have been run, do a regular mirror operation
		ELSE 'Mirror'
	END AS kind
        "#,
            maintenance_interval_secs,
            path,
            path,
            path
        )
        .fetch_one(&mut *tx)
        .await?;
        let job_kind = rec
            .kind
            .map(models::JobKind::from)
            .expect("invalid database value for job kind");
        let job_kind_str = job_kind.to_string();
        let result = sqlx::query!(
            r#"
    INSERT INTO jobs (
        host,
        pid,
        started,
        updated,
        path,
        kind
    ) VALUES (
        ?,
        ?,
        STRFTIME('%s', 'NOW'), 
        STRFTIME('%s', 'NOW'),
        ?,
        ?
    ) RETURNING id;
            "#,
            hostname,
            process_id,
            path,
            job_kind_str,
        )
        .fetch_one(&mut *tx)
        .await?;
        sqlx::query!(
            r#"
    UPDATE lorries SET
        running_job = ?
    WHERE
        path = ?;
            "#,
            result.id,
            path,
        )
        .execute(&mut *tx)
        .await?;
        tx.commit().await?;
        Ok((result.id, job_kind))
    }

    /// Record a job's result status within a transaction after issuing a job
    /// from the function call issue above.
    #[allow(clippy::too_many_arguments)]
    pub(crate) async fn record(
        &self,
        exit_code: i32,
        job_id: i64,
        path: &str,
        output: &str,
        status: &models::JobStatus,
        kind: &models::JobKind,
        push_refs: Option<&PushRefs>,
        warnings: Option<Vec<workerlib::Warning>>,
    ) -> Result<(), sqlx::Error> {
        let mut tx = self.0.begin().await?;
        let is_successful = matches!(status, models::JobStatus::Successful);
        let status_str = &status.to_string();
        let last_warnings = warnings
            .as_ref()
            .map(|warnings| warnings.len())
            .unwrap_or_default() as i64;

        let last_kind = kind.to_string();

        sqlx::query!(
            r#"
    UPDATE jobs
        SET 
            output = ?,
            updated = STRFTIME('%s', 'NOW'),
            exit = ?,
            ended = STRFTIME('%s', 'NOW'),
            status = ?
    WHERE
        id = ?;

    UPDATE lorries
        SET
            running_job = NULL,
            last_run = STRFTIME('%s', 'NOW'),
            last_run_exit = ?,
            last_successful_run = CASE 
                WHEN ? = true THEN
                    STRFTIME('%s', 'NOW') 
                    ELSE last_successful_run
                END,
            last_status = ?,
            last_warnings = ?,
            last_kind = ?
        WHERE
            path = ?;
"#,
            output,
            exit_code,
            status_str,
            job_id,
            exit_code,
            is_successful,
            status_str,
            last_warnings,
            last_kind,
            path,
        )
        .execute(&mut *tx)
        .await?;
        if let Some(push_refs) = push_refs {
            let successful: Vec<(bool, (&String, &String))> =
                push_refs.0.iter().map(|r| (true, (&r.0, &r.1))).collect();
            let failed: Vec<(bool, (&String, &String))> =
                push_refs.1.iter().map(|r| (false, (&r.0, &r.1))).collect();
            for (successful, (name, message)) in [successful, failed].concat().iter() {
                sqlx::query!(
                    r#"
INSERT INTO refs (
    job_id,
    name,
    message,
    successful
) VALUES (?, ?, ?, ?)
"#,
                    job_id,
                    name,
                    message,
                    successful,
                )
                .execute(&mut *tx)
                .await?;
            }
        };
        if let Some(warnings) = warnings {
            for warning in warnings {
                let (kind, message) = match warning {
                    Warning::NonMatchingRefSpecs { pattern } => {
                        (String::from("NonMatchingRefSpecs"), pattern.clone())
                    }
                };
                sqlx::query!(
                    r#"
INSERT INTO warnings (
    job_id,
    kind,
    message
) VALUES (?, ?, ?)
"#,
                    job_id,
                    kind,
                    message,
                )
                .execute(&mut *tx)
                .await?;
            }
        }
        tx.commit().await?;
        Ok(())
    }

    /// List all pending notifications
    pub(crate) async fn read_notifications(
        &self,
        threshold: u32,
    ) -> Result<Vec<models::Notification>, sqlx::Error> {
        let notifications = sqlx::query_as!(
            models::Notification,
            r#"SELECT 
                notifications.id, 
                jobs.id AS job_id,
                jobs.path AS "path!: String",
                jobs.output,
                jobs.status AS "status!: String"
            FROM notifications 
            LEFT JOIN jobs on notifications.last_job = jobs.id
            WHERE notifications.sent = 0 AND (notifications.attempt = 0 OR notifications.attempt >= ?)
            "#,
            threshold
        )
        .fetch_all(&self.0)
        .await?;
        Ok(notifications)
    }

    /// Mark a notification as having been sent
    pub(crate) async fn mark_sent(&self, notification_id: i64) -> Result<(), sqlx::Error> {
        sqlx::query!(
            "UPDATE notifications SET sent = 1 WHERE id = ?",
            notification_id
        )
        .execute(&self.0)
        .await?;
        Ok(())
    }

    /// Check the DB connection
    #[tracing::instrument(skip_all)]
    pub(crate) async fn check_connection(&self) -> Result<(), sqlx::Error> {
        self.0.acquire().await?;
        Ok(())
    }
}
