//!
//! The Lorry2 scheduler makes use of the Lorry database to schedule the
//! running of Lorries.
//!
//! * Build the scheduler
//! * Run the scheduler with [`Scheduler.schedule_all()`](Scheduler::schedule_all)
//!

use crate::comms::Job;
use crate::executor::Executor;
use crate::read_config::ReadConfigError;
use crate::state_db::LorryEntry;
use crate::url_builder::UrlBuilder;
use crate::worker::{DefaultWorker, JobAccepted, JobReady, JobResult, State, ThreadId, Worker};
use async_trait::async_trait;
use std::collections::HashMap;
use std::error::Error as StdError;
use std::sync::{Arc, RwLock};
use std::thread::available_parallelism;
use thiserror::Error;
use tokio::sync::mpsc::error::SendTimeoutError;
use tokio::sync::mpsc::{channel, Receiver};
use tokio::sync::Mutex;
use tokio::task::{spawn, JoinHandle};
use tokio::time::{self, Duration};
use tokio_util::sync::CancellationToken;
use work_queue::{LocalQueue, Queue};
use workerlib::Arguments;

/// Maximum amount of time sending a message on a channel can wait until it
/// causes a panic and the scheduler shuts down.
pub const CHANNEL_TIMEOUT_MS: u64 = 900;

/// Globally readable object that records the status of each running thread for
/// display in the Lorry web UI.
///
/// It indirectly maps [`ThreadId`]s & [`comms::Job`](crate::comms::Job)s to each other.
// TODO: Is the mapping actually done?
pub type Tracker = Arc<RwLock<HashMap<ThreadId, State>>>;

/// Scheduler component that runs in a separate thread
pub type Component<T> = Arc<Mutex<Box<T>>>;

/// Possible scheduling errors
#[derive(Error, Debug)]
pub enum SchedulerError {
    #[error("job recorder error: {0}")]
    Recorder(String),

    #[error("job polling error: {0}")]
    Polling(String),

    #[error("config polling error: {0}")]
    Configuration(String),

    #[error("channel timeout: {0}")]
    Timeout(#[from] Box<SendTimeoutError<Job>>),
}

struct Timing {
    allocator_interval: Duration,
    config_reader_interval: Duration,
    worker_interval: Duration,
}

impl Default for Timing {
    fn default() -> Self {
        Timing {
            allocator_interval: Duration::from_millis(800),
            config_reader_interval: Duration::from_millis(5000),
            worker_interval: Duration::from_millis(1500),
        }
    }
}

// TODO: This can be reworked so that a CancellationToken is referenced to a
// respective JoinHandle but for now I see no reason to add this
pub struct ShutdownHandle {
    handles: Vec<JoinHandle<()>>,
    cancellation_tokens: Vec<CancellationToken>,
}

impl ShutdownHandle {
    /// Shutdown Lorry2
    ///
    /// 1. Log the number of running tokio tasks
    /// 2. Call [`CancellationToken::cancel()`] on each [`CancellationToken`]
    /// 3. Wait for all tasks to end
    pub async fn shutdown(self) {
        tracing::warn!("shutting down scheduler threads");
        tracing::trace!(
            "running threads: {}",
            self.handles
                .iter()
                .filter(|handle| !handle.is_finished())
                .count()
        );
        tracing::trace!("cancelling {} tokens", self.cancellation_tokens.len());
        self.cancellation_tokens
            .iter()
            .for_each(|token| token.cancel());
        futures::future::join_all(self.handles).await;
    }
}

/// Create a new tracker
pub fn tracker() -> Tracker {
    Arc::new(RwLock::new(HashMap::new()))
}

/// Create a new component that can be accessed across threads
pub fn component<T>(t: T) -> Component<T> {
    Arc::new(Mutex::new(Box::new(t)))
}

/// Responsible for checking for new lorries to run as jobs
#[async_trait]
pub trait JobAllocator: Send + 'static {
    /// Return the next available lorry to run from the database
    async fn next(&self) -> Result<Option<LorryEntry>, Box<dyn StdError>>;

    /// Create a new [`comms::Job`](crate::comms::Job) for the given lorry ([`LorryEntry`])
    async fn issue(&self, entry: &LorryEntry) -> Result<Job, Box<dyn StdError>>;

    async fn cleanup_db(&self) -> Result<(), Box<dyn StdError>>;
}

/// Job poller that runs at a set interval
///
/// See [`IntervalJobPoller::poll()`]
struct IntervalJobPoller<T>
where
    T: JobAllocator + Send + 'static,
{
    /// `poller` is a [`Component`]\<T\> where `T` implements [`JobAllocator`]
    /// (amongst others)
    pub poller: Component<T>,

    pub interval: Duration,

    /// `tracker` is a [`Tracker`]
    pub tracker: Tracker,

    pub queue: Queue<JobReady>,
    pub db_lock: Arc<Mutex<()>>,
}

/// Implement trait [`JobAllocator`]
impl<T> IntervalJobPoller<T>
where
    T: JobAllocator + Send + 'static,
{
    /// Check (using [`give_job::DBJobAllocator.next()`]) if there's a
    /// lorry that is due to be run.  If there is, push it onto the queue and
    /// wait for a worker to tell us it will do the work the lorry specifies.
    /// Once the lorry has been accepted by a worker, create a new [`comms::Job`](crate::comms::Job) and
    /// send it to the worker to run.
    async fn poll(&self) -> Result<(), SchedulerError> {
        let mut interval = time::interval(self.interval);
        let poller = self.poller.lock().await;
        tracing::info!("Initializing job poller");
        loop {
            // Prevent recorder db access
            let lock = self.db_lock.lock().await;
            // Get a Lorry if one is ready
            if let Some(lorry) = poller
                .next()
                .await
                .map_err(|e| SchedulerError::Polling(e.to_string()))?
            {
                let lorry_path = lorry.path.clone();

                // Create a channel over which `JobAccepted`s can be sent.  The
                // transmitter will be used by a worker in `work()`.
                let (tx, mut rx) = channel::<JobAccepted>(1);

                // Push the ready Lorry onto the global queue, along with the
                // `JobAccepted` transmitter.  When one of the local queues
                // empties they can take this JobReady.
                self.queue.push(JobReady(lorry.clone(), tx));

                // Wait for a worker to pop a `ReadyJob` off the queue, and
                // send a `JobAccepted` back (i.e. wait to be notified that a
                // worker is ready to run the job specified by the Lorry
                let result = rx.recv().await.unwrap();

                // Use the DB to issue/create a new job for the Lorry
                let job = poller
                    .issue(&lorry)
                    .await
                    .map_err(|e| SchedulerError::Polling(e.to_string()))?;
                let job_id = job.id;
                drop(lock);

                // Send the [`comms::Job`](crate::comms::Job) to the worker that just notified us it's ready
                // to run it
                result
                    .2
                    .send_timeout(job, Duration::from_millis(CHANNEL_TIMEOUT_MS))
                    .await
                    .map_err(Box::new)?;

                // Record that the thread is now busy
                self.tracker
                    .write()
                    .unwrap()
                    .insert(result.0, State::Busy((lorry_path.to_string(), job_id)));
            // If not ready yet, wait interval length of time
            } else {
                drop(lock);
                interval.tick().await;
            }
        }
    }

    /// Indirectly run [state_db::StateDatabase.reset()]
    async fn cleanup_db(&self) -> Result<(), SchedulerError> {
        let poller = self.poller.lock().await;
        poller
            .cleanup_db()
            .await
            .map_err(|e| SchedulerError::Polling(e.to_string()))
    }
}

/// Responsible for saving job results into a persistent store like a database
#[async_trait]
pub trait Recorder: Send + 'static {
    async fn record(&self, result: &JobResult) -> Result<(), Box<dyn StdError>>;
}

/// Wraps around a recorder and updates it as soon as it receives a result
struct FixedRecorder<T>
where
    T: Recorder + Send + 'static,
{
    pub results: Receiver<JobResult>,
    pub recorder: Component<T>,
    pub tracker: Tracker,
    pub db_lock: Arc<Mutex<()>>,
}

impl<T> FixedRecorder<T>
where
    T: Recorder + Send + 'static,
{
    /// Record the exit status (the [`JobResult`]) of a
    /// [`comms::Job`](crate::comms::Job) into the `state_db`.
    ///
    /// A worker calls `.run()` on its runner, which returns once the
    /// [`comms::Job`](crate::comms::Job) has completed or failed.  That
    /// success/failed info is immediately sent here to be recorded.
    async fn record(&mut self) -> Result<(), SchedulerError> {
        let recorder = self.recorder.lock().await;
        tracing::info!("Initializing job recorder");
        loop {
            if let Some(result) = self.results.recv().await {
                let thread_id = result.0;
                let lock = self.db_lock.lock().await;
                recorder
                    .record(&result)
                    .await
                    .map_err(|e| SchedulerError::Recorder(e.to_string()))?;
                drop(lock);
                self.tracker.write().unwrap().insert(thread_id, State::Idle);
            } else {
                tracing::warn!("Results channel has been shutdown");
                return Err(SchedulerError::Recorder(String::from(
                    "Results channel has shutdown",
                )));
            }
        }
    }
}

/// Responsble for periodically polling lorry configuration and updating the
/// database
#[async_trait]
pub trait ConfigReader: Send + 'static {
    /// Read the configuration from the file system or remote git repository
    /// and update the database to reflect the most recent config
    async fn read(&self) -> Result<(), ReadConfigError>;
}

/// Read the configuration at a specified interval
pub struct IntervalConfigReader<T>
where
    T: ConfigReader + Send + 'static,
{
    pub config_reader: Component<T>,
    pub interval: Duration,
    pub db_lock: Arc<Mutex<()>>,
}

impl<T> IntervalConfigReader<T>
where
    T: ConfigReader + Send + 'static,
{
    pub async fn read(&self) -> Result<(), SchedulerError> {
        let config_reader = self.config_reader.lock().await;
        let mut interval = time::interval(self.interval);
        tracing::info!("Initializing config reader");
        loop {
            let lock = self.db_lock.lock().await;
            config_reader
                .read()
                .await
                .map_err(|e| SchedulerError::Configuration(e.to_string()))?;
            drop(lock);
            interval.tick().await;
        }
    }
}

struct Settings {
    threads: usize,
    local_queue_size: u16,
}

impl Default for Settings {
    fn default() -> Self {
        let n_cpus = available_parallelism().unwrap();
        let threads = if usize::from(n_cpus) == 1 {
            1
        } else {
            usize::from(n_cpus) - 1
        };
        Settings {
            threads,
            local_queue_size: 32,
        }
    }
}

/// Builder for [`Scheduler`]
#[derive(Default)]
pub struct SchedulerBuilder {
    settings: Settings,
    timing: Timing,
    url_builder: UrlBuilder,
}

#[allow(dead_code)]
impl SchedulerBuilder {
    /// Username of the downstream git forge
    pub fn username(mut self, username: &str) -> Self {
        self.url_builder.username = Some(username.to_string());
        self
    }

    /// Hostname of the downstream git forge
    pub fn hostname(mut self, hostname: &str) -> Self {
        self.url_builder.hostname = hostname.to_string();
        self
    }

    /// Personal Access Token (PAT) for downstream git forge
    pub fn private_token(mut self, token: Option<String>) -> Self {
        self.url_builder.private_token = token;
        self
    }

    /// A path to a file that contains a token for downstream git forge
    pub fn token_file(mut self, file: Option<std::path::PathBuf>) -> Self {
        self.url_builder.token_file = file;
        self
    }

    /// If insecure operations are permitted against the downstream server
    pub fn insecure(mut self, insecure: bool) -> Self {
        self.url_builder.insecure = Some(insecure);
        self
    }

    /// Number of worker threads to spawn
    ///
    /// TODO: equal to n_threads in the config (but which n_threads?)
    pub fn threads(mut self, n_threads: usize) -> Self {
        self.settings.threads = n_threads;
        self
    }

    /// Queue size for each underlying worker thread
    pub fn local_queue_size(mut self, size: u16) -> Self {
        self.settings.local_queue_size = size;
        self
    }

    /// Time between checking for new jobs in the database
    pub fn poller_interval(mut self, duration: Duration) -> Self {
        self.timing.allocator_interval = duration;
        self
    }

    /// Interval between attempts to refresh the remote (or local) configuration
    pub fn config_reader_interval(mut self, duration: Duration) -> Self {
        self.timing.config_reader_interval = duration;
        self
    }

    /// Build a [`Scheduler`]
    ///
    /// This sets up workers and the global and local queues used by them.  It
    /// also sets up the channel used to pass around [`JobResult`] to record
    /// the results of jobs run.
    pub fn build<P, R, C, E>(
        self,
        arguments: &Arguments,
        poller: P,
        recorder: R,
        config_reader: Option<C>,
        executor: E,
    ) -> Scheduler<P, R, C, E>
    where
        P: JobAllocator + Send + 'static,
        R: Recorder + Send + 'static,
        C: ConfigReader + Send + 'static,
        E: Executor + Clone + Send + Sync + 'static,
    {
        // Work stealing queue passed to each thread bound worker
        //
        // There will be `self.settings.threads` number of [`LocalQueue`]s, and
        // each one of those local queues will be able to hold
        // `self.settings.local_queue_size` number of items before any further
        // [`JobReady`]s overflow into the global queue.
        //
        // The default number of [`JobReady`]s a local queue can hold is 32.
        let queue = Queue::<JobReady>::new(self.settings.threads, self.settings.local_queue_size);

        let (result_tx, result_rx) = channel::<JobResult>(self.settings.threads * 4);

        let tracker = Tracker::new(RwLock::new(HashMap::from_iter(
            (0..self.settings.threads).map(|n| (ThreadId(n), State::Idle)),
        )));

        // Give each worker a [`LocalQueue`]
        let workers: Vec<(LocalQueue<JobReady>, DefaultWorker<E>)> = queue
            .local_queues()
            .enumerate()
            .map(|(i, q)| {
                (
                    q,
                    DefaultWorker {
                        thread_id: ThreadId(i),
                        runner: component(executor.clone()),
                        mirror_settings: arguments.clone(),
                        url_builder: self.url_builder.clone(),
                        polling_interval: self.timing.worker_interval,
                        result_ch: result_tx.clone(),
                    },
                )
            })
            .collect();

        let lock = Arc::new(Mutex::new(()));

        // Return the newly built Scheduler.
        Scheduler {
            tracker: tracker.clone(),
            // The poller takes the queue.
            poller: IntervalJobPoller {
                poller: Arc::new(Mutex::new(Box::new(poller))),
                interval: self.timing.allocator_interval,
                tracker: tracker.clone(),
                queue,
                db_lock: lock.clone(),
            },
            // `recorder.results` holds the receiving end of the
            // `channel::<JobResult>`.  The transmitting end is used in a
            // worker's `work()`
            recorder: FixedRecorder {
                recorder: Arc::new(Mutex::new(Box::new(recorder))),
                results: result_rx,
                tracker: tracker.clone(),
                db_lock: lock.clone(),
            },
            config_reader: config_reader.map(|config_reader| IntervalConfigReader {
                config_reader: Arc::new(Mutex::new(Box::new(config_reader))),
                interval: self.timing.config_reader_interval,
                db_lock: lock.clone(),
            }),
            workers,
        }
    }
}

/// The Lorry scheduler
///
/// The Lorry scheduler is built using [`SchedulerBuilder`], and is then run
/// using [`Scheduler.schedule_all()`](Scheduler::schedule_all) (its only method).
pub struct Scheduler<P, R, C, E>
where
    P: JobAllocator + Send + 'static,
    R: Recorder + Send + 'static,
    C: ConfigReader + Send + 'static,
    E: Executor + Clone + Send + Sync + 'static,
{
    poller: IntervalJobPoller<P>,
    recorder: FixedRecorder<R>,
    config_reader: Option<IntervalConfigReader<C>>,
    tracker: Tracker,
    workers: Vec<(LocalQueue<JobReady>, DefaultWorker<E>)>,
}

impl<P, R, C, E> Scheduler<P, R, C, E>
where
    P: JobAllocator + Send + 'static,
    R: Recorder + Send + 'static,
    C: ConfigReader + Send + 'static,
    E: Executor + Clone + Send + Sync + 'static,
{
    /// Run the scheduler
    ///
    /// Before anything else, 2 vectors are created
    ///
    /// 1. `handles` is a vector of tokio [`JoinHandle`]s.
    /// 2. `cancellation_tokens` is a vector of tokio [`CancellationToken`]s.
    ///
    /// Before a tokio task is [`tokio::spawn()`]ed, a corresponding cancellation token is
    /// created; a clone of the token is pushed into `cancellation_tokens`
    /// and the tokio task takes the token.  The following tokens are created:
    ///
    /// * 1 token for the poller
    /// * 1 token for the recorder
    /// * 1 for the config reader
    /// * 0+ tokens for the workers (1 token for each entry in `workers`)
    ///
    /// The `poller`, `recorder`, `config_reader`, and `workers` fields are used
    /// from inside async closures which are run by [`tokio::spawn()`].  The closures
    /// use the [`tokio::select!`] macro to run the desired function (see below)
    /// and to then wait, either for the function to return an error or for a
    /// cancellation request (using the cancellation token):
    ///
    /// * for poller call its [`IntervalJobPoller.poll()`](IntervalJobPoller.poll) method
    /// * for recorder call its [`FixedRecorder.record()`](FixedRecorder.record) method
    /// * for config_reader call its [`IntervalConfigReader.read()`](IntervalConfigReader::read) method
    /// * for each worker in workers call its [`DefaultWorker.work()`](DefaultWorker::work) method
    ///
    /// Once all the tokio tasks have been spawned, a tuple is returned.  The
    /// first field is a [`ShutdownHandle`] struct that holds the `handles` and
    /// `cancellation_tokens` vectors; the second field holds a clone of the
    /// `tracker` field.
    pub async fn schedule_all(mut self) -> Result<(ShutdownHandle, Tracker), SchedulerError> {
        let mut handles: Vec<JoinHandle<()>> = Vec::new();
        let mut cancellation_tokens: Vec<CancellationToken> = Vec::new();

        // Spawn job poller
        let poller_cancellation_token = CancellationToken::new();
        cancellation_tokens.push(poller_cancellation_token.clone());
        handles.push(spawn(async move {
            tokio::select! {
                poller_error = self.poller.poll() => {
                    if let Err(err) = poller_error {
                        panic!("job poller shut down: {err}");
                    }
                }
                _ = poller_cancellation_token.cancelled() => {
                    tracing::debug!("shutting down poller");
                    self.poller.cleanup_db().await.expect("cleanup failed");
                }
            }
        }));

        // Spawn workers, with each one getting its own [`LocalQueue`] of [`JobReady`]s
        for (q, worker) in self.workers.into_iter() {
            let current_task_token = CancellationToken::new();
            cancellation_tokens.push(current_task_token.clone());
            handles.push(spawn(async move {
                tokio::select! {
                    worker_error = worker.work(q) => {
                        if let Err(err) = worker_error {
                            panic!("worker shut down: {err}");
                        }
                    }
                    _ = current_task_token.cancelled() => {
                        tracing::debug!("shutting down worker");
                    }
                }
            }));
        }

        // Spawn DB recorder
        let db_cancellation_token = CancellationToken::new();
        cancellation_tokens.push(db_cancellation_token.clone());
        handles.push(spawn(async move {
            tokio::select! {
                db_error = self.recorder.record() => {
                    if let Err(err) = db_error {
                        panic!("recorder shut down: {err}");
                    }
                }
                _ = db_cancellation_token.cancelled() => {
                    tracing::debug!("shutting down db");
                }
            }
        }));

        // Spawn config reader
        let config_reader_cancellation_token = CancellationToken::new();
        cancellation_tokens.push(config_reader_cancellation_token.clone());
        if let Some(config_reader) = self.config_reader {
            handles.push(spawn(async move {
                tokio::select! {
                    config_reader_error = config_reader.read() => {
                        if let Err(err) = config_reader_error {
                            panic!("config reader shut down: {err}")
                        }
                    }
                    _ = config_reader_cancellation_token.cancelled() => {
                        tracing::debug!("shutting down config reader");
                    }
                }
            }));
        }

        Ok((
            ShutdownHandle {
                handles,
                cancellation_tokens,
            },
            self.tracker.clone(),
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::comms::{JobExitStatus, JobId, LorryPath, TimeStamp};
    use crate::executor::{Executor, Params};
    use crate::state_db::models::JobKind;
    use async_trait::async_trait;
    use tokio::sync::mpsc::{channel, Sender};
    use tokio::time::sleep;
    use workerlib::{lorry_specs::SingleLorry, LorrySpec};
    use workerlib::{Error, MirrorStatus, PushRefs};

    #[derive(Clone, Default)]
    struct NoopExecutor {
        pub error: bool,
        pub panic: bool,
        pub runtime: Option<Duration>,
    }

    #[async_trait]
    impl Executor for NoopExecutor {
        async fn run(&self, _params: &Params) -> Result<MirrorStatus, Error> {
            if let Some(runtime) = self.runtime {
                sleep(runtime).await;
                return Ok(MirrorStatus::default());
            }
            if self.panic {
                panic!("runner has paniced");
            }
            if self.error {
                return Err(Error::AllRefspecsFailed {
                    refs: PushRefs::default(),
                    n_attempted: 1,
                });
            }
            Ok(MirrorStatus::default())
        }
    }

    struct NoopPoller(Option<Sender<bool>>);

    #[async_trait]
    impl JobAllocator for NoopPoller {
        async fn next(&self) -> Result<Option<LorryEntry>, Box<dyn StdError>> {
            Ok(Some(LorryEntry {
                path: LorryPath::new(String::new()),
                name: String::new(),
                spec: LorrySpec::Git(SingleLorry::default()),
                running_job: None,
                last_run: TimeStamp(0),
                last_attempted: TimeStamp(0),
                interval: Duration::from_secs(0),
                maintenance: Duration::from_secs(0),
                lorry_timeout: Duration::from_secs(1),
                last_run_results: JobExitStatus::Running,
                last_run_error: None,
                purge_from_before: TimeStamp(0),
                priority: false,
            }))
        }

        async fn issue(&self, _entry: &LorryEntry) -> Result<Job, Box<dyn StdError>> {
            Ok(Job {
                lorry_name: String::new(),
                lorry_spec: LorrySpec::Git(SingleLorry::default()),
                id: JobId(0),
                path: LorryPath::new(String::new()),
                purge_cutoff: TimeStamp(0),
                job_kind: JobKind::Mirror,
            })
        }

        async fn cleanup_db(&self) -> Result<(), Box<dyn StdError>> {
            if let Some(chan) = &self.0 {
                chan.send(true).await.unwrap();
            }
            Ok(())
        }
    }

    struct NoopRecorder(Option<Sender<(ThreadId, bool)>>);

    #[async_trait]
    impl Recorder for NoopRecorder {
        async fn record(&self, result: &JobResult) -> Result<(), Box<dyn StdError>> {
            if let Some(chan) = &self.0 {
                chan.send((result.0, result.2.is_err())).await.unwrap();
            }
            Ok(())
        }
    }

    struct NoopConfigReader;

    #[async_trait]
    impl ConfigReader for NoopConfigReader {
        async fn read(&self) -> Result<(), ReadConfigError> {
            Ok(())
        }
    }

    #[tokio::test]
    async fn scheduler_run_1x_thread() -> Result<(), Box<dyn std::error::Error>> {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = SchedulerBuilder::default().threads(1).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor::default(),
        );

        let (shutdown, _) = scheduler.schedule_all().await?;
        let result = rx.recv().await.unwrap();

        assert!(result.0 == ThreadId(0));
        assert!(!result.1);

        shutdown.shutdown().await;

        Ok(())
    }

    #[tokio::test]
    async fn scheduler_run_3x_thread() -> Result<(), Box<dyn std::error::Error>> {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = SchedulerBuilder::default().threads(3).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor::default(),
        );

        let (shutdown, _) = scheduler.schedule_all().await?;
        let result = rx.recv().await.unwrap();

        assert!(result.0 == ThreadId(0));
        assert!(!result.1);

        shutdown.shutdown().await;

        Ok(())
    }

    #[tokio::test]
    async fn scheduler_run_exceeds_runtime() -> Result<(), Box<dyn std::error::Error>> {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = SchedulerBuilder::default().threads(3).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor {
                runtime: Some(Duration::from_secs(3)),
                ..Default::default()
            },
        );

        let (shutdown, _) = scheduler.schedule_all().await?;
        let result = rx.recv().await.unwrap();

        assert!(result.0 == ThreadId(0));
        // job fails
        assert!(result.1);

        shutdown.shutdown().await;

        Ok(())
    }

    #[tokio::test]
    async fn scheduler_run_2_threads_5_jobs_all_blocking() -> Result<(), Box<dyn std::error::Error>>
    {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = SchedulerBuilder::default().threads(2).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor {
                runtime: Some(Duration::from_secs(3)),
                ..Default::default()
            },
        );

        let (shutdown, _) = scheduler.schedule_all().await?;

        assert!(rx.recv().await.unwrap().1);
        assert!(rx.recv().await.unwrap().1);
        assert!(rx.recv().await.unwrap().1);
        assert!(rx.recv().await.unwrap().1);
        assert!(rx.recv().await.unwrap().1);

        shutdown.shutdown().await;

        Ok(())
    }

    #[tokio::test]
    async fn scheduler_run_job_fails() -> Result<(), Box<dyn std::error::Error>> {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = SchedulerBuilder::default().threads(3).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor {
                error: true,
                ..Default::default()
            },
        );

        let (shutdown, _) = scheduler.schedule_all().await?;
        let result = rx.recv().await.unwrap();

        assert!(result.0 == ThreadId(0));
        assert!(result.1);

        shutdown.shutdown().await;

        Ok(())
    }

    #[tokio::test]
    #[should_panic]
    async fn scheduler_run_job_panics() {
        let (tx, mut rx) = channel::<(ThreadId, bool)>(1);

        let scheduler = SchedulerBuilder::default().threads(3).build(
            &Arguments::default(),
            NoopPoller(None),
            NoopRecorder(Some(tx)),
            Some(NoopConfigReader {}),
            NoopExecutor {
                panic: true,
                ..Default::default()
            },
        );

        let _ = scheduler.schedule_all().await.unwrap();
        tokio::time::timeout(Duration::from_millis(500), rx.recv())
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn scheduler_graceful_shutdown() -> Result<(), Box<dyn std::error::Error>> {
        let (tx, mut rx) = channel::<bool>(1);
        let scheduler = SchedulerBuilder::default()
            .threads(3)
            .poller_interval(Duration::from_millis(100))
            .build(
                &Arguments::default(),
                NoopPoller(Some(tx)),
                NoopRecorder(None),
                Some(NoopConfigReader {}),
                NoopExecutor::default(),
            );

        let (shutdown, _tracker) = scheduler.schedule_all().await?;

        shutdown.shutdown().await;

        let shutdown_val = rx.recv().await.unwrap();

        assert!(shutdown_val);

        Ok(())
    }
}
