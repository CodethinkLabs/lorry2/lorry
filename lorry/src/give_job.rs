//!
//! Works on behalf of the scheduler to poll jobs, and issue them if they're
//! ready. It interacts with the database.
//!

use crate::comms;
use crate::comms::Job;
use crate::downstream;
use crate::downstream::DownstreamError;
use crate::scheduler::JobAllocator;
use crate::scheduler::Recorder;
use crate::state_db::models::JobStatus;
use crate::state_db::LorryEntry;
use crate::state_db::StateDatabase;
use crate::validator::Validator;
use crate::web::ControllerSettings;
use crate::worker::JobResult;
use async_trait::async_trait;
use std::error::Error as StdError;

#[derive(thiserror::Error, Debug)]
pub(crate) enum JobGivingError {
    #[error("Database error occured for lorry {1} while issuing a job: {0}")]
    DBOperationErrorDuringIssue(sqlx::Error, comms::LorryPath),

    #[error("Failed to prepare downstream repo")]
    PreparingDownstreamError(DownstreamError, comms::LorryPath),
}

/// Adapter for recording jobs into the Lorry state database
pub struct DBRecorder(pub StateDatabase);

#[async_trait]
impl Recorder for DBRecorder {
    /// Record the exit status (the [`JobResult`]) of a [`comms::Job`](crate::comms::Job).
    async fn record(&self, result: &JobResult) -> Result<(), Box<dyn StdError>> {
        let job_kind = &result.1.job_kind;
        let mut exit_code: i32 = 0;
        let mut output: Option<String> = None;
        let (status, pushrefs, warnings) = match &result.2 {
            Ok(result) => {
                let warnings = if !result.warnings.is_empty() {
                    Some(result.warnings.clone())
                } else {
                    None
                };
                (
                    JobStatus::Successful,
                    Some(result.push_refs.clone()),
                    warnings,
                )
            }
            Err(err) => {
                output = Some(err.to_string());
                if let Some(code) = err.status() {
                    exit_code = code;
                } else {
                    exit_code = -1;
                };
                match err {
                    workerlib::Error::AllRefspecsFailed {
                        refs,
                        n_attempted: _,
                    } => (JobStatus::Failed, Some(refs.clone()), None),
                    workerlib::Error::SomeRefspecsFailed { refs, n_failed: _ } => {
                        (JobStatus::from_push_refs(refs), Some(refs.clone()), None)
                    }
                    _ => (JobStatus::Failed, None, None),
                }
            }
        };
        self.0
            .record(
                exit_code,
                result.1.id.0,
                &result.1.path.to_string(),
                &output.unwrap_or_default(),
                &status,
                job_kind,
                pushrefs.as_ref(),
                warnings,
            )
            .await?;
        Ok(())
    }
}

/// Adapter that uses the `state_db` module to check for any lorries due to be run,
/// and creates/issues the corresponding [`comms::Job`]s for workers to run.
pub struct DBJobAllocator(pub StateDatabase, pub ControllerSettings);

#[async_trait]
impl JobAllocator for DBJobAllocator {
    /// If there's a Lorry that's ready to be run, prepare the downstream store
    /// and return the Lorry.
    ///
    /// *This is not an `impl Iterator`.*
    ///
    /// From the DB fetch the next lorry (a [`LorryEntry`]) that should be run
    /// (if there is one).  This is done using [`StateDatabase::next()`].  For
    /// the lorry that's returned, prepare the downstream repo with
    /// [downstream::Downstream::prepare_repo].
    async fn next(&self) -> Result<Option<LorryEntry>, Box<dyn StdError>> {
        // `self.0` is a StateDatabase
        if let Some(lorry) = self.0.next().await? {
            let lorry_path = lorry.path.clone();

            // Validate path will be accepted by GitLab
            if let Err(errors) = Validator::validate_gitlab_path(&lorry_path) {
                for error in errors {
                    tracing::error!("GitLab path disallowed: {}", error);
                }
                return Ok(None);
            };

            if lorry.priority {
                // toggle off priority if it was set to prevent the job
                // from running immediately after it was issued.
                self.0.toggle_priority_run(&lorry_path, false).await?;
            };
            // TODO: Due to the desired statelessness of the Lorry controller there is
            // no way to infer the default branch of a given mirror because we would
            // have to reach out either to the mirror on disk or the remote repository.
            //
            // Wire up the worker side to detect the HEAD of the mirror and then
            // configure it here. Although the current use case is that of a single
            // process with full Gitlab access there are security considerations once
            // split between controllers and workers.
            match self
                .1
                .downstream
                .prepare_repo(
                    &lorry.path,
                    downstream::RepoMetadata {
                        head: None,
                        description: Some(format!("Mirror: {}", lorry.name)),
                    },
                )
                .await
                .map_err(|e| JobGivingError::PreparingDownstreamError(e, lorry.path.clone()))
            {
                Ok(_) => return Ok(Some(lorry.clone())),
                Err(e) => {
                    tracing::error!("Failed to prepare upstream repository: {}", e);
                    // Consider the job not viable if we can't configure the
                    // downstream repository but don't crash.
                    return Ok(None);
                }
            }
        };
        Ok(None)
    }

    /// Issue/create/schedule a new job for the Lorry
    ///
    /// Use [`StateDatabase::issue()`] to add a new job for the Lorry and get
    /// back the new job ID.  Create a new [`comms::Job`] with the new job ID and
    /// return it.
    async fn issue(&self, entry: &LorryEntry) -> Result<Job, Box<dyn StdError>> {
        let maintenance_window_secs = entry.maintenance.as_secs() as u32;
        let (new_job_id, job_kind) = self
            .0
            .issue(
                &entry.path.to_string(),
                &self.1.hostname,
                self.1.current_pid,
                maintenance_window_secs,
            )
            .await
            .map_err(|e| JobGivingError::DBOperationErrorDuringIssue(e, entry.path.clone()))?;
        Ok(comms::Job {
            lorry_name: entry.name.clone(),
            lorry_spec: entry.spec.clone(),
            id: comms::JobId(new_job_id),
            path: entry.path.clone(),
            purge_cutoff: entry.purge_from_before,
            job_kind,
        })
    }

    /// Use [`StateDatabase::reset()`] to mark any running jobs as failed/not
    /// running
    async fn cleanup_db(&self) -> Result<(), Box<dyn StdError>> {
        self.0.reset().await?;
        Ok(())
    }
}
