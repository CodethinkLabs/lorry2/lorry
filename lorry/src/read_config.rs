//!
//! Configuration for Lorries (not the Lorry programme)
//!
use crate::comms;
use crate::scheduler::ConfigReader;
use crate::state_db::StateDatabase;
use crate::url_builder::UrlBuilder;
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::time::Duration;
use std::{
    fmt::Write,
    fs::read_to_string,
    path::{Path, PathBuf},
};
use tokio::process::Command;
use tracing::error;
use url::Url;
use workerlib::execute::{execute, CommandBuilder};
use workerlib::lorry_specs::{extract_lorry_specs, LorrySpec};
use workerlib::workspace::Workspace;

/// The name of the Lorry config file, wherever that may be
const CONFIG_FILENAME: &str = "lorry-controller.conf";

// Path within the working directory where the special Lorry configuration
// will be stored.
pub const CONFGIT_DIR_NAME: &str = "__lorry_confgit";

/// Resolved Lorry configurations (loaded up related config files)
pub type Resolved = Vec<(Config, BTreeMap<String, LorrySpec>)>;

/// Errors that can be raised when loading Lorry config.  The config might be
/// local or remote.
#[derive(thiserror::Error, Debug)]
pub enum ReadConfigError {
    #[error("An error occurred removing unneeded lorries from config")]
    FailedToRemoveLorries(sqlx::Error),
    #[error("Glob Error: {0}")]
    Glob(#[from] glob::GlobError),
    #[error("Glob Pattern Invalid: {0}")]
    GlobPattern(#[from] glob::PatternError),
    #[error("IO error for file {0:?}: {1}")]
    IOError(PathBuf, std::io::Error),
    #[error("Failed to parse specs in {0}: {1}")]
    SpecParse(PathBuf, serde_yaml::Error),
    #[error("Failed to read config:\n{}", errors.iter().fold(String::new(), |mut acc, e| {let _ = writeln!(acc, "{e}"); acc}))]
    MultipleReadFails { errors: Vec<ReadConfigError> },
    #[error("DB transaction failed: {0}")]
    DBOperationFailed(#[from] sqlx::Error),
    #[error("Failed to setup configuration workspace: {0}")]
    Workspace(#[from] workerlib::workspace::Error),
    #[error("Failed to synchronize confgit: {0}")]
    Execute(#[from] workerlib::execute::Error),
    #[error("Failed to construct confgit url: {0}")]
    UrlBuilder(#[from] crate::url_builder::Error),
}

// TODO: A future optimization should replace git commands here with libgit2
// since it has better security properties and cloning operations are cheap
// for a confgit repository given it's limited size.
mod commands {
    use super::*;

    /// Fetch remote confgit
    pub struct FetchConfgit<'a> {
        pub target_url: &'a url::Url,
        pub git_config_path: &'a Path,
        pub branch_name: &'a str,
    }

    impl CommandBuilder for FetchConfgit<'_> {
        fn build(&self, current_dir: &Path) -> tokio::process::Command {
            let mut cmd = Command::new("git");
            let refspec = format!(
                "+refs/heads/{}:refs/heads/{}",
                self.branch_name, self.branch_name
            );
            cmd.current_dir(current_dir);
            cmd.envs([(
                workerlib::git_config::GIT_CONFIG_GLOBAL,
                self.git_config_path.to_string_lossy().as_ref(),
            )]);
            cmd.args([
                "fetch",
                "--prune",
                "--update-head-ok",
                self.target_url.as_ref(),
                &refspec,
            ]);
            cmd
        }
    }

    /// Reset remote confgit to a certain branch
    pub struct ResetHard<'a> {
        pub branch_name: &'a str,
        pub git_config_path: &'a Path,
    }

    impl CommandBuilder for ResetHard<'_> {
        fn build(&self, current_dir: &Path) -> tokio::process::Command {
            let mut cmd = Command::new("git");
            cmd.current_dir(current_dir);
            cmd.envs([(
                workerlib::git_config::GIT_CONFIG_GLOBAL,
                self.git_config_path.to_string_lossy().as_ref(),
            )]);
            cmd.args(["reset", "--hard", self.branch_name]);
            cmd
        }
    }

    /// Checkout desired confgit branch
    pub struct CheckoutBranch<'a> {
        pub branch_name: &'a str,
        pub git_config_path: &'a Path,
    }

    impl CommandBuilder for CheckoutBranch<'_> {
        fn build(&self, current_dir: &Path) -> tokio::process::Command {
            let mut cmd = Command::new("git");
            cmd.current_dir(current_dir);
            cmd.envs([(
                workerlib::git_config::GIT_CONFIG_GLOBAL,
                self.git_config_path.to_string_lossy().as_ref(),
            )]);
            cmd.args(["checkout", self.branch_name]);
            cmd
        }
    }
}

// TODO verify that we actually use other types, becuase looking at gnome it
// seems we don't
/// Configuration block contained in `lorry-controller.conf`, specifying a set
/// of mirrors and information about how to run them.
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type")]
pub enum Config {
    #[serde(rename = "lorries")]
    Lorries(LorryConfig),
}

/// Configuration block corresponding to a set of lorry mirrors.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct LorryConfig {
    /// Controls how often should a Lorry be run. Is deserialised from an ISO
    /// 8601 duration string by [comms::deserialise_duration]
    #[serde(
        deserialize_with = "comms::deserialise_duration",
        serialize_with = "comms::serialise_duration"
    )]
    pub interval: Duration,
    /// Controls the interval at which Maitenance jobs are ran for matching
    /// Lorries. If the duration is not specified than a multiple of
    /// 10 * Interval is used to run maintenance.
    #[serde(
        default,
        deserialize_with = "comms::deserialise_duration_option",
        serialize_with = "comms::serialise_duration_option"
    )]
    pub maintenance: Option<Duration>,

    // TODO is this value even used anywhere?
    // If not, we should drop it from the DB too
    /// Controls how long a Lorry can run for before it's stopped. Is
    /// deserialised from an ISO 8601 duration string by [comms::deserialise_duration]
    #[serde(
        deserialize_with = "comms::deserialise_duration",
        serialize_with = "comms::serialise_duration"
    )]
    pub timeout: Duration,

    /// Used to create downstream store location.  In the case of GitLab, this
    /// is used to create the group/subgroup/subgroup/etc
    pub prefix: String,

    /// Globs specifying files in which individual lorry confiurations are
    /// found
    pub globs: Vec<String>,
    pub lfs: Option<bool>,
}

impl LorryConfig {
    /// Get group/subgroup/subgroup slug
    pub fn get_group_and_subgroups_slug(&self) -> String {
        self.prefix.clone()
    }
}

impl Default for LorryConfig {
    fn default() -> Self {
        let secs_in_day = 60 * 60 * 24;
        Self {
            interval: std::time::Duration::from_secs(secs_in_day),
            timeout: std::time::Duration::from_secs(secs_in_day),
            maintenance: None,
            prefix: String::default(),
            globs: Vec::default(),
            lfs: Option::default(),
        }
    }
}

/// ConfigSource is either a local file system path or a remote URL from which
/// Lorry uses git to clone remote configuration from.
#[derive(Clone)]
pub enum ConfigSource {
    Local(PathBuf),
    Remote {
        // Path to the repository on the remote host
        repository_path: String,
        workspace: Workspace,
        builder: UrlBuilder,
        global_git_config: PathBuf,
        branch_name: String,
    },
}

impl ConfigSource {
    /// return the path containing Lorry configuration files
    pub fn path(&self) -> PathBuf {
        match self {
            ConfigSource::Local(path_buf) => path_buf.clone(),
            ConfigSource::Remote {
                workspace,
                repository_path: _,
                builder: _,
                global_git_config: _,
                branch_name: _,
            } => workspace.repository_path(),
        }
    }
}

/// ConfigPoller waits for new configuration to be loaded from the remote source
/// and then populates the database accordingly.
pub struct DBConfigReader {
    pub db: StateDatabase,
    pub config: ConfigSource,
}

impl DBConfigReader {
    async fn add_matching_lorries_to_statedb(
        &self,
        config: LorryConfig,
    ) -> Result<std::collections::HashSet<super::comms::LorryPath>, ReadConfigError> {
        let config_dir = self.config.path();
        let (filenames, errors): (Vec<_>, Vec<_>) = config
            .globs
            .into_iter()
            .map(|i| config_dir.join(i))
            .map(|s| glob::glob(s.to_str().expect("Path cannot be globbed over...")))
            .flat_map(|s| s.expect("globbing failed"))
            .partition(Result::is_ok);
        tracing::debug!("filenames: {:?}", filenames);
        tracing::debug!("failed to read files: {:?}", errors);

        // File names is safe to unwrap now due to partitioning above
        let filenames = filenames.into_iter().map(|f| f.unwrap()).collect();

        // TODO sort `filenames` rather than just iterating over it. Then again, is that strictly necessary?
        let mut lorry_specs = read_spec_files(filenames, config.lfs)?;

        tracing::debug!("read lorry specs: {:?}", lorry_specs);

        // TODO do we actually need to sort lorry_specs? If we don't need to, we can avoid the `collect` call
        lorry_specs.sort_by(|(s1, _), (s2, _)| s1.cmp(s2));

        let interval = config.interval;
        // If the maintenance interval is not specified in the Lorry config
        // run fsck and gc roughly every 170 mirror opperations which would
        // be roughly every week on an hourly interval.
        let maintenance = config.maintenance.unwrap_or(interval * 170);
        let timeout = config.timeout;
        let prefix = &config.prefix;
        let added_paths = lorry_specs.into_iter().map(|(subpath, obj)| async move {
            let path = super::comms::LorryPath::new(format!("{}/{}", &prefix, subpath));

            let mut b = std::collections::BTreeMap::new();
            b.insert(&path, obj);
            let spec = b;
            self.db
                .add_to_lorries(&path, &spec, interval, maintenance, timeout)
                .await?;
            Ok::<_, ReadConfigError>(path)
        });

        let added_paths = futures::future::join_all(added_paths)
            .await
            .into_iter()
            .collect::<Result<Vec<_>, _>>()?;

        Ok(added_paths
            .into_iter()
            .collect::<std::collections::HashSet<_>>())
    }

    /// Pull configuration from the remote configuration repository and update configuration based on it.
    pub async fn read_config(&self) -> Result<(), ReadConfigError> {
        match &self.config {
            ConfigSource::Local(_) => {}
            ConfigSource::Remote {
                repository_path,
                workspace,
                builder,
                global_git_config,
                branch_name,
            } => {
                let authenticated_url = builder.build(repository_path)?;
                update_confgit(
                    workspace,
                    global_git_config.as_path(),
                    &authenticated_url,
                    branch_name,
                )
                .await?;
            }
        };
        let conf_dir = self.config.path();
        let config_obj = read_config_file(conf_dir.as_path())?;
        let lorries_to_remove = self.db.get_lorries_paths().await?;
        let mut m = std::collections::HashSet::new();
        for i in lorries_to_remove {
            m.insert(i);
        }
        let mut lorries_to_remove = m;

        for section in config_obj {
            let Config::Lorries(lorries) = section;
            let added = self.add_matching_lorries_to_statedb(lorries).await?;
            lorries_to_remove = &lorries_to_remove - &added;
        }

        futures::future::join_all(
            lorries_to_remove
                .iter()
                .map(|path| async { self.db.remove_lorry(path).await }),
        )
        .await
        .into_iter()
        .collect::<Result<Vec<_>, _>>()
        .map_err(ReadConfigError::FailedToRemoveLorries)?;
        Ok(())
    }
}

#[async_trait]
impl ConfigReader for DBConfigReader {
    async fn read(&self) -> Result<(), ReadConfigError> {
        // TODO: When not using a remote git repository for configuration inotify
        // style file watching should be used instead of polling.
        self.read_config().await?;
        Ok(())
    }
}

/// Attempt to extract configurations for many specific Lorries held in the
/// files passed in
///
/// Makes use of [get_valid_lorry_specs] to load each config file
#[allow(deprecated)]
fn read_spec_files(
    filenames: Vec<PathBuf>,
    config_lfs: Option<bool>,
) -> Result<Vec<(String, LorrySpec)>, ReadConfigError> {
    let mut errors = vec![];
    let specs = filenames
        .into_iter()
        .filter_map(|filename| {
            tracing::debug!(
                "Reading .lorry file: {:?}",
                filename.canonicalize().expect("unwrap issue")
            );
            get_valid_lorry_specs(filename)
                .map_err(|e| errors.push(e))
                .ok()
        })
        .flat_map(|b| b.into_iter())
        .map(|(name, mut spec)| {
            if let LorrySpec::Git(ref mut single_lorry) = spec {
                if let Some(refspecs) = single_lorry.refspecs.as_ref() {
                    tracing::debug!("Deprecated refspecs field in use, use ref_patterns instead. This will be removed in a future version of Lorry");
                    single_lorry.ref_patterns = Some(refspecs.clone());
                }
                if single_lorry.lfs.is_none() {
                    single_lorry.lfs = config_lfs;
                }
            }
            (name, spec)
        })
        .collect();

    match errors.is_empty() {
        true => Ok(specs),
        false => Err(ReadConfigError::MultipleReadFails { errors }),
    }
}

/// Attempt to extract the configuration for a specific Lorry from the input
/// file path
fn get_valid_lorry_specs(
    filename: PathBuf,
) -> Result<BTreeMap<String, workerlib::LorrySpec>, ReadConfigError> {
    let s = std::fs::read_to_string(&filename)
        .map_err(|e| ReadConfigError::IOError(filename.clone(), e))?;
    workerlib::extract_lorry_specs(s).map_err(|e| ReadConfigError::SpecParse(filename, e))
}

/// Attempt to read the top level Lorry configuration files (not the
/// individual configuration files)
pub fn read_config_file(configuration_directory: &Path) -> Result<Vec<Config>, ReadConfigError> {
    let filename = configuration_directory.join(CONFIG_FILENAME);
    tracing::debug!("Reading configuration file {:?}", filename);

    match std::fs::read_to_string(&filename) {
        Ok(s) => match serde_yaml::from_str::<Vec<Config>>(&s) {
            Ok(j) => Ok(j),
            Err(e) => {
                tracing::error!("Error parsing config: {}", e);
                Err(ReadConfigError::SpecParse(filename.to_path_buf(), e))
            }
        },
        Err(e) => {
            tracing::error!(
                "There was an error accessing the lorry-controller.conf, is your config repo correctly formed?: {}", e);
            Err(ReadConfigError::IOError(filename.to_path_buf(), e))
        }
    }
}

/// Resolve the global configuration into all its underlying Lorry specs
pub fn resolve(configuration_directory: &Path) -> Result<Resolved, ReadConfigError> {
    let config = read_config_file(configuration_directory)?;
    let mut results: Vec<(Config, BTreeMap<String, LorrySpec>)> = Vec::with_capacity(config.len());
    for cfg in config {
        let Config::Lorries(lorry_cfg) = cfg;
        let mut inner: BTreeMap<String, LorrySpec> = BTreeMap::new();
        for pattern in lorry_cfg.globs.iter() {
            let config_dir = configuration_directory.to_str().unwrap().to_string();
            let target_glob = format!("{}/{}", config_dir, pattern);
            let specs = glob::glob(&target_glob)?;
            for path in specs {
                let path = path?;
                let spec = read_to_string(path.as_path())
                    .map_err(|e| ReadConfigError::IOError(path.to_path_buf(), e))?;
                let lorry_spec = extract_lorry_specs(spec)
                    .map_err(|e| ReadConfigError::SpecParse(path.to_path_buf(), e))?;
                inner.extend(lorry_spec);
            }
        }
        results.push((Config::Lorries(lorry_cfg), inner));
    }
    Ok(results)
}

async fn update_confgit(
    workspace: &Workspace,
    git_config_path: &Path,
    authenticated_url: &Url,
    branch_name: &str,
) -> Result<(), ReadConfigError> {
    // fetch confgit source
    execute(
        &commands::FetchConfgit {
            target_url: authenticated_url,
            git_config_path,
            branch_name,
        },
        &workspace.repository_path(),
    )
    .await?;
    // checkout correct branch
    execute(
        &commands::CheckoutBranch {
            branch_name,
            git_config_path,
        },
        &workspace.repository_path(),
    )
    .await?;

    // Reset working path
    execute(
        &commands::ResetHard {
            branch_name,
            git_config_path,
        },
        &workspace.repository_path(),
    )
    .await?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use rand::{distributions::Alphanumeric, thread_rng, Rng};
    use std::fs::{create_dir_all, remove_dir_all, write};
    use std::io::Write;
    use tempfile::NamedTempFile;

    #[test]
    fn test_config_resolve() {
        let test_dir_name: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(12)
            .map(char::from)
            .collect();
        let test_dir_path = format!("./test-data/{}", test_dir_name);
        let test_dir = Path::new(&test_dir_path);

        create_dir_all(test_dir).unwrap();

        let git_config_path = test_dir.join("lorry.gitconfig");
        let git_config = workerlib::git_config::Config(git_config_path.clone());
        git_config
            .setup(
                "hello@example.org",
                1,
                false,
                None,
                Path::new("/dev/null"),
                None,
            )
            .unwrap();

        write(
            Path::new(&test_dir_path).join(Path::new("lorry-controller.conf")),
            indoc! {r#"
                [
                    {
                        "type": "lorries",
                        "interval": "PT1H",
                        "timeout": "PT1H",
                        "prefix": "lorry-mirrors/hello",
                        "globs": [
                            "hello.lorry"
                        ]
                    },
                    {
                        "type": "lorries",
                        "interval": "PT1M",
                        "timeout": "PT1H",
                        "prefix": "lorry-mirrors/world",
                        "globs": [
                            "world.lorry"
                        ]
                    },
                    {
                        "type": "lorries",
                        "interval": "PT1M",
                        "timeout": "PT1H",
                        "prefix": "lorry-mirrors/git",
                        "globs": [
                            "git.lorry"
                        ],
                        "lfs": true
                    }
                ]
            "#},
        )
        .unwrap();

        write(
            Path::new(&test_dir_path).join(Path::new("hello.lorry")),
            indoc! {r#"
                raw-files1:
                    type: raw-file
                    urls:
                    - destination: hello/1.0 
                      url: https://example.org/hello-1.0.tar.xz
            "#},
        )
        .unwrap();

        write(
            Path::new(&test_dir_path).join(Path::new("world.lorry")),
            indoc! {r#"
                raw-files2:
                    type: raw-file
                    urls:
                    - destination: world/1.0 
                      url: https://example.org/world-1.0.tar.xz
            "#},
        )
        .unwrap();

        write(
            Path::new(&test_dir_path).join(Path::new("git.lorry")),
            indoc! {r#"
                git_mirror:
                    type: git
                    url: https://some.url
                    lfs: true
            "#},
        )
        .unwrap();

        let resolved_config = resolve(Path::new(&test_dir_path)).unwrap();
        assert!(resolved_config.len() == 3);

        let first = resolved_config.first().unwrap();
        let raw_files1 = first.1.get("raw-files1").unwrap();
        match raw_files1 {
            LorrySpec::RawFiles(files) => {
                let file_spec = files.urls.first().unwrap();
                assert!(file_spec.url.to_string() == "https://example.org/hello-1.0.tar.xz")
            }
            _ => panic!("wrong lorry spec type"),
        }

        let git_mirror = &resolved_config[2];
        match &git_mirror.0 {
            Config::Lorries(spec) => assert!(spec
                .lfs
                .expect("failed to detect that lfs was specified in lorry-controller.conf")),
        };
        match git_mirror.1.get("git_mirror").unwrap() {
            LorrySpec::Git(spec) => {
                assert!(spec
                    .lfs
                    .expect("failed to detect that lfs was specified in single lorry spec"));
            }
            _ => panic!("wrong lorry spec type"),
        }

        remove_dir_all(test_dir_path).unwrap()
    }

    /// Check that lfs option in single lorry specs takes precedence over the
    /// global config
    #[test]
    fn test_lfs_resolve() {
        let mut with_lfs = NamedTempFile::new().unwrap();
        with_lfs
            .write_all(indoc! {br#"
                with_lfs:
                    type: git
                    url: https://some.url
                    lfs: true
            "#})
            .unwrap();
        let with_lfs = with_lfs.path().to_owned();

        let mut without_lfs = NamedTempFile::new().unwrap();
        without_lfs
            .write_all(indoc! {br#"
                without_lfs:
                    type: git
                    url: https://some.url
            "#})
            .unwrap();
        let without_lfs = without_lfs.path().to_owned();

        let specs = read_spec_files(vec![with_lfs, without_lfs], Some(false)).unwrap();
        assert_eq!(specs.len(), 2);

        match &specs[0].1 {
            LorrySpec::Git(spec) => assert!(spec.lfs.unwrap()),
            _ => panic!("Wrong lorry spec"),
        };

        match &specs[1].1 {
            LorrySpec::Git(spec) => assert!(!spec.lfs.unwrap()),
            _ => panic!("Wrong lorry spec"),
        };
    }
}
