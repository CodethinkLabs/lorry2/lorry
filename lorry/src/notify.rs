use std::str::FromStr;
use std::time::Duration;

use askama::Template;
use chrono::Utc;
use fancy_regex::Regex;
use lettre::{
    error::Error as LettreError,
    message::{header::ContentType, Mailbox},
    transport::smtp::{
        authentication::{Credentials, Mechanism},
        client::Tls,
    },
    Address, Message, SmtpTransport, Transport,
};
use url::Url;

use crate::state_db::{models::JobStatus, StateDatabase};

const DEFAULT_SMTP_PORT: u16 = 25;
const DEFAULT_SMTP_TIMEOUT_SECS: u64 = 20;

#[derive(Template)]
#[template(path = "resolved.txt")]
pub struct Resolved {
    job_url: String,
    path: String,
    timestamp: String,
}

#[derive(Template)]
#[template(path = "refs_failed.txt")]
pub struct RefsFailed {
    job_url: String,
    path: String,
    timestamp: String,
}

#[derive(Template)]
#[template(path = "failed.txt")]
pub struct Failed {
    job_url: String,
    path: String,
    output: Option<String>,
    timestamp: String,
}

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Smtp Error: {0}")]
    Smtp(#[from] LettreError),
    #[error("Failed to send message: {0}")]
    Transport(#[from] lettre::transport::smtp::Error),
    #[error("Cannot parse recipient address: {0}")]
    Address(#[from] lettre::address::AddressError),
    #[error("Bad regular expression: {0}")]
    Regexp(#[from] fancy_regex::Error),
}

struct RecipientMatcher {
    groups: Vec<(Regex, Address)>,
}

impl RecipientMatcher {
    pub fn new(groups: &[(String, String)]) -> Result<Self, Error> {
        Ok(RecipientMatcher {
            groups: groups.iter().try_fold(Vec::new(), |mut accm, group| {
                let address = Address::from_str(&group.0)?;
                let regex = Regex::new(&group.1)?;
                accm.push((regex, address));
                Ok::<Vec<(Regex, Address)>, Error>(accm)
            })?,
        })
    }

    pub fn recipients(&self, lorry_path: &str) -> Vec<Address> {
        self.groups.iter().fold(Vec::new(), |mut accm, group| {
            if group
                .0
                .is_match(lorry_path)
                .is_ok_and(|has_match| has_match)
                && !accm.contains(&group.1)
            {
                accm.push(group.1.clone());
            };
            accm
        })
    }
}

#[derive(Default)]
pub struct Builder {
    pub endpoint: String,
    pub port: Option<u16>,
    pub tls: Option<Tls>,
    pub credentials: Option<Credentials>,
    pub from_address: String,
    pub lorry_url_base: String,
    pub alert_groups: Vec<(String, String)>,
    pub alert_threshold: Option<u32>,
}

impl Builder {
    pub fn endpoint(mut self, endpoint: &str) -> Self {
        self.endpoint = endpoint.to_string();
        self
    }

    pub fn port(mut self, port: u16) -> Self {
        self.port = Some(port);
        self
    }

    pub fn tls(mut self, tls_config: Tls) -> Self {
        self.tls = Some(tls_config);
        self
    }

    pub fn credentials(mut self, username: &str, password: &str) -> Self {
        self.credentials = Some(Credentials::new(username.to_string(), password.to_string()));
        self
    }

    /// The domain name used when constructing links in e-mail notifications
    pub fn lorry_url_base(mut self, domain: &str) -> Self {
        self.lorry_url_base = domain.to_string();
        self
    }

    pub fn alert_groups(mut self, groups: &[(String, String)]) -> Self {
        self.alert_groups = groups.to_vec();
        self
    }

    pub fn alert_threshold(mut self, threshold: u32) -> Self {
        self.alert_threshold = Some(threshold);
        self
    }

    /// Domain part of the address from which Lorry sends e-mails, i.e.
    /// lorry@<domain_name>
    #[allow(clippy::wrong_self_convention)]
    pub fn from_address(mut self, from_address: &str) -> Self {
        self.from_address = from_address.to_string();
        self
    }

    pub fn build(&self) -> Result<Notifier, Error> {
        let mut builder = SmtpTransport::builder_dangerous(&self.endpoint)
            .timeout(Some(Duration::from_secs(DEFAULT_SMTP_TIMEOUT_SECS)))
            .port(self.port.unwrap_or(DEFAULT_SMTP_PORT));
        if let Some(tls_config) = &self.tls {
            builder = builder.tls(tls_config.clone());
        }
        if let Some(credentials) = &self.credentials {
            builder = builder.credentials(credentials.clone());
            // NOTE: There is only support for plain auth
            builder = builder.authentication(vec![Mechanism::Plain]);
        }
        let mail_from = Address::from_str(&self.from_address)?;
        let matcher = RecipientMatcher::new(self.alert_groups.as_slice())?;
        Ok(Notifier {
            this_domain: Url::parse(&self.lorry_url_base).unwrap(),
            transport: builder.build(),
            mail_from: mail_from.into(),
            matcher,
            alert_threshold: self.alert_threshold.unwrap_or(3),
        })
    }
}

/// Send e-mail notifications to the targeted recipients
pub struct Notifier {
    this_domain: Url,
    transport: SmtpTransport,
    mail_from: Mailbox,
    matcher: RecipientMatcher,
    alert_threshold: u32,
}

impl Notifier {
    pub fn builder() -> Builder {
        Builder::default()
    }

    async fn send(
        &self,
        job_id: i64,
        path: &str,
        status: &JobStatus,
        output: Option<&String>,
    ) -> Result<(), Error> {
        let mut job_url = self.this_domain.clone();
        let timestamp = Utc::now().to_string();
        job_url.set_path(&format!("/job/{}", job_id));
        let body = match status {
            JobStatus::None => unreachable!(),
            JobStatus::Successful => {
                Resolved {
                    job_url: job_url.to_string(),
                    path: path.to_string(),
                    timestamp,
                }
            }
            .render()
            .unwrap(),
            JobStatus::Failed => Failed {
                job_url: job_url.to_string(),
                path: path.to_string(),
                output: output.map(workerlib::redact::redact),
                timestamp,
            }
            .render()
            .unwrap(),
            JobStatus::RefsFailed => RefsFailed {
                job_url: job_url.to_string(),
                path: path.to_string(),
                timestamp,
            }
            .render()
            .unwrap(),
        };
        let subject = match status {
            JobStatus::None => unreachable!(),
            JobStatus::Successful => format!("Mirror Status Resolved: {}", path),
            JobStatus::Failed => format!("Mirror Failure: {}", path),
            JobStatus::RefsFailed => format!("Mirror has Failed Refs: {}", path),
        };
        let recipients = self.matcher.recipients(path);
        tracing::info!("Sending {} notifications", recipients.len());
        for (i, address) in recipients.iter().enumerate() {
            tracing::info!("Sending notification {}/{}", i + 1, recipients.len());
            let message = Message::builder()
                .from(self.mail_from.clone())
                .to(Mailbox::new(None, address.clone()))
                .subject(subject.clone())
                .header(ContentType::TEXT_PLAIN)
                .body(body.clone())
                .unwrap();
            self.transport.send(&message)?;
        }
        Ok(())
    }
}

/// DbNotifier polls the database searching for new notifications to send
pub struct DbNotifier(pub StateDatabase, pub Notifier);

impl DbNotifier {
    pub async fn poll(&self) -> Result<(), sqlx::Error> {
        let mut ticker = tokio::time::interval(Duration::from_secs(60));
        loop {
            ticker.tick().await;
            let notifications = self.0.read_notifications(self.1.alert_threshold).await?;
            for notification in notifications.iter() {
                match self
                    .1
                    .send(
                        notification.job_id,
                        &notification.path,
                        &notification.status,
                        notification.output.as_ref(),
                    )
                    .await
                {
                    Ok(_) => {
                        self.0.mark_sent(notification.id).await?;
                        tracing::info!("Notification sent successfully");
                    }
                    Err(err) => {
                        tracing::error!("Could not send notification via SMTP: {}", err);
                    }
                };
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn matcher() {
        let matcher = RecipientMatcher::new(&[
            (String::from("bar@example.org"), String::from("/baz/*")),
            (String::from("fallback@example.org"), String::from(".*")),
        ])
        .unwrap();
        assert!(matcher.recipients("not-gonna-match/aaaa").len() == 1);
        assert!(
            matcher
                .recipients("not-gonna-match/aaaa")
                .first()
                .unwrap()
                .to_string()
                == "fallback@example.org"
        );
        assert!(matcher.recipients("/baz/qux").first().unwrap().to_string() == "bar@example.org");
    }
}
