//! TODO: Implement Local Downstream Support Again

use crate::comms;

#[derive(thiserror::Error, Debug)]
pub(crate) enum PrepareLocalRepoError {}

/// Information about the local downstream used for testing
#[derive(Clone, Debug)]
pub struct LocalDownstream {}

impl LocalDownstream {
    pub(crate) async fn prepare_repo(
        &self,
        _repo_path: &comms::LorryPath,
        _metadata: crate::downstream::RepoMetadata,
    ) -> Result<(), PrepareLocalRepoError> {
        unimplemented!("Local Downstream is not currently supported")
    }
}
