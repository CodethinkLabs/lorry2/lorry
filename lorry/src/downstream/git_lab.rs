//!
//! Manage GitLab downstreams.
//!

use crate::comms;
use crate::downstream;
use downstream::Visibility;
use futures::StreamExt;
use futures::TryStreamExt;
use gitlab::api::groups::CreateGroupBuilderError;
use gitlab::api::groups::GroupBuilderError;
use gitlab::api::projects::repository::branches::CreateBranchBuilderError;
use gitlab::api::projects::CreateProjectBuilderError;
use gitlab::api::projects::EditProjectBuilderError;
use gitlab::api::projects::ProjectBuilderError;
use gitlab::api::ApiError;
use gitlab::api::AsyncQuery;
use gitlab::AsyncGitlab;
use gitlab::GitlabError;
use gitlab::RestError;
use std::path::PathBuf;
use tokio::fs;
use types::Group;
use url::Url;

/// Implements minimal structs needed to construct projects and group
/// namespaces in downstream gitlab.
mod types {
    use serde::Deserialize;
    #[derive(Deserialize)]
    pub struct Group {
        pub id: u64,
        pub full_path: String,
    }

    #[derive(Deserialize)]
    pub struct Project {
        pub id: u64,
        pub description: Option<String>,
        pub default_branch: Option<String>,
    }

    #[derive(Deserialize)]
    pub struct RepoBranch {}
}

/// Errors that can arise when trying to prepare a downstream GitLab repo
#[derive(thiserror::Error, Debug)]
pub(crate) enum PrepareRepoError {
    #[error("Project path contains less than two components: This would attempt to create a project outside a group.")]
    ProjectPathTooShort,

    #[error("Failure building GitLab client: {0}")]
    GitlabBuilder(#[from] GitlabError),

    #[error("Failure preparing project builder: {0}")]
    GitlabProjectBuilder(#[from] ProjectBuilderError),

    #[error("Failure preparing create branch builder: {0}")]
    GitlabCreateBranchBuilder(#[from] CreateBranchBuilderError),

    #[error("Failure preparing create project builder: {0}")]
    GitlabCreateProjectBuilder(#[from] CreateProjectBuilderError),

    #[error("Failure preparing edit project builder: {0}")]
    GitlabEditProjectBuilder(#[from] EditProjectBuilderError),

    #[error("Failure preparing create group builder: {0}")]
    GitlabCreateGroupBuilder(#[from] CreateGroupBuilderError),

    #[error("Failure preparing group builder: {0}")]
    GitlabGroupBuilder(#[from] GroupBuilderError),

    #[error("Failure in API: {0}")]
    GitlabApi(#[from] ApiError<RestError>),

    #[error("Could not read token file: {0}")]
    TokenFileCouldNotBeRead(#[from] std::io::Error),
}

trait GitlabErrorReport {
    fn report(self, why: impl Into<String>) -> Self;
}

impl<T, E> GitlabErrorReport for Result<T, E>
where
    E: std::error::Error,
{
    fn report(self, why: impl Into<String>) -> Self {
        if let Err(e) = &self {
            let why = why.into();
            tracing::warn!("{why} - {e} - {e:?}");
        }
        self
    }
}

#[derive(Debug, Clone)]
pub enum TokenSource {
    Token(String),
    Path(PathBuf),
}

/// Information about the downstream GitLab
#[derive(Clone, Debug)]
pub struct GitLabDownstream {
    pub downstream_visibility: gitlab::api::common::VisibilityLevel,
    pub token_source: TokenSource,
    pub git_url: Url,
    pub insecure: bool,
}

/// Methods to setup the GitLab downstream ready to receive upstream mirrors
impl GitLabDownstream {
    pub fn new(
        hostname: Url,
        downstream_visibility: Visibility,
        private_token: Option<String>,
        token_file: Option<PathBuf>,
        insecure: bool,
    ) -> Self {
        Self {
            downstream_visibility: downstream_visibility.into(),
            git_url: hostname,
            token_source: if let Some(t) = private_token {
                TokenSource::Token(t)
            } else if let Some(f) = token_file {
                TokenSource::Path(f)
            } else {
                panic!("No token source given")
            },
            insecure,
        }
    }

    /// Creates a new gitlab API client
    async fn get_current_client(&self) -> Result<AsyncGitlab, PrepareRepoError> {
        let token = match &self.token_source {
            TokenSource::Token(t) => t.to_owned(),
            TokenSource::Path(p) => fs::read_to_string(p)
                .await
                .map_err(PrepareRepoError::TokenFileCouldNotBeRead)?
                .trim()
                .to_string(),
        };
        if self.insecure {
            tracing::debug!("url: {}", self.git_url.authority());
            Ok(gitlab::GitlabBuilder::new(self.git_url.authority(), token)
                .insecure()
                .build_async()
                .await
                .report("Error creating client")?)
        } else {
            Ok(gitlab::GitlabBuilder::new(self.git_url.authority(), token)
                .build_async()
                .await
                .report("Error creating client")?)
        }
    }

    /// Edit project description to match upstream repo
    async fn update_description(
        &self,
        repo_path: &comms::LorryPath,
        metadata: &downstream::RepoMetadata,
        gitlab_instance: &AsyncGitlab,
    ) -> Result<(), PrepareRepoError> {
        if let Some(new_description) = &metadata.description {
            match gitlab::api::projects::EditProject::builder()
                .project(repo_path)
                .description(new_description)
                .build()
                .report(format!(
                    "building editproject for {repo_path}: {new_description}"
                )) {
                Ok(query) => {
                    query
                        .query_async(gitlab_instance)
                        .await
                        .report(format!(
                            "running editproject for {repo_path}: {new_description}"
                        ))
                        // Handle errors by reporting and continuing on; updating
                        // project description is not critical!
                        .map_or_else(
                            |e| {
                                tracing::warn!(
                                    "Failed to update description for {}: {e}",
                                    repo_path
                                )
                            },
                            |_: types::Project| (),
                        );
                }
                Err(e) => tracing::warn!("Failed to create update query for {}: {e}", repo_path),
            }
            Ok(())
        } else {
            Ok(())
        }
    }

    /// Create new branch on Gitlab project
    async fn create_branch(
        &self,
        repo_path: &comms::LorryPath,
        new_head: &str,
        gitlab_instance: &AsyncGitlab,
    ) -> Result<(), PrepareRepoError> {
        match gitlab::api::projects::repository::branches::CreateBranch::builder()
            .project(repo_path)
            .branch(new_head)
            .build()
            .report(format!("building createbranch {repo_path}:{new_head}"))
        {
            Ok(q) => {
                let _branch: Option<types::RepoBranch> = q
                    .query_async(gitlab_instance)
                    .await
                    .report(format!("running createbranch {repo_path}:{new_head}"))
                    .ok();
            }
            Err(e) => tracing::warn!(
                "Failed to create query to create a branch for {}: {e}",
                repo_path
            ),
        };
        Ok(())
    }

    /// Edit project and set default branch
    async fn edit_project(
        &self,
        repo_path: &comms::LorryPath,
        new_head: String,
        gitlab_instance: &AsyncGitlab,
    ) -> Result<(), PrepareRepoError> {
        match gitlab::api::projects::EditProject::builder()
            .project(repo_path)
            .default_branch(&new_head)
            .build()
            .report(format!("building editproject {repo_path}:{new_head}"))
        {
            Ok(q) => {
                let _proj: Option<types::Project> = q
                    .query_async(gitlab_instance)
                    .await
                    .report(format!("running editproject {repo_path}:{new_head}"))
                    .ok();
            }
            Err(e) => tracing::warn!(
                "Failed to create query to set default branch for {}: {e}",
                repo_path
            ),
        };
        Ok(())
    }

    /// Finalise creating project
    async fn create_project(
        &self,
        repo_path: &comms::LorryPath,
        metadata: downstream::RepoMetadata,
        gitlab_instance: &AsyncGitlab,
        path_components: Vec<&str>,
    ) -> Result<types::Project, PrepareRepoError> {
        // create the group tree and return the id of the parent group for this project.
        let namespace_id = self
            .build_group_tree(&path_components, gitlab_instance)
            .await?;

        let created_project: types::Project = {
            let existing_project: Result<types::Project, _> =
                gitlab::api::projects::Project::builder()
                    .project(repo_path)
                    .build()
                    .report(format!("building project for get {repo_path}"))?
                    .query_async(gitlab_instance)
                    .await
                    .report(format!("getting project {repo_path}"));
            match existing_project {
                Ok(ex) => Ok(ex),
                Err(_) => {
                    let mut b = gitlab::api::projects::CreateProject::builder();
                    b.name(*(path_components.last().unwrap()))
                        .visibility(self.downstream_visibility)
                        .namespace_id(namespace_id);
                    b.path(*(path_components.last().unwrap()));

                    // TODO perhaps supply worker::DEFAULT_BRANCH_NAME if metadata.head is None? But that would not line up with the upstream...
                    if let Some(head) = metadata.head {
                        // Create the branch before we set it as default, or we'll get an error
                        self.create_branch(repo_path, &head, gitlab_instance)
                            .await?;
                        b.default_branch(head);
                    }

                    metadata
                        .description
                        .map(|description| b.description(description));
                    b.pages_access_level(gitlab::api::projects::FeatureAccessLevelPublic::Disabled)
                        .container_registry_enabled(false)
                        .autoclose_referenced_issues(false)
                        .lfs_enabled(true)
                        .auto_devops_enabled(false);

                    b.build()
                        .report(format!("build createproject for new project {repo_path}"))?
                        .query_async(gitlab_instance)
                        .await
                        .report(format!("run createproject for new project {repo_path}"))
                }
            }
        }
        .report(format!("acquire created project {repo_path}"))?;
        Ok(created_project)
    }

    /// Set various Gitlab features to disabled
    async fn set_access_levels(
        &self,
        created_project: types::Project,
        repo_path: &comms::LorryPath,
        gitlab_instance: &AsyncGitlab,
    ) -> Result<(), PrepareRepoError> {
        let _: types::Project = gitlab::api::projects::EditProject::builder()
            .project(created_project.id)
            .issues_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
            .merge_requests_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
            .builds_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
            .wiki_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
            .snippets_access_level(gitlab::api::projects::FeatureAccessLevel::Disabled)
            .build()
            .report(format!("building editproject for {repo_path}"))?
            .query_async(gitlab_instance)
            .await
            .report(format!("Running editproject for {repo_path}"))?;
        Ok(())
    }

    /// Setup up downstream GitLab repos into which upstream Git repos and raw
    /// files can be mirrored
    #[tracing::instrument(skip_all)]
    pub(crate) async fn prepare_repo(
        &self,
        repo_path: &comms::LorryPath,
        metadata: downstream::RepoMetadata,
    ) -> Result<(), PrepareRepoError> {
        let gitlab_instance = self.get_current_client().await?;

        match gitlab::api::projects::Project::builder()
            .project(repo_path)
            .build()
            .report(format!("building project {repo_path}"))?
            .query_async(&gitlab_instance)
            .await
            .report(format!("querying project {repo_path}"))
        {
            Ok::<types::Project, _>(project) => {
                tracing::debug!("Project {repo_path} exists in local GitLab already.");
                //Update the description to match that from the upstream repo, if necessary
                if metadata.description != project.description {
                    self.update_description(repo_path, &metadata, &gitlab_instance)
                        .await?;
                }

                // Set if default branch exists
                if project.default_branch.is_some() {
                    if metadata.head != project.default_branch && metadata.head.is_some() {
                        let new_head = metadata.head.unwrap();
                        // Create the branch in case it does not already exist
                        self.create_branch(repo_path, &new_head, &gitlab_instance)
                            .await?;

                        // Edit existing project
                        self.edit_project(repo_path, new_head, &gitlab_instance)
                            .await?;
                        Ok(())
                    } else {
                        // Either the downstream and upstream default branches
                        // agree, which is good - or, the upstream default
                        // branch is not set.
                        // TODO: Seriously what would that case mean? Is it even possible?
                        Ok(())
                    }
                } else {
                    // The downstream has no default branch. That's OK, this means we
                    // haven't pulled from upstream yet. Once we do we'll get a default branch.
                    // return OK on the basis that we're about to pull to the downstream,
                    Ok(())
                }
            }

            Err(e) => {
                tracing::info!("Need to create {repo_path} on local GitLab due to err: {e}");
                let path_components = repo_path.components();

                if path_components.len() < 2 {
                    tracing::error!("Project path {repo_path} is too short!");
                    return Err(PrepareRepoError::ProjectPathTooShort);
                }

                let created_project = self
                    .create_project(repo_path, metadata, &gitlab_instance, path_components)
                    .await?;
                tracing::info!("Created {} project in local Gitlab.", repo_path);

                self.set_access_levels(created_project, repo_path, &gitlab_instance)
                    .await?;
                tracing::info!("Set up fully {} project in local Gitlab.", repo_path);
                Ok(())
            }
        }
    }

    #[tracing::instrument(skip_all)]
    async fn create_group_if_missing(
        &self,
        group_path: &str,
        group_name: &str,
        parent_group: Group,
        gitlab_instance: &AsyncGitlab,
    ) -> Result<Group, PrepareRepoError> {
        match gitlab::api::groups::Group::builder()
            .group(group_path)
            .build()
            .report(format!("building group get for {group_path}"))?
            .query_async(gitlab_instance)
            .await
            .report(format!("running group get for {group_path}"))
        {
            Ok::<Group, _>(g) => {
                tracing::debug!("Group {} was found!", &g.full_path);
                Ok(g)
            }
            // Used to check for ApiError::GitlabService, but that was failing in testing.
            // TODO: "group not found" error exists for multiple enum variants, so we
            // are behaving as if all gitlab::api::ApiError::Gitlab mean the group was not found.
            Err(e) => match e {
                // If group does not exist create new group
                gitlab::api::ApiError::Gitlab { msg: _ } => {
                    tracing::debug!("trying to create group {group_path}...");
                    let new_group: Group = {
                        let mut b = gitlab::api::groups::CreateGroup::builder();
                        b.name(group_name);
                        b.path(group_name);
                        if !parent_group.full_path.is_empty() {
                            // a parent group for this path exists so create a subgroup
                            b.parent_id(parent_group.id);
                        }
                        b.visibility(self.downstream_visibility);

                        b.build()
                            .report(format!("Building creategroup for {group_name}"))?
                            .query_async(gitlab_instance)
                            .await
                            .report(format!("Running creategroup for {group_name}"))
                    }?;
                    Ok(Group {
                        full_path: new_group.full_path,
                        id: new_group.id,
                    })
                }
                e => {
                    tracing::debug!("Got some other critical error:{:?}", e);
                    Err(e)?
                }
            },
        }
    }

    /// Creates the full group tree specified in `path_components` and returns
    /// the group id (u64) of the most nested group which the project belongs
    /// to.    
    #[tracing::instrument(skip_all)]
    async fn build_group_tree(
        &self,
        path_components: &[&str],
        gitlab_instance: &AsyncGitlab,
    ) -> Result<u64, PrepareRepoError> {
        // Convert path components into a stream and exclude final path
        // component as it will be used to construct the project
        let group_names = futures::stream::iter(&path_components[0..(path_components.len() - 1)])
            .map(Ok::<_, PrepareRepoError>);

        // For each path component check if an associated group exists. If not,
        // create a new group. The parent of the current group is stored in the
        // accumulator so that the full path can be constructed for the project.
        let id = group_names
            .try_fold(
                Group {
                    full_path: "".to_string(),
                    id: 0,
                },
                |parent_group: Group, &group_name| async move {
                    let group_path = if parent_group.full_path.is_empty() {
                        group_name.to_string()
                    } else {
                        // construct path from parent path stored in accumulator and current path component
                        format!("{}/{}", parent_group.full_path, group_name)
                    };
                    tracing::debug!("Querying {group_path} to check if path already exists");
                    let group = self
                        .create_group_if_missing(
                            &group_path,
                            group_name,
                            parent_group,
                            gitlab_instance,
                        )
                        .await?;
                    // Save parent group into accumulator for next iteration
                    Ok(group)
                },
                // Return the id of the most nested group.
            )
            .await?
            .id;
        Ok(id)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use mockito::Server;

    #[tokio::test]
    pub async fn prepare_repo_exists() {
        let mut server = Server::new_async().await;

        // gitlab login
        let login = server
            .mock("GET", "/api/v4/user?")
            .with_status(200)
            .create_async()
            .await;

        let project = server
            .mock("GET", "/api/v4/projects/%2Ffuu%2Fbar?")
            .with_status(200)
            .with_body(r#"{"id": 1, "full_path": "/fuu/bar"}"#)
            .create_async()
            .await;

        let downstream = GitLabDownstream {
            downstream_visibility: gitlab::api::common::VisibilityLevel::Public,
            token_source: TokenSource::Token("gplat-FAKE".to_string()),
            git_url: Url::parse(&server.url()).unwrap(),
            insecure: true,
        };

        downstream
            .prepare_repo(
                &comms::LorryPath::new(String::from("/fuu/bar")),
                downstream::RepoMetadata {
                    head: Some(String::from("main")),
                    description: Some(String::from("some really great repo")),
                },
            )
            .await
            .unwrap();

        login.assert();

        project.assert()
    }

    #[tokio::test]
    pub async fn prepare_repo_missing() {
        let mut server = Server::new_async().await;

        let login = server
            .mock("GET", "/api/v4/user?")
            .with_status(200)
            .create_async()
            .await;

        let project = server
            .mock("GET", "/api/v4/projects/fuu%2Fbar?")
            .with_status(404)
            .expect(2)
            .with_body(r#"{"message": "404 Project Not Found"}"#)
            .create_async()
            .await;

        let group_get = server
            .mock("GET", "/api/v4/groups/fuu?")
            .with_status(404)
            .with_body(r#"{"message": "404 Group Not Found"}"#)
            .create_async()
            .await;

        let group_create = server
            .mock("POST", "/api/v4/groups?")
            .with_status(200)
            .with_body(r#"{"id": 123, "full_path": "fuu"}"#)
            .create_async()
            .await;

        let branch_create_main = server
            .mock("POST", "/api/v4/projects/fuu%2Fbar/repository/branches?")
            .with_status(200)
            .with_body(r#"{"branch": "main"}"#)
            .create_async()
            .await;

        let project_create = server
            .mock("POST", "/api/v4/projects?")
            .with_status(200)
            .with_body(r#"{"full_path": "fuu/bar", "id": 123}"#)
            .create_async()
            .await;

        let project_edit = server
            .mock("PUT", "/api/v4/projects/123?")
            .with_status(200)
            .with_body(r#"{"id": 123}"#)
            .create_async()
            .await;

        let downstream = GitLabDownstream {
            downstream_visibility: gitlab::api::common::VisibilityLevel::Public,
            token_source: TokenSource::Token("gplat-FAKE".to_string()),
            git_url: Url::parse(&server.url()).unwrap(),
            insecure: true,
        };

        downstream
            .prepare_repo(
                &comms::LorryPath::new(String::from("fuu/bar")),
                downstream::RepoMetadata {
                    head: Some(String::from("main")),
                    description: Some(String::from("some really great repo")),
                },
            )
            .await
            .unwrap();

        login.assert();
        project.assert();
        group_get.assert();
        group_create.assert();
        branch_create_main.assert();
        project_create.assert();
        project_edit.assert();
    }

    #[tokio::test]
    pub async fn prepare_repo_2_groups_with_1_missing() {
        let mut server = Server::new_async().await;

        let login = server
            .mock("GET", "/api/v4/user?")
            .with_status(200)
            .create_async()
            .await;

        let project = server
            .mock("GET", "/api/v4/projects/alice%2Fbob%2Ffuu?")
            .with_status(404)
            .expect(2)
            .with_body(r#"{"message": "404 Project Not Found"}"#)
            .create_async()
            .await;

        let group_get_alice = server
            .mock("GET", "/api/v4/groups/alice?")
            .with_status(200)
            .with_body(r#"{"id": 1, "full_path": "alice"}"#)
            .create_async()
            .await;

        let group_get_alice_bob = server
            .mock("GET", "/api/v4/groups/alice%2Fbob?")
            .with_status(404)
            .with_body(r#"{"message": "404 Group Not Found"}"#)
            .create_async()
            .await;

        let group_create = server
            .mock("POST", "/api/v4/groups?")
            .with_status(200)
            .with_body(r#"{"full_path": "alice/bob", "id": 2}"#)
            .create_async()
            .await;

        let branch_create_main = server
            .mock(
                "POST",
                "/api/v4/projects/alice%2Fbob%2Ffuu/repository/branches?",
            )
            .with_status(200)
            .with_body(r#"{"branch": "main"}"#)
            .create_async()
            .await;

        let project_create = server
            .mock("POST", "/api/v4/projects?")
            .with_status(200)
            .with_body(r#"{"full_path": "alice/bob/fuu", "id": 3}"#)
            .create_async()
            .await;

        let project_edit = server
            .mock("PUT", "/api/v4/projects/3?")
            .with_status(200)
            .with_body(r#"{"id": 3}"#)
            .create_async()
            .await;

        let downstream = GitLabDownstream {
            downstream_visibility: gitlab::api::common::VisibilityLevel::Public,
            token_source: TokenSource::Token("gplat-FAKE".to_string()),
            git_url: Url::parse(&server.url()).unwrap(),
            insecure: true,
        };

        downstream
            .prepare_repo(
                &comms::LorryPath::new(String::from("alice/bob/fuu")),
                downstream::RepoMetadata {
                    head: Some(String::from("main")),
                    description: Some(String::from("some really great repo")),
                },
            )
            .await
            .unwrap();

        login.assert();
        project.assert();
        group_get_alice.assert();
        group_get_alice_bob.assert();
        group_create.assert();
        branch_create_main.assert();
        project_create.assert();
        project_edit.assert();
    }
}
