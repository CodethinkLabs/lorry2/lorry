//!
//! Manages the dowstreams used by Lorry2.  This currently means managing the
//! local GitLab server that mirrors are pushed to.
//!
//! // NOTE: Downstream is not a trait because dyn compatability with threads
//! // would fight against you in ControllerSettings on calling
//! // prepare repo.

pub mod git_lab;
pub mod local;

use crate::comms;
use git_lab::PrepareRepoError;
use local::PrepareLocalRepoError;

/// Metadata about the mirrored repo. Should be reasonably lowest common denominator
#[derive(Debug, Clone)]
pub struct RepoMetadata {
    /// The branch to use as the default branch of the repo
    pub head: Option<String>,

    /// Repo description to display to viewers
    pub description: Option<String>,
}

/// Connection to a downstream host.
#[derive(Clone, Debug)]
#[allow(dead_code)]
pub enum Downstream {
    GitLab(git_lab::GitLabDownstream),
    Local(local::LocalDownstream),
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum DownstreamError {
    #[error("Failure preparing Gitlab repo: {0}")]
    PrepareGitlabRepo(#[from] PrepareRepoError),

    #[error("Failure preparing local repo: {0}")]
    PrepareLocalRepo(#[from] PrepareLocalRepoError),
}

impl Downstream {
    /// Create the target git repository for a minion to mirror to.
    pub async fn prepare_repo(
        &self,
        repo_path: &comms::LorryPath,
        metadata: RepoMetadata,
    ) -> Result<(), DownstreamError> {
        match self {
            Downstream::GitLab(g) => g.prepare_repo(repo_path, metadata).await?,
            Downstream::Local(l) => l.prepare_repo(repo_path, metadata).await?,
        }
        Ok(())
    }
}

/// Visibility setting to use when creating a repo. While this is host-dependent,
/// the current implementation should be lowest common denominator.
#[derive(clap::ValueEnum, Copy, Clone, Debug, serde::Deserialize, Default)]
pub enum Visibility {
    #[serde(rename = "public")]
    Public,

    #[default]
    #[serde(rename = "internal")]
    Internal,

    #[serde(rename = "private")]
    Private,
}

impl From<Visibility> for gitlab::api::common::VisibilityLevel {
    fn from(value: Visibility) -> Self {
        match value {
            Visibility::Public => gitlab::api::common::VisibilityLevel::Public,
            Visibility::Internal => gitlab::api::common::VisibilityLevel::Internal,
            Visibility::Private => gitlab::api::common::VisibilityLevel::Private,
        }
    }
}
