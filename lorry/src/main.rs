//!
//! Lorry2 mirrors upstream Git repos and raw files into a downstream store.
//!
//! Before anything is run, the Lorry programme needs to be configured
//! (`workerlib::Config`), and what is to be mirrored needs to be specified
//! (`workerlib::ReadConfig`).  A "lorry" outlines how something (e.g. a Git repo) is to
//! be mirrored.  It is specified across a `.yaml` and a `.conf` file:
//!
//! * The `.conf` file specifies: how often it is to be mirrored; the mirror
//!   timeout; the group the downstream repo will be part of; the `.yaml`
//!   files that hold the rest of the information.
//! * The `.yaml` specifies: whether this Lorry is mirroring a raw file, or a
//!   Git repo; the URL of the target; the ref patterns to fetch/ignore (if the
//!   Lorry is for a Git repo).
//!
//! Lorries and jobs are related but conceptually different; a lorry is a
//! record/description of what is to be mirrored.  A job is that mirroring
//! operation actually being run.
//!
//! Lorry can now be run: config is loaded & validated/filtered; the
//! downstream GitLab PAT token is loaded; the [state_db] is setup (migrations run);
//! the web UI is configured & launched; the [workerlib] is configured; the
//! Lorries are loaded from config files into the [state_db]; the
//! [scheduler] configured & run.
//!
//! The [scheduler] is the engine - it uses its poller to poll the [state_db] for the
//! next Lorry that's to be run, and pushes it onto the queue. The scheduler
//! also has workers.  When a worker is ready to do some work, it pops the next
//! Lorry off this shared queue, and lets the poller know it has accepted that
//! Lorry.  The poller uses the [state_db] to issue a job for the Lorry in question and
//! gives this job to the worker.  The worker has a single runner, which
//! implements the `Executor` trait and can thus actaully run the job. This
//! runner makes use of the `workerlib` to actually run the job (i.e. do the
//! mirroring).
//!
//! Jobs carry out the mirroring specified by a Lorry.  This means they fetch a
//! remote Git repo or raw file, and then push this into a [downstream] store.
//! For now this means pushing into a local GitLab server.
//!
//! The [state_db] has been mentioned a few times already.  It stores: lorries;
//! info about the jobs assocated with each lorry; info about refs mirrored by
//! a job; warnings and errors assocated with with each job and its refs.  It's
//! used by the scheduler, as mentioned above, as well as the web UI.
//!
//! The [web] UI displays info about: the Lorry programme; the lorries it's
//! managing; the jobs for each lorry, and associated warnings & errors.  It
//! also allows admins to reschedule & cancel jobs.
//!

use ansi_term::Colour::{Green, Red, Yellow};
use axum::serve;
use clap::{arg, Parser};
use config::Configuration;
use downstream::git_lab::GitLabDownstream;
use notify::{DbNotifier, Notifier};
use read_config::ConfigSource;
use scheduler::ShutdownHandle;
use std::error::Error;
use std::panic::set_hook;
use std::sync::Arc;
use std::{net::SocketAddr, path::PathBuf};
use tokio::net::TcpListener;
use tokio::signal::unix::{signal, SignalKind};
use tokio::sync::Mutex;
use tracing::Level;
use url::Url;
use url_builder::UrlBuilder;
use workerlib::workspace::Workspace;

mod comms;
mod config;
mod downstream;
mod executor;
mod give_job;
mod linter;
mod notify;
mod read_config;
mod scheduler;
mod state_db;
mod url_builder;
mod validator;
mod web;
mod worker;

use crate::config::load;
use crate::executor::DefaultExecutor;
use crate::give_job::{DBJobAllocator, DBRecorder};
use crate::read_config::DBConfigReader;
use crate::state_db::StateDatabase;

/// The default behavior of Lorry is to immediately shutdown anytime a thread
/// panics for any reason.
const FAIL_FAST: bool = true;

/// Lorry Controller
#[derive(Parser)]
struct ControllerCommandLineArguments {
    /// Path to your configuration file
    #[arg(long = "config")]
    config_file: Option<PathBuf>,

    /// Logging level
    #[arg(long = "level")]
    pub log_level: Option<Level>,

    /// Poll the configuration and update the database for scheduling
    #[arg(long = "poll-config", default_value = "true")]
    pub poll_config: Option<bool>,

    /// Process jobs from the database
    #[arg(long = "process-jobs", default_value = "true")]
    pub process_jobs: Option<bool>,

    /// Enable the http interface
    #[arg(long = "enable-http", default_value = "true")]
    pub enable_http: Option<bool>,

    /// Validate a Lorry configuration file
    #[arg(long = "validate")]
    validate: Option<PathBuf>,

    /// Allow duplicate mirrors and raw files in config validation
    #[arg(long = "allow-duplicates", default_value = "false")]
    validate_allow_duplicates: bool,
}

/// The `hostname` parameter in the config can contain only a name and a port
/// number. This checks to make sure no scheme was specified and then converts
/// it to a Url object with `https` as the default scheme.
// It would be nice in the future to check no credentials or path was provided
// either.
fn parse_hostname(hostname: &str, insecure: bool) -> Result<Url, String> {
    // Confirm that no scheme has been specified in hostname
    if let Ok(tmp_url) = Url::parse(hostname) {
        if tmp_url.host().is_some() {
            return Err(format!("It looks like you've specified a scheme in `hostname` in the controller config - please do not!: {hostname}"));
        }
    }

    // Use https as default here. Swapping out for http or ssh is handled in
    // `distributed::minion::run_job` (at time of writing)
    if insecure {
        eprintln!("WARNING: communicating over plain text, do not use this in production");
        Url::parse(&format!("http:{}", hostname))
            .map_err(|e| format!("failed to parse hostname {hostname}: {e}"))
    } else {
        Url::parse(&format!("https:{}", hostname))
            .map_err(|e| format!("failed to parse hostname {hostname}: {e}"))
    }
}

/// Initialize the configuration at start, if the configuration is not
/// available it will crash the server immediately. Subsequent updates are
/// handled by the ConfigPoller.
async fn init_config(config_src: &ConfigSource, db: &StateDatabase) -> Result<(), Box<dyn Error>> {
    DBConfigReader {
        db: db.clone(),
        config: config_src.clone(),
    }
    .read_config()
    .await?;
    // Purge all jobs recorded as running since the scheduler is only now
    // initializing and no jobs can be running.
    let n_rows = db.reset().await?;
    if n_rows > 0 {
        tracing::info!("Reset {} lorries", n_rows);
    }
    Ok(())
}

/// Gracefully shutdown the axum server when a signal is received
async fn graceful_shutdown(shutdown_func: ShutdownHandle) {
    let mut interrupt_future =
        signal(SignalKind::interrupt()).expect("Failed to setup interrupt signal");
    let mut terminate_future =
        signal(SignalKind::terminate()).expect("Failed to setup terminate signal");

    tokio::select! {
        _ = interrupt_future.recv() => {},
        _ = terminate_future.recv() => {},
    }

    tracing::info!("caught interrupt, shutting down lorry, peacefully.");
    shutdown_func.shutdown().await;
}

/// Load Lorry's configuration either from a remote or local source
fn load_confgit_source(config: &Configuration) -> Result<ConfigSource, Box<dyn Error>> {
    match &config.confgit_url {
        Some(confgit_url) => {
            // TODO: Make this seperate from downstream configuration
            let parsed = Url::parse(confgit_url)?;
            let hostname = parsed.host_str().unwrap();
            let workspace =
                Workspace::new(config.working_area.as_path(), read_config::CONFGIT_DIR_NAME);
            workspace.init_if_missing(false, false)?;
            let insecure = if config.insecure { Some(true) } else { None };

            Ok(ConfigSource::Remote {
                repository_path: parsed.path().to_string(),
                workspace,
                builder: UrlBuilder {
                    hostname: hostname.to_string(),
                    username: config.username.clone(),
                    port: parsed.port(),
                    insecure,
                    // Look for confgit specific token then fall back to
                    // general private token if unspecified.
                    private_token: config
                        .gitlab_private_confgit_token
                        .as_ref()
                        .or(config.gitlab_private_token.as_ref())
                        .cloned(),
                    token_file: config
                        .gitlab_private_confgit_token_file
                        .as_ref()
                        .or(config.gitlab_private_token_file.as_ref())
                        .cloned(),
                },
                global_git_config: config.git_config_path.clone(),
                branch_name: config.confgit_branch.clone(),
            })
        }
        None => Ok(ConfigSource::Local(config.configuration_directory.clone())),
    }
}

/// Lorry entry point
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // Get command line args
    let args = ControllerCommandLineArguments::parse();

    // Set a panic hook/handler
    std::panic::set_hook(Box::new(|info| {
        eprintln!("=====💣=¡Don't panic, Lorry has panicked!=💣=====");
        eprintln!("{}", workerlib::redact::redact(info));
        eprintln!("=================================================");
    }));

    if FAIL_FAST {
        let default_handler = std::panic::take_hook();
        set_hook(Box::new(move |info| {
            default_handler(info);
            std::process::exit(1);
        }));
    }

    // Validate the command line args, and gets stats about the selected config (or any errors)
    if let Some(validate_test_dir) = args.validate {
        let linter = crate::linter::Linter::new(&validate_test_dir);
        match linter.lint() {
            Ok(_) => {
                let _ = linter.stats().map(|stats| println!("{}", stats));
            }
            Err(errors) => {
                if errors.iter().any(|e| match e {
                    linter::LinterError::Config(_) => {
                        eprintln!("{}:\n{}", Red.paint("[ERROR]"), e);
                        true
                    }
                    linter::LinterError::DuplicateRawFiles(url, _) => {
                        if args.validate_allow_duplicates {
                            eprintln!("{}: Duplicate Raw File: {}", Yellow.paint("[WARN]"), url);
                            false
                        } else {
                            eprintln!("{}: Duplicate Raw File: {}", Red.paint("[ERROR]"), url);
                            true
                        }
                    }
                    linter::LinterError::DuplicateGitMirrors(url, _) => {
                        if args.validate_allow_duplicates {
                            eprintln!("{}: Duplicate Git Mirror: {}", Yellow.paint("[WARN]"), url);
                            false
                        } else {
                            eprintln!("{}: Duplicate Git Mirror: {}", Red.paint("[ERROR]"), url);
                            true
                        }
                    }
                    // Exit if there's a GitLab name error
                    linter::LinterError::GitLabName(gitlab_error) => {
                        match gitlab_error {
                            validator::GitLabNameError::Group(error_msg)
                            | validator::GitLabNameError::SubGroup(error_msg)
                            | validator::GitLabNameError::Project(error_msg)
                            | validator::GitLabNameError::Path(error_msg) => {
                                eprintln!("{}: {}", Red.paint("[ERROR]"), error_msg);
                            }
                        }
                        true
                    }
                }) {
                    std::process::exit(1)
                } else {
                    let _ = linter.stats().map(|stats| println!("{}", stats));
                }
            }
        };
        eprintln!("{}", Green.paint("[OK]"));
        std::process::exit(0)
    }

    // Load the config from the TOML
    //
    // Also loads the GitLab PAT
    let config_path = args.config_file.unwrap_or("controller.conf".into());
    let config = match load(config_path.as_path()) {
        Ok(config) => config,
        Err(err) => {
            panic!("failed to load controller config:\n{}", err);
        }
    };

    let config_src = load_confgit_source(&config)?;
    // Initialize the global git config
    workerlib::git_config::Config(config.git_config_path.clone()).setup(
        &config.email,
        config.n_threads as i64,
        config.insecure,
        Some(&config.http_version.to_string()),
        &config.askpass_program,
        config.git_credentials_file.as_deref(),
    )?;

    // Setup tracing
    let formatter = tracing_subscriber::fmt::format::debug_fn(|writer, field, value| {
        let field_str = format!("{:?}", value);
        write!(
            writer,
            "{}: {}",
            field,
            workerlib::redact::redact(&field_str)
        )
    });

    tracing_subscriber::fmt()
        .compact()
        .with_line_number(true)
        .with_level(true)
        .with_thread_ids(true)
        .fmt_fields(formatter)
        .with_max_level(args.log_level.unwrap_or(config.log_level))
        .init();
    tracing::info!("Logger initialized");

    // Setup the DB and run any migrations
    state_db::init_and_migrate(&config.state_db).await?;
    let db = StateDatabase::connect(&config.state_db, false).await?;
    tracing::info!("Database initialized");

    // Get the hostname (and optional port) of the downstream mirror host
    let hostname = match parse_hostname(&config.hostname, config.insecure) {
        Ok(hostname) => hostname,
        Err(err) => panic!("failed to load controller config:\n{err}"),
    };

    // Fine to omit local
    let downstream = GitLabDownstream::new(
        hostname,
        config.visibility,
        config.gitlab_private_token.clone(),
        config.gitlab_private_token_file.clone(),
        config.insecure,
    );

    // Lookup the hostname of this system
    let resolved_hostname = hostname::get()?.into_string().unwrap();
    tracing::info!("Initializing Lorry controller {}", resolved_hostname);

    // Set the web UI settings
    let settings = web::ControllerSettings {
        downstream: downstream::Downstream::GitLab(downstream),
        configuration_directory: config.configuration_directory,
        hostname: resolved_hostname,
        current_pid: std::process::id(),
        namespace_depth: config.namespace_depth,
    };
    let addr = SocketAddr::from(([0, 0, 0, 0], config.port));

    // Setup the workerlib
    let mut workerlib_args = workerlib::Arguments {
        pack_threads: Some(config.clone.n_threads),
        use_git_binary: matches!(config.clone.engine, config::CloneEngine::GitBinary).into(),
        git_config_path: config.git_config_path,
        sha256sum_required: config.sha256sums_required.is_some_and(|required| required),
        ..Default::default()
    };
    workerlib_args.working_area.clone_from(&config.working_area);
    if let Some(maximum_redirects) = config.maximum_redirects {
        workerlib_args.maximum_redirects = maximum_redirects
    };
    workerlib_args.check_ssl_certificates = !config.insecure;

    let mut tracker: Option<scheduler::Tracker> = None;
    let mut shutdown: Option<ShutdownHandle> = None;

    if args.poll_config.is_some_and(|poll| poll)
        && (args.process_jobs.is_none() || args.process_jobs.is_some_and(|process| !process))
    {
        tracing::info!("Running standalone config poller");
        init_config(&config_src, &db).await?;
        let db = db.clone();
        tokio::task::spawn(async move {
            let reader = scheduler::IntervalConfigReader {
                config_reader: scheduler::component(DBConfigReader {
                    db,
                    config: config_src.clone(),
                }),
                interval: config.confgit_update_period.into(),
                db_lock: Arc::new(Mutex::new(())),
            };
            reader.read().await.expect("Error refreshing configuration");
        });
    } else if args.process_jobs.is_some_and(|process| process) {
        let mut config_reader: Option<DBConfigReader> = None;
        if args.poll_config.is_some_and(|poll| poll) {
            tracing::info!("Launching config poller with multi-threaded worker");
            init_config(&config_src, &db).await?;
            config_reader = Some(DBConfigReader {
                db: db.clone(),
                config: config_src.clone(),
            });
        } else {
            tracing::info!("Launching standalone multi-threaded worker");
        };

        // Build the scheduler
        let scheduler = scheduler::SchedulerBuilder::default()
            .username(&config.username.unwrap())
            .hostname(&config.hostname)
            .insecure(config.insecure)
            .private_token(config.gitlab_private_token)
            .token_file(config.gitlab_private_token_file)
            .threads(config.n_threads)
            .config_reader_interval(config.confgit_update_period.into())
            .build(
                &workerlib_args,
                DBJobAllocator(db.clone(), settings.clone()),
                DBRecorder(db.clone()),
                config_reader,
                DefaultExecutor {},
            );

        // Run the scheduler
        let (_shutdown, _tracker) = scheduler.schedule_all().await?;
        shutdown = Some(_shutdown);
        tracker = Some(_tracker);
    };

    if let Some(smtp_config) = config.smtp {
        let mut notifier = Notifier::builder()
            .endpoint(&smtp_config.endpoint)
            .tls(smtp_config.tls_config())
            .from_address(&smtp_config.from_address)
            .alert_groups(&config.alert_groups.map_or(Vec::new(), |alert_config| {
                alert_config
                    .iter()
                    .map(|group| (group.recipient.to_string(), group.pattern.to_string()))
                    .collect()
            }))
            .lorry_url_base(&smtp_config.base_url.unwrap()); // set in config.rs
        if let Some(port) = smtp_config.port {
            notifier = notifier.port(port);
        }
        if let Some(username) = smtp_config.username {
            notifier = notifier.credentials(&username, &smtp_config.password.unwrap());
        }
        if let Some(threshold) = config.alert_threshold {
            notifier = notifier.alert_threshold(threshold);
        }
        let notifier = notifier.build()?;
        let notifier = DbNotifier(db.clone(), notifier);
        // FIXME: Wire in shutdown
        tokio::spawn(async move {
            tracing::info!("Launching SMTP Notifier");
            notifier.poll().await
        });
    }

    // Run/serve the web UI
    if args.enable_http.is_some_and(|http| http) {
        tracing::info!("Launching http server listening on port {}", config.port);
        let app = web::app(
            db.into(),
            settings.clone(),
            tracker.unwrap_or(scheduler::tracker()),
        )
        .await;

        let listener = TcpListener::bind(addr).await?;

        serve(listener, app.router)
            .with_graceful_shutdown(graceful_shutdown(shutdown.unwrap()))
            .await
            .expect("Failed to run server..");

        Ok(())
    } else {
        loop {
            // FIXME FIXME FIXME
            tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
        }
    }
}

mod tests {
    #[test]
    fn test_parse_hostname() {
        assert!(super::parse_hostname("https://archlinux.org/", false).is_err());
        assert!(super::parse_hostname("archlinux.org/", false).is_ok());
        assert!(super::parse_hostname("archlinux.org:909", false).is_ok());

        assert!(super::parse_hostname("name:secret@archlinux.org:909", false).is_ok());
        assert!(super::parse_hostname("archlinux.org:909/hello", false).is_ok());
    }
}
