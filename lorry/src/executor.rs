//!
//! Executor trait implemented by workers so that they can run jobs.
//!

use crate::comms::Job;
use async_trait::async_trait;
use std::path::Path;
use url::Url;
use workerlib::MirrorStatus;

/// Params passed into the runner
pub struct Params<'a> {
    pub working_area: &'a Path,
    pub job: &'a Job,
    pub target_url: &'a Url,
    pub mirror_settings: &'a workerlib::Arguments,
}

/// Executor is responsible for executing mirroring operations from within a
/// worker thread.
#[async_trait::async_trait]
pub trait Executor {
    /// Run the Lorry job returning any failure that happened during the
    /// mirroring operation.
    async fn run(&self, params: &Params) -> Result<MirrorStatus, workerlib::Error>;
}

/// Default mirror executor used for normal operation
#[derive(Clone)]
pub struct DefaultExecutor;

#[async_trait]
impl Executor for DefaultExecutor {
    /// Used by [`DefaultWorker`](crate::worker::DefaultWorker)
    /// to actaully run a [`comms::Job`](crate::comms::Job)
    async fn run(&self, params: &Params) -> Result<MirrorStatus, workerlib::Error> {
        // Working area needs to always be an absolute path
        std::fs::create_dir_all(params.working_area).unwrap();
        let absolute_working_area = params.working_area.canonicalize().unwrap();

        // Use the workerlib to setup a workspace
        let workspace =
            workerlib::workspace::Workspace::new(&absolute_working_area, &params.job.lorry_name);
        workspace.init_if_missing(
            matches!(params.job.lorry_spec, workerlib::LorrySpec::RawFiles(_)),
            true,
        )?;
        tracing::info!("workspace initialized: {:?}", workspace);
        let lorry_name = &params.job.lorry_name;
        match params.job.job_kind {
            crate::state_db::models::JobKind::Mirror => {
                // Use workerlib to do the mirroring
                workerlib::try_mirror(
                    &params.job.lorry_spec,
                    lorry_name,
                    params.target_url,
                    &workspace,
                    params.mirror_settings,
                )
                .await
            }
            crate::state_db::models::JobKind::Gc => {
                tracing::info!("Running git-gc maintenance task");
                let config_path = params.mirror_settings.git_config_path.as_path();
                let cmd = workerlib::git_gc::Gc {
                    config_path,
                    prune_expiry_date: None,
                    auto: false,
                };
                workerlib::execute::execute(&cmd, workspace.repository_path().as_path()).await?;
                Ok(MirrorStatus::default())
            }
            crate::state_db::models::JobKind::Fsck => {
                tracing::info!("Running git-fsck maintenance task");
                let config_path = params.mirror_settings.git_config_path.as_path();
                let cmd = workerlib::git_fsck::Fsck {
                    config_path,
                    connectivity_only: true,
                    no_dangling: true,
                };
                workerlib::execute::execute(&cmd, workspace.repository_path().as_path()).await?;
                Ok(MirrorStatus::default())
            }
        }
    }
}
