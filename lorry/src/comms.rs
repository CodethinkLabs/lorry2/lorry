//!
//! Defines Jobs and related data types.
//!
//! Jobs are run by workers.
//!

use serde::{Deserialize, Deserializer, Serialize, Serializer};

use crate::state_db::models::JobKind;

/// The status of a given job; either it is running (or has never been
/// completed before), or it is completed, with some exit status.
#[derive(Serialize, Deserialize, PartialEq, Copy, Clone, Debug)]
pub enum JobExitStatus {
    FinishedExitCode(i64),
    Running,
}

impl From<Option<i64>> for JobExitStatus {
    fn from(value: Option<i64>) -> JobExitStatus {
        if let Some(exit_code) = value {
            JobExitStatus::FinishedExitCode(exit_code)
        } else {
            JobExitStatus::Running // or, technically, has not been run yet
        }
    }
}

/// Sent by the controller to the worker to specify the job the worker should work on
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Job {
    // The text of the job, this would be the contents of the .lorry file
    pub lorry_name: String,
    pub lorry_spec: workerlib::LorrySpec,
    pub id: JobId,
    pub path: LorryPath,
    // If the working copies were mirrored before this date, delete them
    pub purge_cutoff: TimeStamp,
    pub job_kind: JobKind,
}

// Represents the partial file path that identifies a lorry spec; i.e by the
// relative upstream path it will be mirrored to.
#[derive(
    Serialize, Deserialize, Debug, Clone, sqlx::Type, PartialEq, PartialOrd, Eq, Ord, Hash,
)]
#[sqlx(transparent)]
#[serde(transparent)]
pub struct LorryPath(String);

impl std::fmt::Display for LorryPath {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl LorryPath {
    pub fn new(s: String) -> Self {
        LorryPath(s)
    }

    pub fn components(&self) -> Vec<&str> {
        self.0.split('/').collect::<Vec<_>>()
    }
}

impl<'a> From<&'a LorryPath> for gitlab::api::common::NameOrId<'a> {
    fn from(value: &'a LorryPath) -> Self {
        value.0.as_str().into()
    }
}

#[derive(Serialize, Deserialize, Copy, Clone, sqlx::Type, Debug, PartialEq)]
#[serde(transparent)]
#[sqlx(transparent)]
pub struct JobId(pub i64);

#[derive(Serialize, Deserialize, Copy, Clone, sqlx::Type, Debug, sqlx::FromRow)]
#[sqlx(transparent)]
pub struct TimeStamp(pub i64);

/// Custom deserialisation so ISO8601 durations can be deserialised from the
/// config into `std::time::Duration`s (can't be done directly)
pub fn deserialise_duration<'de, D>(deserializer: D) -> Result<std::time::Duration, D::Error>
where
    D: Deserializer<'de>,
{
    let duration: iso8601::Duration = Deserialize::deserialize(deserializer)?;
    Ok(duration.into())
}

/// Custom serialisation so that `std::time::Duration`s are serialsed to
/// ISO8601 durations (seconds only)
/// TODO: We should upstream serde support for ISO8601
pub fn serialise_duration<S>(value: &std::time::Duration, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_str(&format!("PT{}S", value.as_secs()))
}

/// Same as deserialise_duration but as an Option
pub fn deserialise_duration_option<'de, D>(
    deserializer: D,
) -> Result<Option<std::time::Duration>, D::Error>
where
    D: Deserializer<'de>,
{
    let duration: iso8601::Duration = Deserialize::deserialize(deserializer)?;
    Ok(Some(duration.into()))
}

/// Same as serialise_duration but as an Option
pub fn serialise_duration_option<S>(
    value: &Option<std::time::Duration>,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    if let Some(duration) = value {
        serializer.serialize_str(&format!("PT{}S", duration.as_secs()))
    } else {
        serializer.serialize_none()
    }
}
