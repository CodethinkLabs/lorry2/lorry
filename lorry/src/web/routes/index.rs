//!
//! The Lorry web UI homepage.
//!

use crate::state_db::models::Lorry;
use crate::state_db::Pagination;
use crate::web::ControllerState;
use crate::web::{error::WebError, filters};
use askama::Template;
use axum::extract::{Query, State};

#[derive(Template)]
#[template(path = "index.html")]
pub struct Index<'a> {
    title: &'a str,
    lorries: Vec<Lorry>,
    pagination: Pagination,
    stylesheet: &'static str,
    logo: &'static str,
}

///
/// Handle a GET request to `/`
///
#[axum_macros::debug_handler]
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_home(
    State(state): State<ControllerState>,
    Query(mut pagination): Query<Pagination>,
) -> Result<Index<'static>, WebError> {
    let lorries = pagination.paginate_lorries(&state.db).await?;
    Ok(Index {
        title: "",
        lorries,
        pagination,
        stylesheet: include_str!("../../../assets/pico.min.css"),
        logo: include_str!("../../../assets/codethink-logo.svg"),
    })
}
