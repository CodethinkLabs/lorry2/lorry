use crate::state_db::models::Lorry;
use crate::state_db::Pagination;
use crate::web::ControllerState;
use crate::web::{error::WebError, filters};
use askama::Template;
use axum::{
    extract::{Query, Request, State},
    response::Redirect,
};

#[derive(Template)]
#[template(path = "failed.html")]
pub struct Index<'a> {
    title: &'a str,
    lorries: Vec<Lorry>,
    pagination: Pagination,
    stylesheet: &'static str,
    logo: &'static str,
    this_url: String,
}

pub(crate) async fn handle_post_schedule_failed(
    State(state): State<ControllerState>,
    request: Request,
) -> Result<Redirect, WebError> {
    let failed_lorries = state.db.get_all_failed_lorries_info().await?;
    for lorry in failed_lorries {
        tracing::info!("user is requesting priority for (path) {}", &lorry.path);
        let path = crate::comms::LorryPath::new(lorry.path);
        state.db.toggle_priority_run(&path, true).await?;
    }
    let target = request.uri().path();
    Ok(Redirect::to(target))
}

#[axum_macros::debug_handler]
#[tracing::instrument(skip_all)]
pub(crate) async fn handle_get_failed(
    State(state): State<ControllerState>,
    Query(mut pagination): Query<Pagination>,
    request: Request,
) -> Result<Index<'static>, WebError> {
    let lorries = pagination.paginate_failed_lorries(&state.db).await?;
    let this_url = request.uri().path().to_string();
    Ok(Index {
        title: "Failed Lorries",
        lorries,
        pagination,
        stylesheet: include_str!("../../../assets/pico.min.css"),
        logo: include_str!("../../../assets/codethink-logo.svg"),
        this_url,
    })
}
