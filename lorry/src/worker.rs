//!
//! Worker types and methods
//!
//! Workers run jobs.  More specifcally, [`DefaultWorker`] represents a worker,
//! and holds everything needed to run a job.  It has a method,
//! [`DefaultWorker.work()`](DefaultWorker::work) which is used to do work (an
//! [`DefaultExecutor`](crate::executor::DefaultExecutor) is
//! used from within a worker to
//! [`DefaultExecutor.run()`](crate::executor::DefaultExecutor::run()) a job).
//!

use crate::comms::{Job, JobId};
use crate::executor::{Executor, Params as ExecutorParams};
use crate::scheduler::CHANNEL_TIMEOUT_MS;
use crate::state_db::LorryEntry;
use crate::url_builder::UrlBuilder;
use std::error::Error as StdError;
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::mpsc::{channel, Sender};
use tokio::sync::Mutex;
use tokio::task::spawn;
use tokio::time::{sleep, Instant};
use work_queue::LocalQueue;
use workerlib::{Error, MirrorStatus};

/// State of a given thread, can only be busy or idle.
#[derive(Clone, Debug)]
pub enum State {
    /// JobId the thread is currently processing
    Busy((String, JobId)),

    Idle,
}

/// A simple numeric identifier for tracking active threads.
#[derive(Clone, Debug, Hash, Copy, Eq, PartialEq, Default)]
pub struct ThreadId(pub usize);

/// Work stealing local job queue
pub type LocalJobQueue = LocalQueue<JobReady>;

/// Used to indicate/queue a Lorry that is ready to be run as a corresponding
/// job.
///
/// `JobReady` is a tuple struct and represents a lorry due to be run as a
/// corresponding job.  The first member is the lorry ([`LorryEntry`]) that's
/// to be run.  The second member is a [`Sender`] that can send [`JobAccepted`]s.
/// The sender is used by a worker to tell the poller it is ready to run the
/// [`comms::Job`](crate::comms::Job) associated with the lorry that's ready.  It does this by sending a
/// [`JobAccepted`] over the channel; the channel is setup in
/// `scheduler::IntervalJobPoller`.
///
/// `JobReady`s are pushed onto the global `Queue`, which `LocalQueue`s can
/// pull from (each worker has a `LocalQueue`).
///
/// `issue()` is used to actually issue the [`comms::Job`](crate::comms::Job) (`poller.issue()` called in
/// `poll()`). This returns a [`comms::Job`](crate::comms::Job), which is sent to the worker (in `work()`)
/// over the channel that `work()` created.
///
/// See also [`JobAccepted`] and [`JobResult`]
pub struct JobReady(pub LorryEntry, pub Sender<JobAccepted>);

/// This is used by a worker to indicate (to the scheduler's poller) that it
/// will run the contained lorry (the poller will send back the actual [`comms::Job`](crate::comms::Job))  
///
/// For background see [`JobReady`]
///
/// A [`comms::Job`](crate::comms::Job) is a running lorry.  If a lorry is to be run, a job needs to be
/// created.  A [`JobReady`] represents a lorry that's ready to be run, and which
/// is therefore in the queue ([`LocalJobQueue`]). When a worker is free to run a
/// new lorry, it pops one of these [`JobReady`]s off the queue.  This happens at
/// the beginning of [`DefaultWorker::work()`]. To get the actual [`comms::Job`](crate::comms::Job), the
/// worker, inside `work()`, creates a [`JobAccepted`] and sends it back to
/// `Scheduler::poll()`.  The scheduler thus creates a [`comms::Job`](crate::comms::Job) proper by calling
/// [`poller.issue()`] in `IntervalJobPoller::poll()`.  This call to
/// `poller.issue()` uses/updates the [state_db](crate::state_db), and returns the job as as a
/// [`comms::Job`](crate::comms::Job).  Next, `IntervalJobPoller::poll()` sends the [`comms::Job`](crate::comms::Job) to the
/// [`DefaultWorker`] that just sent the [`JobAccepted`].  [`DefaultWorker::work()`]
/// then runs the [`comms::Job`](crate::comms::Job).
///
/// See also [`JobResult`]
#[allow(dead_code)]
pub struct JobAccepted(pub ThreadId, pub LorryEntry, pub Sender<Job>);

/// Used to indicate/send the exit status of a particular job as returned from
/// a worker thread (returned by `Worker.job()`).
///
/// For background see [`JobAccepted`]
///
/// [`DefaultWorker.work()`](DefaultWorker.work) calls `runner.run()` in an
/// async closure in a `tokio` task.  `DefaultExecutor` implements
/// [`Executor::run()`] which returns a `Result<MirrorStatus, workerlib::Error>`,
/// *not* a [`JobResult`].  A [`JobResult`] is created from this returned
/// `Result`, and sent over a `Sender<JobResult>` to the scheduler's recorder.
///
/// In `scheduler::Builder::build()` workers are setup, including giving each
/// [`DefaultWorker`] a `tx` (assigned to `DefaultWorker.result_ch`).  This
/// vector of workers is assigned to `Scheduler.workers`.  The `rx` is assigned
/// to `recorder.results`.  The `recorder` receives these `JobResult`s inside
/// `FixedRecorder.record()`, which calls `DBRecorder.record()` which uses
/// `StateDatabase.record()` to record the info about the finished job (jobs,
/// refs, and warnings tables)
pub struct JobResult(pub ThreadId, pub Job, pub Result<MirrorStatus, Error>);

/// Runner is responsible for executing mirroring operations from within a
/// worker thread.
#[async_trait::async_trait]
pub trait Worker: Send + Sync {
    /// Do work associated with a particular job.  This includes getting a
    /// Lorry from the shared queue, informing the poller (which issues a job
    /// and sends it to the worker), and then actaully running the job,
    /// returning any failure that happened during the mirroring operation.
    async fn work(self, queue: LocalJobQueue) -> Result<(), Box<dyn StdError>>;
}

/// Worker is a thread bound to a channel that is responsible for running
/// Lorry operations. All operations have a timeout and when that timeout is
/// exceeded the underlying operations are killed. When all threads are busy
/// jobs are added to queue and scheduled when a thread becomes available.
/// Workers only communicate by passing messages and have no access to the
/// underlying data store.
pub struct DefaultWorker<E>
where
    E: Executor + Clone + Send + Sync + 'static,
{
    /// Unique thread ID
    pub thread_id: ThreadId,

    /// Static runner implementation, typically just DefaultRunner
    pub runner: Arc<Mutex<Box<E>>>,

    /// Settings passed into workerlib
    pub mirror_settings: workerlib::Arguments,

    /// Used to construct URLs that pass into workerlib
    pub url_builder: UrlBuilder,

    /// Interval used to poll for jobs from the queue within the worker thread
    pub polling_interval: Duration,

    /// Channel to send results from the worker back to the scheduler
    pub result_ch: Sender<JobResult>,
}

#[async_trait::async_trait]
impl<E> Worker for DefaultWorker<E>
where
    E: Executor + Clone + Send + Sync + 'static,
{
    /// Get a [`LorryEntry`] that's ready to run from the global `Queue`, and run the
    /// corresponding [`comms::Job`](crate::comms::Job).
    ///
    /// This runs a loop forever.  Each iteration it checks it there is
    /// [`JobReady`] in the [`LocalJobQueue`].  If there isn't, sleep for the
    /// polling interval length of time, otherwise do some work.
    ///
    /// Call [`DefaultExecutor.run()`](crate::executor::DefaultExecutor::run()) to actually run
    async fn work(self, mut queue: LocalJobQueue) -> Result<(), Box<dyn StdError>> {
        // Each loop either does no work, or runs 1 job
        loop {
            if let Some(message) = queue.pop() {
                // Timeout created - if it's exceeded we will kill the
                // underlying job.
                //
                // TODO: individual mirrors should be able to specify this too.
                let timeout = message.0.lorry_timeout;

                let now = Instant::now();

                // Setup a channel so that the Scheduler poller can send this
                // worker a [`comms::Job`](crate::comms::Job) to run.
                let (tx, mut rx) = channel::<Job>(1);

                // Send a [`JobAccepted`] to the poller to notify it that this
                // worker is ready to do the work specified by the lorry.  Send
                // the `Sender<Job>` just created so that the scheduler can
                // send back the [`comms::Job`](crate::comms::Job) to be run.
                message
                    .1
                    .send_timeout(
                        JobAccepted(self.thread_id, message.0.clone(), tx),
                        Duration::from_millis(CHANNEL_TIMEOUT_MS),
                    )
                    .await
                    .unwrap();

                // From the channel receive the Job from the poller
                let job = rx.recv().await.unwrap();
                let thread_id = self.thread_id;
                let working_area = self.mirror_settings.working_area.clone();
                let target_url = self.url_builder.build(&job.path.to_string())?;

                // `clone()` so they can be `move`d into the closure below
                let mirror_settings = self.mirror_settings.clone();
                let worker_job = job.clone();
                let result_ch = self.result_ch.clone();
                let runner = self.runner.clone();
                let mirror_path = worker_job.path.to_string();

                // Spawn a tokio task that runs an async closure
                let handle = spawn(async move {
                    let runner = runner.lock().await;

                    // Use DefaultExecutor.run() to run the job
                    match runner
                        .run(&ExecutorParams {
                            working_area: working_area.as_path(),
                            job: &worker_job,
                            target_url: &target_url,
                            mirror_settings: &mirror_settings,
                        })
                        .await
                    {
                        Ok(status) => {
                            tracing::info!("mirroring operation for {:?}, completed", mirror_path);

                            // Send the JobResult to the Scheduler's recorder,
                            // which will update the DB
                            result_ch
                                .send_timeout(
                                    JobResult(thread_id, worker_job, Ok(status)),
                                    Duration::from_secs(CHANNEL_TIMEOUT_MS),
                                )
                                .await
                                .unwrap();
                        }
                        // An error occured when the job was run
                        Err(err) => {
                            match err {
                                Error::SomeRefspecsFailed { refs: _, n_failed } => {
                                    tracing::info!(
                                        "Mirroring operation for {:?} as {} failed refs",
                                        mirror_path,
                                        n_failed
                                    );
                                }
                                _ => {
                                    tracing::info!(
                                        "Mirroring operation for {:?} has failed: {:?}",
                                        mirror_path,
                                        err
                                    );
                                }
                            }

                            // Send the JobResult to the Scheduler's recorder,
                            // which will update the DB
                            result_ch
                                .send_timeout(
                                    JobResult(thread_id, worker_job, Err(err)),
                                    Duration::from_secs(CHANNEL_TIMEOUT_MS),
                                )
                                .await
                                .unwrap();
                        }
                    };
                });

                // Nested (labelled) loop to wait for the handle to finish
                'handle_loop: while !handle.is_finished() {
                    // If the task has been running for longer than its timeout
                    // abort it
                    if now.elapsed() >= timeout {
                        tracing::warn!(
                            "job has exceeded its allocated runtime [{:?}], terminating",
                            timeout
                        );
                        handle.abort();

                        // Send the JobResult to the Scheduler's recorder,
                        // which will update the DB
                        self.result_ch
                            .send_timeout(
                                JobResult(
                                    thread_id,
                                    job,
                                    Err(Error::TimeoutExceeded {
                                        seconds: now.elapsed().as_secs(),
                                    }),
                                ),
                                Duration::from_secs(CHANNEL_TIMEOUT_MS),
                            )
                            .await
                            .unwrap();
                        break 'handle_loop;
                    };

                    // If it hasn't yet timed out, log how long it's been
                    // running for
                    let elapsed = now.elapsed();
                    let elapsed_seconds = elapsed.as_secs();
                    if (elapsed_seconds as f32 / 60.0) % 1.0 == 0.0 && elapsed_seconds > 0 {
                        tracing::info!(
                            "Thread {:?} [{}] has been active for {:?}",
                            self.thread_id,
                            message.0.path.to_string(),
                            elapsed,
                        );
                    }

                    // Sleep for polling interval
                    sleep(self.polling_interval).await;
                }
            } else {
                sleep(self.polling_interval).await;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        comms::{JobExitStatus, LorryPath, TimeStamp},
        state_db::models::JobKind,
    };
    use async_trait::async_trait;
    use tokio::sync::mpsc::channel;
    use work_queue::Queue;
    use workerlib::{lorry_specs::SingleLorry, LorrySpec};

    #[derive(Clone)]
    struct NoopRunner;

    #[async_trait]
    impl Executor for NoopRunner {
        async fn run(&self, _params: &ExecutorParams) -> Result<MirrorStatus, Error> {
            Ok(MirrorStatus::default())
        }
    }

    #[tokio::test]
    async fn test_worker_single_op() {
        let queue = Queue::<JobReady>::new(1, 1);

        let (tx, mut rx) = channel::<JobAccepted>(1);
        let (result_tx, mut result_rx) = channel::<JobResult>(32);

        let worker = DefaultWorker {
            thread_id: ThreadId(0),
            runner: Arc::new(Mutex::new(Box::new(NoopRunner {}))),
            polling_interval: Duration::from_millis(300),
            url_builder: UrlBuilder::default(),
            mirror_settings: workerlib::Arguments::default(),
            result_ch: result_tx,
        };

        let local_job_queue = queue.local_queues().next().unwrap();

        spawn(async move {
            worker.work(local_job_queue).await.unwrap();
        });

        // TODO: implement defaults
        queue.push(JobReady(
            LorryEntry {
                path: LorryPath::new(String::new()),
                name: String::new(),
                spec: LorrySpec::Git(SingleLorry::default()),
                running_job: None,
                last_run: TimeStamp(0),
                last_attempted: TimeStamp(0),
                interval: Duration::from_secs(0),
                maintenance: Duration::from_secs(0),
                lorry_timeout: Duration::from_secs(10),
                last_run_results: JobExitStatus::FinishedExitCode(-1),
                last_run_error: None,
                purge_from_before: TimeStamp(0),
                priority: false,
            },
            tx,
        ));

        let accepted = rx.recv().await.unwrap();
        assert!(accepted.0 .0 == 0);
        let start_ch = accepted.2;
        start_ch
            .send(Job {
                lorry_name: String::from("test lorry"),
                lorry_spec: LorrySpec::Git(SingleLorry::default()),
                id: JobId(0),
                path: LorryPath::new(String::new()),
                purge_cutoff: TimeStamp(0),
                job_kind: JobKind::Mirror,
            })
            .await
            .unwrap();

        let result = result_rx.recv().await.unwrap();
        assert!(result.0 .0 == 0);
        assert!(result.1.lorry_name == "test lorry");
        assert!(result.2.is_ok());
    }
}
