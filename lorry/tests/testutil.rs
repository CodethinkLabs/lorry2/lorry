use std::future::Future;
use std::sync::Arc;
use std::time::{Duration, Instant};
use tokio::process::{Child, Command};
use tokio::sync::Mutex;

const BINARY_PATH: &str = env!("CARGO_BIN_EXE_lorry");

pub fn start_lorry(config_file: &str, cwd: &str) -> Arc<Mutex<Child>> {
    Arc::new(Mutex::new(
        Command::new(BINARY_PATH)
            .args(&["--config", config_file])
            .current_dir(cwd)
            .spawn()
            .expect("Failed to start binary"),
    ))
}

pub async fn project_exists(gitlab_url: &str, group: &str, name: &str, token: &str) -> bool {
    let client = reqwest::Client::new();
    let response = client
        .get(&format!("{}/projects/{}%2F{}", gitlab_url, group, name))
        .header("PRIVATE-TOKEN", token)
        .send()
        .await
        .unwrap();

    response.status().is_success()
}

pub async fn delete_project_if_exists(gitlab_url: &str, group: &str, name: &str, token: &str) {
    let client = reqwest::Client::new();
    let _ = client
        .delete(&format!("{}/projects/{}%2F{}", gitlab_url, group, name))
        .header("PRIVATE-TOKEN", token)
        .send()
        .await;
}

pub async fn commit_exists(
    gitlab_url: &str,
    group: &str,
    project: &str,
    token: &str,
    commit_id: &str,
) -> bool {
    let client = reqwest::Client::new();
    let response = client
        .get(&format!(
            "{}/projects/{}%2F{}/repository/commits/{}",
            gitlab_url, group, project, commit_id
        ))
        .header("PRIVATE-TOKEN", token)
        .send()
        .await
        .unwrap();

    response.status().is_success()
}

pub async fn file_exists(
    gitlab_url: &str,
    group: &str,
    project: &str,
    token: &str,
    filename: &str,
) -> bool {
    let client = reqwest::Client::new();
    let response = client
        .get(&format!(
            "{}/projects/{}%2F{}/repository/files/{}?ref=main",
            gitlab_url, group, project, filename
        ))
        .header("PRIVATE-TOKEN", token)
        .send()
        .await
        .unwrap();

    response.status().is_success()
}

pub async fn check_repository_mirroring<Fut>(
    gitlab_url: &str,
    config_file: &str,
    group: &str,
    project_name: &str,
    cwd: &str,
    inner_closure: impl Fn(String) -> Fut,
) where
    Fut: Future<Output = bool>,
{
    let token =
        std::env::var("LORRY_GITLAB_PRIVATE_TOKEN").expect("LORRY_GITLAB_PRIVATE_TOKEN not set");

    // Cleanup before test
    delete_project_if_exists(&gitlab_url, &group, &project_name, &token).await;

    // Start binary as background process
    let child = start_lorry(config_file, cwd);

    // Verify creation with timeout
    let start = Instant::now();
    let timeout = Duration::from_secs(30);
    let mut created = false;
    let mut commit_present = false;

    while Instant::now().duration_since(start) < timeout {
        if project_exists(&gitlab_url, &group, &project_name, &token).await {
            created = true;

            let start_inner = Instant::now();
            let timeout_inner = Duration::from_secs(30);

            while Instant::now().duration_since(start_inner) < timeout_inner {
                if inner_closure(token.clone()).await {
                    commit_present = true;
                    break;
                }
                tokio::time::sleep(Duration::from_secs(2)).await;
            }

            break;
        }
        tokio::time::sleep(Duration::from_secs(2)).await;
    }

    let _ = delete_project_if_exists(&gitlab_url, &group, &project_name, &token).await;
    let mut child_guard = child.lock().await;
    child_guard.kill().await.expect("Failed to kill process");

    assert!(created, "Project was not created within timeout");
    assert!(commit_present, "Expected commit not created");
}
