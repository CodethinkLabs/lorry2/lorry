use serial_test::serial;

mod testutil;

#[tokio::test]
#[ignore = "This is an e2e test, run with --ignored to run it"]
#[serial]
async fn test_git_mirror() {
    let gitlab_url = "http://localhost:9999/api/v4";
    let config_file = "./tests/e2e-configs/git_mirror.toml";

    let group_name = "lorry_git_mirror";
    let project_name = "lorry";
    testutil::check_repository_mirroring(
        gitlab_url,
        config_file,
        group_name,
        project_name,
        ".",
        |token| async move {
            testutil::commit_exists(
                &gitlab_url,
                &group_name,
                &project_name,
                &token,
                "19685b1fe3b6cc8f40e479067be56609b13156c0",
            )
            .await
        },
    )
    .await;
}

#[tokio::test]
#[ignore = "This is an e2e test, run with --ignored to run it"]
#[serial]
async fn test_raw_file_mirror() {
    let gitlab_url = "http://localhost:9999/api/v4";
    let config_file = "./tests/e2e-configs/raw_file_mirror.toml";

    let group_name = "lorry_raw_file";
    let project_name = "lorry-assets";
    testutil::check_repository_mirroring(
        gitlab_url,
        config_file,
        group_name,
        project_name,
        ".",
        |token| async move {
            testutil::file_exists(
                &gitlab_url,
                &group_name,
                &project_name,
                &token,
                "lorry-tiny.png",
            )
            .await
        },
    )
    .await;
}

#[tokio::test]
#[ignore = "This is an e2e test, run with --ignored to run it"]
#[serial]
async fn test_example() {
    let gitlab_url = "http://localhost:9999/api/v4";
    let config_file = "lorry.example.toml";

    let group_name = "lorry";
    let project_name = "lorry";
    testutil::check_repository_mirroring(
        gitlab_url,
        config_file,
        group_name,
        project_name,
        "../",
        |token| async move {
            testutil::commit_exists(
                &gitlab_url,
                &group_name,
                &project_name,
                &token,
                "19685b1fe3b6cc8f40e479067be56609b13156c0",
            )
            .await
        },
    )
    .await;
}

#[tokio::test]
#[ignore = "This is an e2e test, run with --ignored to run it"]
#[should_panic]
#[serial]
async fn test_break_on_http2() {
    let gitlab_url = "http://localhost:9999/api/v4";
    let config_file = "./tests/e2e-configs/break_on_http2.toml";

    let group_name = "lorry_break_on_http2";
    let project_name = "broken-assets";
    testutil::check_repository_mirroring(
        gitlab_url,
        config_file,
        group_name,
        project_name,
        ".",
        |token| async move {
            testutil::file_exists(
                &gitlab_url,
                &group_name,
                &project_name,
                &token,
                "lorry-tiny.png",
            )
            .await
        },
    )
    .await;
}
