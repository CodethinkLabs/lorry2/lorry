All refs have failed for mirror {{ path }}.

Visit {{ job_url }} for more information.

{% if let Some(output) = output %}
Job output:
{{ output }}
{% endif %}

Sent via Lorry @ {{ timestamp }}
