# Lorry 2

Lorry is a high performance server capaible of mirroring tens of thousands of
Git repositories and binary asset files from multiple "upstream" sources into
a single unified "downstream" code hosting service.

You can find the latest Lorry documentation 
[here](https://codethinklabs.gitlab.io/lorry/lorry2).

##### License

Lorry is [licensed](https://gitlab.com/CodethinkLabs/lorry/lorry2/-/blob/main/LICENSE?ref_type=heads) 
under Apache License Version 2.0
