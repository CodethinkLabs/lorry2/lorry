#!/bin/sh

podman run --rm -ti --net host \
	-v $PWD/prometheus.yml:/etc/prometheus/prometheus.yml \
	-v $PWD/lorry-alerts.yml:/etc/prometheus/lorry-alerts.yml \
	docker.io/prom/prometheus
