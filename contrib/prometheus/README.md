# Prometheus / Alert Manager Configuration

This is a simple example illustrating how to configure Prometheus to send
alerts based on a Lorry namespace. Assumes you have Podman installed and that
Lorry is running on `localhost:3000`.

### Usage

```sh
./run-example.sh
```

Browse to [http://localhost:9090/alerts](http://localhost:9090/alerts).
