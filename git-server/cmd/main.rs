use std::io::Error;
use std::path::{Path, PathBuf};

use clap::Parser;
use git_server::Server;
use tracing::Level;

/// Tiny git-server mostly for testing purposes, point it at a directory and
/// it will serve any repositories from within over HTTP.
#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    /// Address to listen for connections on
    #[arg(long)]
    address: Option<String>,

    /// Logging Level
    #[arg(long)]
    level: Option<Level>,

    /// Authentication string user:password
    #[arg(short, long)]
    auth: Option<String>,

    /// Directory to serve git repositories from
    workdir: Option<PathBuf>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = Cli::parse();
    tracing_subscriber::fmt()
        .compact()
        .with_line_number(true)
        .with_max_level(args.level.unwrap_or(Level::INFO))
        .init();
    let user_pass = args.auth.clone().map(|auth_str| {
        let split: Vec<&str> = auth_str.split(':').collect();
        (
            split.first().unwrap().to_string(),
            split.get(1).unwrap().to_string(),
        )
    });
    let server = Server::default()
        .with_auth_required(args.auth.is_some())
        .with_auth(move |user: &str, pass: &str| -> bool {
            println!("USER: {:?} PASS: {:?}", user, pass);
            if let Some(user_pass) = &user_pass {
                return user_pass.0 == user && user_pass.1 == pass;
            }
            true
        })
        .with_address(&args.address.unwrap_or(String::from("127.0.0.1:7878")))
        .with_workdir(
            args.workdir
                .as_ref()
                .map_or(Path::new("."), |p| p.as_path()),
        );
    server.serve().await?;
    Ok(())
}
