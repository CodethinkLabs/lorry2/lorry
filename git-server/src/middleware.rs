use std::sync::Arc;

use axum::{
    extract::Request,
    http::{
        header::{AUTHORIZATION, WWW_AUTHENTICATE},
        StatusCode,
    },
    middleware::Next,
    response::{IntoResponse, Response},
    Extension,
};
use axum_extra::headers::{authorization::Basic, Authorization, Header};

use crate::Configuration;

const BASIC_AUTH_REALM: &str = "Basic realm=\"Tiny Git Forge\"";

fn unathorized() -> Response {
    (
        StatusCode::UNAUTHORIZED,
        [(WWW_AUTHENTICATE, BASIC_AUTH_REALM)],
    )
        .into_response()
}

/// Enforce basic authorization as required by the server's configuration
pub async fn authorization(
    Extension(cfg): Extension<Arc<Configuration>>,
    request: Request,
    next: Next,
) -> Response {
    let is_push_request = request.uri().path().contains("git-receive-pack");
    for (header, value) in request.headers() {
        tracing::debug!("{:?} = {:?}", header, value);
    }
    if let Some(auth_fn) = &cfg.auth_fn {
        if let Some(auth_header) = request.headers().iter().find_map(|(header, value)| {
            if header.eq(&AUTHORIZATION) {
                let auth: Result<Authorization<Basic>, headers::Error> =
                    Authorization::decode(&mut [value.clone()].iter());
                auth.ok()
            } else {
                None
            }
        }) {
            let func = auth_fn.lock().await;
            if func(auth_header.username(), auth_header.password()) {
                tracing::debug!("Authentication successful")
            } else {
                tracing::warn!("Unauthorized request");
                return unathorized();
            }
        } else {
            tracing::warn!("Unauthorized request");
            return unathorized();
        }
    } else if is_push_request {
        // authorization not configured
        tracing::warn!("Unauthorized request");
        return unathorized();
    }
    next.run(request).await
}
