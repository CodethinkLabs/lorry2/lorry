use std::{
    io::{self, ErrorKind},
    path::Path,
    process::Stdio,
    str::FromStr,
    sync::Arc,
};

use axum::{
    body::Body,
    http::{
        header::{HeaderMap, HeaderName, HeaderValue},
        Method, StatusCode, Uri,
    },
    response::{IntoResponse, Response},
    Extension,
};
use axum_extra::TypedHeader;

use bytes::{Buf, Bytes, BytesMut};
use futures::TryStreamExt;
use tokio::{
    io::AsyncReadExt,
    process::{Child, ChildStderr, ChildStdout, Command},
    sync::mpsc,
};
use tokio_stream::wrappers::ReceiverStream;
use tokio_util::io::StreamReader;
use tracing::{debug, error, log, warn};

use crate::Configuration;

#[derive(Debug)]
pub struct Error(String);

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        (
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("Internal Server Error: {}", self.0),
        )
            .into_response()
    }
}

const GIT_HEADERS: &[(&str, &str)] = &[
    ("Content-Type", "CONTENT_TYPE"),
    ("Content-Length", "CONTENT_LENGTH"),
    ("Git-Protocol", "GIT_PROTOCOL"),
    ("Content-Encoding", "HTTP_CONTENT_ENCODING"),
];

struct CgiResponse(usize, StatusCode, HeaderMap);

/// Parse cgi headers returned from git-http-backend
fn parse_cgi_headers(buffer: &[u8]) -> Result<Option<CgiResponse>, Error> {
    let mut headers = [httparse::EMPTY_HEADER; 10];
    let (body_offset, headers) = match httparse::parse_headers(buffer, &mut headers)
        .map_err(|_| Error("failed to parse headers".into()))?
    {
        httparse::Status::Complete(v) => v,
        httparse::Status::Partial => return Ok(None),
    };

    let mut response_headers = HeaderMap::new();

    for header in headers {
        let name = HeaderName::from_str(header.name)
            .map_err(|_| Error("Failed to parse header name from Git over CGI".into()))?;
        let value = HeaderValue::from_bytes(header.value)
            .map_err(|_| Error("Failed to parse header value from Git over CGI".into()))?;
        debug!("git-http-backend returned header: {:?}/{:?}", name, value);
        response_headers.insert(name, value);
    }

    let mut status_code = StatusCode::OK;

    if let Some(status) = response_headers.remove("Status").filter(|s| s.len() >= 3) {
        let status = &status.as_ref()[..3];

        if let Ok(status) = StatusCode::from_bytes(status) {
            status_code = status
        }
    }

    Ok(Some(CgiResponse(
        body_offset,
        status_code,
        response_headers,
    )))
}

pub async fn handler(
    Extension(cfg): Extension<Arc<Configuration>>,
    TypedHeader(user_agent): TypedHeader<headers::UserAgent>,
    method: Method,
    uri: Uri,
    headers: HeaderMap,
    body: Body,
) -> Result<Response, Error> {
    log::info!("handling git operation on {:?} [{}]", cfg.root, user_agent,);
    let uri_path = Path::new(uri.path().trim_start_matches('/'));
    let path_translated = cfg.root.join(uri_path);

    let mut command = Command::new("git");

    for (header, env) in GIT_HEADERS {
        if let Some(value) = headers.get(*header) {
            let value = value.to_str().map_err(|_| Error("invalid header".into()))?;
            debug!("client header: {} {}", header, value);
            command.env(env, value);
        }
    }

    let mut cmd = command
        .arg("http-backend")
        .env("REQUEST_METHOD", method.as_str())
        .env(
            "PATH_TRANSLATED",
            path_translated.to_string_lossy().to_string(),
        )
        .env("QUERY_STRING", uri.query().unwrap_or(""))
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .kill_on_drop(true);

    log::debug!("calling {:?}", cmd);

    // always export everything
    cmd = cmd.env("GIT_HTTP_EXPORT_ALL", "true");
    let mut child = cmd.spawn().map_err(|e| Error(e.to_string()))?;

    let mut stdout = child.stdout.take().unwrap();
    let mut stderr = child.stderr.take().unwrap();
    let mut stdin = child.stdin.take().unwrap();

    // read request body and forward to stdin
    let mut body = StreamReader::new(
        body.into_data_stream()
            .map_err(|e| std::io::Error::new(ErrorKind::Other, e)),
    );

    let n_bytes = tokio::io::copy_buf(&mut body, &mut stdin)
        .await
        .map_err(|e| Error(e.to_string()))?;
    debug!("read {} bytes from client", n_bytes);

    // wait for the headers back from git http-backend
    let mut out_buf = BytesMut::with_capacity(1024);
    let cgi_response = loop {
        let n = stdout
            .read_buf(&mut out_buf)
            .await
            .map_err(|e| Error(e.to_string()))?;
        if n == 0 {
            break None;
        }

        if let Some(response) = parse_cgi_headers(&out_buf)? {
            out_buf.advance(response.0);
            debug!("read {} bytes", response.0);
            break Some((response.1, response.2));
        }
    };

    // if the `headers` loop broke with `None`, the `git http-backend` didn't return any parseable
    // headers so there's no reason for us to continue. there may be something in stderr for us
    // though.
    let Some(cgi_response) = cgi_response else {
        print_status(&mut child, &mut stderr).await;
        return Err(Error(
            "incomplete response from git-http-backend".to_string(),
        ));
    };

    // stream the response back to the client
    let (body_send, body_recv) = mpsc::channel(8);
    tokio::spawn(forward_response_to_client(
        out_buf, body_send, stdout, stderr, child,
    ));

    let mut resp = Body::from_stream(ReceiverStream::new(body_recv)).into_response();

    let status = resp.status_mut();
    *status = cgi_response.0;

    let resp_headers = resp.headers_mut();

    for header in cgi_response.1 {
        let name = header.0.unwrap();
        resp_headers.insert(name, header.1.clone());
    }

    Ok(resp)
}

/// Forwards the entirety of `stdout` to `body_send`, printing subprocess stderr and status on
/// completion.
async fn forward_response_to_client(
    mut out_buf: BytesMut,
    body_send: mpsc::Sender<Result<Bytes, io::Error>>,
    mut stdout: ChildStdout,
    mut stderr: ChildStderr,
    mut child: Child,
) {
    loop {
        let (out, mut end) = match stdout.read_buf(&mut out_buf).await {
            Ok(0) => (Ok(out_buf.split().freeze()), true),
            Ok(n) => (Ok(out_buf.split_to(n).freeze()), false),
            Err(e) => (Err(e), true),
        };

        if body_send.send(out).await.is_err() {
            warn!("Receiver went away during git http-backend call");
            end = true;
        }

        if end {
            break;
        }
    }

    print_status(&mut child, &mut stderr).await;
}

/// Prints the exit status of the `git` subprocess.
async fn print_status(child: &mut Child, stderr: &mut ChildStderr) {
    match tokio::try_join!(child.wait(), read_stderr(stderr)) {
        Ok((status, stderr)) if status.success() => {
            debug!(stderr, "git http-backend successfully shutdown");
        }
        Ok((status, stderr)) => error!(stderr, "git http-backend exited with status code {status}"),
        Err(e) => error!("Failed to wait on git http-backend shutdown: {e}"),
    }
}

/// Reads the entirety of stderr for the given handle.
async fn read_stderr(stderr: &mut ChildStderr) -> io::Result<String> {
    let mut stderr_out = Vec::new();
    stderr.read_to_end(&mut stderr_out).await?;
    Ok(String::from_utf8_lossy(&stderr_out).into_owned())
}
