#!/bin/sh

set -ex


mkdir -p lorry/db
mkdir -p db

./scripts/run_gitlab.sh -ci
./scripts/wait_until_gitlab_is_ready.sh
. ./scripts/request_gitlab_token.sh
SQLX_OFFLINE=true cargo test -- --ignored
podman kill lorry-gitlab-integration-test
podman rm lorry-gitlab-integration-test
