#!/bin/sh

[[ -d "public" ]] && {
	rm -r public
}

mdbook build -d ../public docs
# Deploy API documentation for workerlib module
cargo doc --package workerlib --no-deps
cp -R target/doc public/api
