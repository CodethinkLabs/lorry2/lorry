#!/bin/sh
# Tag a new release of Lorry
set -e

TMP_ANNOTATE_PATH="/tmp/lorry-annotation.txt"

if [ ! -n "$1" ] ; then
	printf "Usage: release.sh VERSION"
	exit 1
fi

TAG="$1"

PREVIOUS_TAG="$(git describe --tags --abbrev=0)"

printf "Release Lorry version: $TAG\n\n" > "$TMP_ANNOTATE_PATH"
git --no-pager log --oneline --no-merges "$PREVIOUS_TAG..HEAD" >> "$TMP_ANNOTATE_PATH"

git tag --sign --edit -a "$TAG" -F "$TMP_ANNOTATE_PATH"
