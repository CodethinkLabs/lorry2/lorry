#!/bin/sh

# This script exists to facilitate the running of as many of the Lorry2
# pipeline checks on one's dev machine as possible.  The checks are taken from
# `gitlab-ci.yml`

#------------------------------------------------------------------------------

run_check() {
    echo "[running] '$1'"
    eval "$1" > /dev/null 2>&1
    if [ $? -eq 0 ] ;
    then
        echo " - passed"
    else
        echo " - failed (exit code = $?)"
        exit 1
    fi
}

#------------------------------------------------------------------------------

# Run the quick ones first (i.e. fail fast)
run_check "cargo clippy -- -Dwarnings"
run_check "cargo sqlx prepare --workspace"
run_check "cargo fmt --check"
run_check "mdbook build ./docs"

# This is actually stricter than the CI (this documents all, not just
# workerlib, and includes private items)
run_check "cargo doc --document-private-items --no-deps"

# Run last because it's the slowest
run_check "cargo test"

#------------------------------------------------------------------------------

echo "\n"
echo "Lorry2: all passed"
echo "\n"
echo "All local checks passing (GitLab CI pipeline likely to pass)"
echo "\n"
