import json
import argparse
import requests

def update_lorry_data(instance,details):
    print(f"Fetching lorry stats for {instance}")
    headers = {}
    if "auth" in details:
        headers["Authorization"] = details["auth"]
    response = requests.get(f"{details["url"]}/1.0/list-lorries",headers=headers)
    if response.status_code == 200:
        with open(f"{instance}_lorries.json", "wb") as f:
            f.write(response.content)
            print(f"Cached: {instance}_lorries.json")
    else:
        print(f"{instance} declined to comment on its lorries: {response.status_code}")

def gen_stats(instance,lorries):
    stats = {
        "total": len(lorries),
        "never_run": 0,
        "running": 0,
        "running_job" : 0,
        "running_mismatch": 0,
        "success": 0,
        "fail": 0,

    }

    failed = []

    for lorry in lorries:
        if lorry["running_job"]:
            stats["running_job"] +=1
        if lorry["last_run"] == 0:
            stats["never_run"] +=1
        elif isinstance(lorry["last_run_results"],str):
            # ERM for some reason "last_run_finished" can be the string "running",
            # but it does not align with the lorry["running_job"] job field...
            # shrugs
            if not lorry["running_job"]:
                stats["running_mismatch"] +=1
            stats["running"] +=1
        elif lorry["last_run_results"]["Finished"]["exit_code"] == 0:
            stats["success"] +=1
        else:
            stats["fail"] +=1
            failed.append(lorry["name"])
    print(stats)
    with open(f"{instance}_stats.json","w") as stats_file:
        json.dump(stats,stats_file, indent=2)
        print(f"Stats dumped to {instance}_stats.json")
    with open(f"{instance}_fails.json","w") as fails_file:
        json.dump(failed,fails_file, indent=2)
        print(f"Fails dumped to {instance}_fails.json")


def main():
    parser = argparse.ArgumentParser(
            usage="""
Create a lorries_config.json
{
    "my_lorry": {
        "url":"<URL TO API ENDPOINT>"
        "auth": "<FIXME>" # Optional
    }
}

To pull results from lorry: `./lorries_analysis.py --update`
On other runs runs can use cached results: `./lorries_analysis.py`
   - e.g while modifying analysis output
""",
            description="analyse lorry API list-lorries status")
    parser.add_argument("--update", action="store_true",
                            help="Query the api instead of using cached results")
    parser.add_argument("--config", default="lorries_config.json",
                            help="Path to the config file to use")

    args = parser.parse_args()

    with open(args.config,"r") as config:
        lorry_instances = json.load(config)

    for (instance,details) in lorry_instances.items():
        if args.update:
            update_lorry_data(instance,details)
        with open(f"{instance}_lorries.json","r") as lorries:
            print(f"{instance}:")
            lorries = json.load(lorries)
            gen_stats(instance,lorries)

if __name__ == "__main__":
    main()
