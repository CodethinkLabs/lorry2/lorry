#!/usr/bin/env python3

# Last tested against lorry docker image: `registry.gitlab.com/codethinklabs/lorry/lorry2:main-2024-02-23-c28e3b12`

from pathlib import Path
import argparse


def convert_file(file_path):
    """
    convert a single lorry file to lorry2 format
    """
    content = ""

    with open(file_path, "r", encoding="utf-8") as r:
        new_repo_flag = False
        repo_content = ""
        merge_url_flag = False

        for line in r.readlines():
            stripped_line = line.strip()
            # new repo
            if not line.startswith(" ") and stripped_line.endswith(":"):
                if repo_content:
                    content += repo_content
                repo_content = ""
                new_repo_flag = True

            if new_repo_flag:
                # merge line only contains "url:" with next line
                # ie: convert
                #
                # url:
                #    https://lorry1.url
                #
                # to "url: https://lorry1.url"
                if stripped_line == "url:":
                    repo_content += line.rstrip()
                    merge_url_flag = True
                # drop repos with "type: tarball", "type: hg", "type: bzr",
                #                 "type: svn", "type: cvs",
                #                 "type: zip", "type: gzip"
                # as they are no longer supported in lorry2
                elif stripped_line in [
                    "type: tarball", "type: hg", "type: bzr", "type: svn",
                    "type: cvs", "type: zip", "type: gzip"
                ]:
                    repo_content = ""
                    new_repo_flag = False
                else:
                    if merge_url_flag:
                        repo_content += f" {stripped_line}\n"
                        merge_url_flag = False
                    else:
                        repo_content += line

        if repo_content:
            content += repo_content

    return content


def convert_to_lorry2(lorry_config_repo):
    """
    convert lorry_config_repo to be compatible with lorry2
    """

    dirs_to_ignore = [".git"]
    dir_path = Path(lorry_config_repo).absolute()
    subdirs = sorted([d for d in dir_path.iterdir()
                      if d.is_dir() and d.name not in dirs_to_ignore])

    for subdir in subdirs:
        lorry_files = [
            f for f in subdir.iterdir() if f.is_file() and f.suffix == ".lorry"
        ]

        for lorry_file in lorry_files:
            content = convert_file(lorry_file)
            with open(lorry_file, "w", encoding="utf-8") as w:
                w.write(content)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        usage="""convert_to_lorry2.py lorry_config_repo

        lorry_config_repo: path to lorry config repo that need to be converted
        """,
        description="convert lorry config repo to lorry2 format")
    parser.add_argument('lorry_config_repo')
    args = parser.parse_args()
    convert_to_lorry2(args.lorry_config_repo)
