#!/bin/sh

echo "Checking if config documentation is synced..."

sed -n '/^```toml - Annotated Example$/,/```/{/```toml/!{/```/!p}}' ./docs/src/configuration.md > /tmp/example
if cmp --silent -- "/tmp/example" "./lorry.example.toml"; then
    echo "Documentation is consistent with example"
    exit 0
else
    case "$1" in
        -u|--update)
            echo "Updating config example documentation..."
            sed -i '/^```toml - Annotated Example$/,/```/{
                /```toml/!d 
                r ./lorry.example.toml
                a ```
            }' ./docs/src/configuration.md 
            echo "Updated documentation"
            exit 0 
            ;;
        *)
            echo "Config documentation requires sync, rerun with --update"
            exit 1
            ;;
    esac
fi



