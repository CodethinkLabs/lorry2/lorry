//! Module used to download raw files from the internet and mirror them into a
//! working directory.
//!

use crate::{Arguments, LORRY_VERSION_HEADER};
use futures_util::StreamExt;
use std::path::{Path, PathBuf};
use thiserror::Error;
use tokio::fs::File;
use tokio::io::AsyncWriteExt;

#[derive(Error, Debug)]
pub enum FileDownloadError {
    #[error("Could not open the file")]
    CouldNotOpenForWriting {
        source: std::io::Error,
        target: PathBuf,
    },

    #[error("Error occurred while setting up REQWEST")]
    CouldNotSetUpDownload { source: reqwest::Error },

    #[error("There was an error during the download itself")]
    DownloadFailed { source: reqwest::Error },

    #[error("Could not convert to synchronous file")]
    CouldNotConvertFile { target: PathBuf },

    #[error("Could not write to file")]
    CouldNotWriteToFile { source: std::io::Error },

    #[error("An unexpected status code was returned from the server: status code {0}")]
    InvalidStatusCode(u16),
}

/// Download the file from `url` to `file_destination`
pub(crate) async fn download_file(
    file_destination: &Path,
    url: &url::Url,
    check_certs: bool,
    arguments: &Arguments,
) -> Result<(), FileDownloadError> {
    let mut file_to_write_to = File::create(file_destination).await.map_err(|e| {
        FileDownloadError::CouldNotOpenForWriting {
            source: e,
            target: file_destination.to_path_buf(),
        }
    })?;
    // Build the client
    let client = reqwest::Client::builder()
        .user_agent(LORRY_VERSION_HEADER)
        .redirect(reqwest::redirect::Policy::limited(
            (arguments.maximum_redirects + 1) as usize,
        ))
        .danger_accept_invalid_certs(arguments.check_ssl_certificates && check_certs)
        .build()
        .map_err(|e| FileDownloadError::CouldNotSetUpDownload { source: e })?;
    // Use client for GET request
    let res = client
        .get(url.clone())
        .send()
        .await
        .map_err(|e| FileDownloadError::DownloadFailed { source: e })?;

    // TODO: In the previous implementation of download_file using curl
    // it was valid to encounter a zero status because of how we "move
    // local files around" Determine whether this is still an issue.
    if res.status() != 200 && res.status() != 0 {
        return Err(FileDownloadError::InvalidStatusCode(res.status().into()));
    }

    let file_destination_as_string = file_destination.to_string_lossy();
    let headers = res.headers();
    // Grab headers and get the last modified time as UTC
    let url_date = match headers.get("Last-Modified") {
        // Attempt to extract header line without last-modified prefix
        Some(header) => {
            // get time as UTC
            let header_as_str = header.to_str().unwrap_or("").trim();
            match chrono::DateTime::parse_from_rfc2822(header_as_str) {
                Ok(dt) => Some(dt),
                Err(e) => {
                    tracing::debug!("Was unable to parse the Last-Modified header from {url} into a timestamp. This is not a critical error but will result in incorrect access time metadata for {file_destination_as_string}. Error: {e}
                        the header was : {header_as_str}");
                        None
                },
            }
        }
        None => None,
    }.map(chrono::DateTime::<chrono::Utc>::from);

    let mut stream = res.bytes_stream();
    // Read chunks from stream and write to file asynchronously
    while let Some(chunk) = stream.next().await {
        match chunk {
            Ok(s) => {
                file_to_write_to
                    .write_all(&s)
                    .await
                    .map_err(|e| FileDownloadError::CouldNotWriteToFile { source: e })?;
            }
            Err(e) => return Err(FileDownloadError::CouldNotSetUpDownload { source: e }),
        }
    }
    // Flush the output stream to ensure all contents are written to disk
    // Do we need sync_all here?
    file_to_write_to
        .flush()
        .await
        .map_err(|e| FileDownloadError::CouldNotWriteToFile { source: e })?;
    let file =
        file_to_write_to
            .try_into_std()
            .map_err(|_| FileDownloadError::CouldNotConvertFile {
                target: file_destination.to_path_buf(),
            })?;

    // If we could extract the remote file's last modified date, apply it to the local file
    match url_date {
        None => Ok(()),
        Some(dt) => {
            match file.set_modified(dt.into()) {
                Err(e) => {
                    tracing::debug!("Was unable to set the last modified time from {url} to to file at {file_destination_as_string}. This is not a critical error but will result in incorrect access time metadata. Error: {e}");
                    Ok(())
                } // Discard an error here; it doesn't matter.
                Ok(()) => Ok(()),
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use mockito::Server;
    use std::{path::Path, time::UNIX_EPOCH};
    use tempfile::TempDir;
    use url::Url;

    fn setup(tmp_path: &Path, maximum_redirects: u32) -> Arguments {
        Arguments {
            working_area: tmp_path.join("working-area"),
            maximum_redirects,
            ..Default::default()
        }
    }

    #[tokio::test]
    async fn test_redirection() {
        let mut server = Server::new_async().await;
        // Return HTTP 301
        server
            .mock("GET", "/redirect")
            .with_status(301)
            .with_header("Location", "/target")
            .create_async()
            .await;
        server
            .mock("GET", "/target")
            .with_body("abcd")
            .create_async()
            .await;
        let target_url = server.url() + "/redirect";
        let target = Url::parse(&target_url).unwrap();
        let tmp = TempDir::new().unwrap();
        let args = setup(tmp.path(), 0);
        let ret = download_file(&tmp.path().join("test-file"), &target, false, &args).await;
        // the server returns a redirect but we don't allow for them
        assert!(ret.is_err());
        let err = ret.err().unwrap();
        println!("ERROR: {:?}", err);
        assert!(matches!(err, FileDownloadError::DownloadFailed { .. }));
        let args = setup(tmp.path(), 1);
        let ret = download_file(&tmp.path().join("test-file"), &target, false, &args).await;
        // the server follows the redirect and works as expected
        assert!(ret.is_ok());
        let content = std::fs::read_to_string(tmp.path().join("test-file")).unwrap();
        assert!(content == "abcd")
    }

    #[tokio::test]
    async fn test_header() {
        let mut server = Server::new_async().await;
        server
            .mock("GET", "/")
            .with_status(200)
            .with_header("Last-Modified", "Wed, 21 Oct 2015 07:28:00 GMT")
            .with_body("xyz")
            .create_async()
            .await;

        let mut server2 = Server::new_async().await;
        server2
            .mock("GET", "/")
            .with_status(200)
            .with_body("xyz")
            .create_async()
            .await;
        let mut target_url = server.url();
        let mut target = Url::parse(&target_url).unwrap();
        let tmp = TempDir::new().unwrap();
        let args = setup(tmp.path(), 10);
        let ret = download_file(&tmp.path().join("test-file"), &target, false, &args).await;
        assert!(ret.is_ok());
        let data = std::fs::metadata(tmp.path().join("test-file")).unwrap();
        assert_eq!(
            1445412480,
            data.modified()
                .unwrap()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_secs()
        );
        target_url = server2.url();
        target = Url::parse(&target_url).unwrap();
        let ret = download_file(&tmp.path().join("test-file"), &target, false, &args).await;
        assert!(ret.is_ok());
    }
}
