//! Internal server for various integration tests
//!

use std::io::Error as IoError;
use std::path::Path;

use git2::{Diff, Error as GitError, Repository, RepositoryInitOptions};
use git_server::Server as GitServer;
use port_check::free_local_port;
use tokio::sync::mpsc::channel;
use url::Url;

pub const EXAMPLE_COMMIT: &str = r#"
diff --git a/world.txt b/world.txt
new file mode 100644
index 0000000..4a881d8
--- /dev/null
+++ b/world.txt
@@ -0,0 +1 @@
+Hello
"#;

const USERNAME: &str = "lorry";
const PASSWORD: &str = "insecure";
const INITIAL_COMMIT_MESSAGE: &str = "Initial Commit\n\nHello World";

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("IO Error: {0}")]
    IoError(#[from] IoError),

    #[error("Failed to setup repository: {0}")]
    GitError(#[from] GitError),
}

// Represents a named repository with an array of commits applied within
#[derive(Clone)]
pub struct TestRepo(pub (String, Vec<String>));

impl TestRepo {
    pub fn address(&self, addr: &str) -> Url {
        Url::parse(format!("http://{}:{}@{}/{}", USERNAME, PASSWORD, addr, self.0 .0).as_str())
            .unwrap()
    }
}

/// Initialize the test dir with a vector or repositories and return a
/// GitServer ready to serve them. The test_dir should be deleted after the
/// test has been completed. The GitServer is expected to run in its own
/// thread setup by the test caller.
pub async fn spawn_test_server(
    test_dir: &Path,
    repositories: &[TestRepo],
) -> Result<String, Error> {
    for repo in repositories {
        let mut opts = RepositoryInitOptions::new();
        opts.initial_head("main");
        opts.bare(true);
        let repo_dir = test_dir.join(repo.0 .0.clone());
        let repository = Repository::init_opts(repo_dir.as_path(), &opts)?;
        let mut config = repository.config()?;
        config.set_str("user.name", "Lorry")?;
        config.set_str("user.email", "lorry@example.org")?;
        // ensure git allows pushing over http
        config.set_bool("http.receivepack", true)?;
        let mut index = repository.index()?;
        let id = index.write_tree()?;
        let tree = repository.find_tree(id)?;
        let sig = repository.signature()?;
        repository.commit(Some("HEAD"), &sig, &sig, INITIAL_COMMIT_MESSAGE, &tree, &[])?;
        for (i, diff_contents) in repo.0 .1.iter().enumerate() {
            let id = index.write_tree().unwrap();
            let tree = repository.find_tree(id).unwrap();
            let sig = repository.signature().unwrap();
            let diff = Diff::from_buffer(diff_contents.as_bytes()).unwrap();
            let mut index = repository.apply_to_tree(&tree, &diff, None).unwrap();
            repository.set_index(&mut index).unwrap();
            let id = index.write_tree().unwrap();
            let tree = repository.find_tree(id).unwrap();
            let head = repository.head().unwrap();
            let last_commit = head.peel_to_commit().unwrap();
            repository.commit(
                Some("HEAD"),
                &sig,
                &sig,
                format!("Test Commit: {}/{}", i + 1, repo.0 .1.len()).as_str(),
                &tree,
                &[&last_commit],
            )?;
        }
    }
    let free_port = free_local_port().unwrap();
    let address = format!("127.0.0.1:{}", free_port);
    let address_thread = address.clone();
    let test_dir_thread = test_dir.to_path_buf();
    let (tx, mut rx) = channel::<bool>(1);
    tokio::task::spawn(async move {
        let server = GitServer::default()
            .with_address(&address_thread)
            .enable_push(true)
            .with_auth_required(true)
            .with_workdir(&test_dir_thread)
            .with_auth(|user, pass| user == USERNAME && pass == PASSWORD);
        tx.send(true).await.unwrap();
        server.serve().await.unwrap();
    });
    rx.recv().await.unwrap();
    Ok(address)
}
