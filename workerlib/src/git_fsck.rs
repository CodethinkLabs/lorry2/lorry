use std::path::Path;

use crate::execute::{Command, CommandBuilder};

/// Git Fsck Validity Checking
pub struct Fsck<'a> {
    pub config_path: &'a Path,
    // Enable the --connectivity-only flag for git-fsck
    pub connectivity_only: bool,
    // Enable the --no-danglging flag for git-fsck
    pub no_dangling: bool,
}

impl CommandBuilder for Fsck<'_> {
    fn build(&self, current_dir: &Path) -> tokio::process::Command {
        let mut cmd = Command::new("git");
        cmd.current_dir(current_dir);
        cmd.envs([(
            crate::git_config::GIT_CONFIG_GLOBAL,
            self.config_path.to_string_lossy().as_ref(),
        )]);
        let mut args = vec!["fsck", "--no-progress"];
        if self.connectivity_only {
            args.push("--connectivity-only");
        }
        if self.no_dangling {
            args.push("--no-dangling");
        }
        cmd.args(args);
        cmd
    }
}

#[cfg(test)]
mod tests {
    use crate::workspace::Workspace;

    use super::*;
    use tempfile::tempdir;

    #[tokio::test]
    pub async fn run_git_fsck_bare() {
        let workdir = tempdir().unwrap();
        let workspace = Workspace(workdir.into_path());
        assert!(workspace.init_if_missing(false, true).unwrap());
        let fsck = Fsck {
            config_path: Path::new("/dev/null"),
            connectivity_only: false,
            no_dangling: false,
        };
        fsck.build(workspace.repository_path().as_path())
            .output()
            .await
            .unwrap();
    }
}
