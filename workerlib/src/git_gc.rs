use std::path::Path;

use crate::execute::{Command, CommandBuilder};

const DEFAULT_GC_EXPIRY: &str = "1d";

/// Git Garbage Collection
pub struct Gc<'a> {
    pub config_path: &'a Path,
    /// Earliest date to prune temporary objects from
    pub prune_expiry_date: Option<&'a str>,
    /// If the --auto flag should be enabled, typically should be false since
    /// if it is enabled prune is unlikely to be ran frequently.
    pub auto: bool,
}

impl CommandBuilder for Gc<'_> {
    fn build(&self, current_dir: &Path) -> tokio::process::Command {
        let mut cmd = Command::new("git");
        cmd.current_dir(current_dir);
        cmd.envs([(
            crate::git_config::GIT_CONFIG_GLOBAL,
            self.config_path.to_string_lossy().as_ref(),
        )]);
        let expiry = self.prune_expiry_date.unwrap_or(DEFAULT_GC_EXPIRY);
        let prune_arg = format!("--prune={}", expiry);
        let mut args = vec!["gc", "--quiet", &prune_arg];
        if self.auto {
            args.push("--auto")
        }
        cmd.args(args);
        cmd
    }
}

#[cfg(test)]
mod tests {
    use crate::workspace::Workspace;

    use super::*;
    use tempfile::tempdir;

    #[tokio::test]
    pub async fn run_git_gc_bare() {
        let workdir = tempdir().unwrap();
        let workspace = Workspace(workdir.into_path());
        assert!(workspace.init_if_missing(false, true).unwrap());
        let gc = Gc {
            config_path: Path::new("/dev/null"),
            prune_expiry_date: None,
            auto: false,
        };
        gc.build(workspace.repository_path().as_path())
            .output()
            .await
            .unwrap();
    }
}
