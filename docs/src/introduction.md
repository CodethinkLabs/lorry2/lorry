
![Lorry Logo](images/lorry-tiny.png)

# Introduction 

Lorry is a high performance software asset mirroring system that duplicates 
Git repositories and other build artifacts into "downstream" software 
repositories. Lorry is capable of mirroring tens of thousands of software 
repositories and raw files.

## Why Use Lorry

If you only have the need to mirror a few Git repositories periodically Lorry
might not be the right choice as you can use tools provided by your Git host.
However if you require a large volume of mirrored code so that your software
can be built when upstream source code is lost or modified Lorry may be a good
choice.

### Upstream Code Can Disapear

Upstream software repositories can disappear for various reasons which can 
cause disruption in the supply chain. Lorry maintains mirrors of software so 
that if an upstream becomes unavailable due to an outage or other means the
code is still available to you.

### Mirroring Through Firewalls

Some software development environments forbid direct access to the internet.
Lorry can be configured in such a way that it is placed in a DMZ zone between
the isolated environment and the generally available internet.

### Auditable Configuration

Using Lorry with a `confgit` configuration source: A lorry configuration stored
within a Git repository allows auditing and the implementation of change 
control practices around the adding and removing of software dependencies.

### Seperation of Concerns

It is often desirable to separate the roles responsibilities of operations 
teams with those of software developers. Using Lorry in combination with the
security policies of Gitlab allow developers with appropriate repository
access to be able to manage creation of software mirrors independent of 
operations teams.

### Avoiding Re-written History

Although a discouraged practice some software repositories "force push" changes 
to their main branches disrupting the verifiable integrity of their codebase. 
In worse cases a nefarious actor can even "force push" malicious code into at
Git repository and software that depends on it will pull it down by default.
Lorry disallows mirroring git references that contain re-written history by
default which provides a safe-guard against bad changes.

### Regulatory Compliance

Some software licenses require that consumed source code is made available on
public servers.

## Architecture Diagram

A simple diagram illustrating the various components of a Lorry installation.

![Architecture Diagram](images/diagram.png)
