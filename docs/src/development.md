# Development

## Rust Docs

Rust docs can be built for Lorry and served locally with `cargo doc`.  Running `cargo doc --open` from `/lorry` opens up the Rust docs for Lorry, while running it from `/workerlib` opens them up for WorkerLib.  Adding the `--document-private-items` option means Rust docs will be included for non-public functions/methods/etc.

## Developing Lorry

The Lorry repository contains everything you need to start hacking on the
codebase right away. First ensure that you've followed the instructions for
[installing Lorry locally](./installation.html#from-source).

Although not strictly required installing [cargo-watch](https://watchexec.github.io/#cargo-watch) is suggested since it makes the development workflow easier. Install
it either by your package manager or via `cargo install watch`. Additionally
you should ensure that you have [podman](podman.io) installed which is the only
supported container platform for running Lorry.

This example uses Gitlab which is the primary [software forge](https://en.wikipedia.org/wiki/Forge_(software)) supported by Lorry.

```sh
# In a seperate terminal pane you can launch Gitlab. This will take a few minutes.
scripts/run_gitlab.sh

# Request an authentication token from Gitlab. Note that you need to wait about
# five minutes before running this since Gitlab will not immediately be ready.
#
# "file" can optionally be passed in as the first argument to save the GitLab
# PAT to the `.token` file.  Otherwise, it is put into the ENV.
source scripts/request_gitlab_token.sh

# Launch the watch script which will restart Lorry on code changes.
scripts/watch.sh
```

Once GitLab is running it will be available at  [http://localhost:9999](http://localhost:9999).  The username is `root` and the password `insecure1111` (this can be changed by editing `/scripts/run_gitlab.sh`)

## Writing Documentation

Documentation is managaed with [mdbook](https://rust-lang.github.io/mdBook/).

Ensure you have that installed and then run `scripts/gen_book.sh` to launch the
server. Documentation content is located under `docs/content/`.

## API Documentation

<!-- TODO: Link broken -->
Lorry is not yet available on crates.io however its bleeding edge API 
documentation can be found [here](/api/workerlib/index.html).

## Development Containers

Lorry uses several containers during it's CI process. To update the version
of Rust for example that is used to compile Lorry you should modify the
appropriate variable in `scripts/setup_build_containers.sh` and then execute
that script (assuming you have appropriate permissions to access Lorry's
Gitlab container registry).

## Changing the version of Rust in CI / Containers

To change the version of Rust bump the version number defined in 
`scripts/setup_build_containers.sh` and then run the script to mirror the new
version. Additionally set the version defined in `.gitlab-ci.yml` to match the
container / tag you mirrored in the previous step.

## Releasing a New Version of Lorry

Lorry releases follow [SemVer](https://semver.org/).

First identify the previous version of Lorry by running `git tag`.

```sh
scripts/release.sh v2.1.0
# Edit the annotation with any relevant information
# Inspect the new tag
git tag
# Push the new tag
git push --tags
```

CI will automatically pickup the tag and a new version of the Lorry contrainer
should be available in several minutes in the container registry.

## Running e2e tests

The lorry repository contains e2e tests. They run on ever merge request, commit to default branch and release in CI.

To run the locally ensure that your repository is clean, there are no residual `.gitlab-container`, `lorry/db` or `lorry/workd` directories and no container named `lorry-gitlab-integration-test` is running in podman.

To run e2e tests, simply run

```sh
./scripts/run_e2e.sh
```

This will deploy gitlab using podman, run request a token from gitlab, run the tests and remove the gitlab container.
