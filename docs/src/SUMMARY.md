# Summary

Lorry is a high performance software asset mirroring system that duplicates 
Git repositories and other build artifacts into "downstream" software 
repositories. Its configuration can be stored on disk or within a git 
repository which will be periodically updated.

- [Introduction](./introduction.md)
- [Installation](./installation.md)
- [Configuration](./configuration.md)
- [HTTP Interface](./http_interface.md)
- [Development](./development.md)
- [Troubleshooting](./troubleshooting.md)
- [Migrate From Lorry 1](./lorry-1-migration.md)
- [License](./license.md)
